<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

// Get the extensions's configuration
$extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_community']);

// Set language source file
$ll = 'LLL:EXT:moox_community/Resources/Private/Language/locallang_db.xlf:';

// hide fe group default fields for custom fe group variant
$hideFields = 'description,subgroup,tx_extbase_type';

/***************
 * Add extra fields to the fe_groups record
 */
$newFeGroupsColumns = array(
    'pid' => array(
        'label' => $ll.'tx_mooxcommunity_domain_model_frontendusergroup.pid',
        'config' => array(
            'type' => 'passthrough'
        )
    ),
    'tstamp' => array(
        $ll.'tx_mooxcommunity_domain_model_frontendusergroup.tstamp',
        'config' => array(
            'type' => 'passthrough'
        ),
        // special moox configuration
        'moox' => array(
            'extkey' => 'moox_community',
            'plugins' => array(
                'mooxcommunity' => array(
                    'listGroups','detailGroup'
                ),
            ),
            'sortable' => 0,
        ),
    ),
    'crdate' => array(
        $ll.'tx_mooxcommunity_domain_model_frontendusergroup.crdate',
        'config' => array(
            'type' => 'passthrough'
        ),
        // special moox configuration
        'moox' => array(
            'extkey' => 'moox_community',
            'plugins' => array(
                'mooxcommunity' => array(
                    'listGroups','detailGroup'
                ),
            ),
            'sortable' => 0,
        ),
    ),
    'variant' => array(
        'exclude' => 1,
        'label'    => $ll.'tx_mooxcommunity_domain_model_frontendusergroup.variant',
        'config' => array(
            'type' => 'select',
            'items' => array(
                array($ll . 'tx_mooxcommunity_domain_model_frontendusergroup.variant.default', 'fe_groups'),
                array($ll . 'tx_mooxcommunity_domain_model_frontendusergroup.variant.community', 'moox_community'),
            ),
            'size' => 1,
            'maxitems' => 1,
        ),
        // special moox configuration
        'moox' => array(
            'extkey' => 'moox_community',
        ),
    ),
    'quota_event_note' => array(
        'exclude' => 0,
        'label' => $ll . 'tx_mooxcommunity_domain_model_frontendusergroup.quota_event_note',
        'displayCond' => 'FIELD:variant:=:fe_groups',
        'config' => array(
            'type' => 'input',
            'size' => 15,
            'eval' => 'int',
            'default' => '0'
        )
    ),
    'quota_marketplace' => array(
        'exclude' => 0,
        'label' => $ll . 'tx_mooxcommunity_domain_model_frontendusergroup.quota_marketplace',
        'displayCond' => 'FIELD:variant:=:fe_groups',
        'config' => array(
            'type' => 'input',
            'size' => 15,
            'eval' => 'int',
            'default' => '0'
        )
    ),
    'community_admins' => array(
        'exclude' => 0,
        'label' => $ll.'tx_mooxcommunity_domain_model_frontendusergroup.community_admins',
        'displayCond' => 'FIELD:variant:=:moox_community',
        'config' => array(
            'type' => 'select',
            'allowNonIdValues' => 1,
            'enableMultiSelectFilterTextfield' => 1,
            'selectedListStyle' => 'width:400px',
            'itemListStyle' => 'width:400px',
            'default' => '',
            'itemsProcFunc' => 'DCNGmbH\MooxCommunity\Hooks\TcaFormHelper->communityAdmins',
            'size' => 6,
            'maxitems' => 99,
            'minitems' => 0,
            'multiple' => 1,
        ),
        // special moox configuration
        'moox' => array(
            'extkey' => 'moox_community',
        ),
    ),
    'community_moderators' => array(
        'exclude' => 0,
        'label' => $ll.'tx_mooxcommunity_domain_model_frontendusergroup.community_moderators',
        'displayCond' => 'FIELD:variant:=:moox_community',
        'config' => array(
            'type' => 'select',
            'allowNonIdValues' => 1,
            'enableMultiSelectFilterTextfield' => 1,
            'selectedListStyle' => 'width:400px',
            'itemListStyle' => 'width:400px',
            'default' => '',
            'itemsProcFunc' => 'DCNGmbH\MooxCommunity\Hooks\TcaFormHelper->communityModerators',
            'size' => 6,
            'maxitems' => 99,
            'minitems' => 0,
            'multiple' => 1,
        ),
        // special moox configuration
        'moox' => array(
            'extkey' => 'moox_community',
        ),
    ),
    'info' => array(
        'exclude' => 0,
        'label' => $ll.'tx_mooxcommunity_domain_model_frontendusergroup.info',
        'displayCond' => 'FIELD:variant:=:moox_community',
        'config' => array(
            'type' => 'text',
            'cols' => 40,
            'rows' => 10,
            'eval' => 'trim',
        ),
        'defaultExtras' => 'richtext[]',
        // special moox configuration
        'moox' => array(
            'extkey' => 'moox_community',
            'plugins' => array(
                'mooxcommunity' => array(
                    'editGroup','detailGroup'
                ),
            ),
            'sortable' => 0,
        ),
    ),
    'images' => array(
        'exclude' => 0,
        'label' => $ll.'tx_mooxcommunity_domain_model_frontendusergroup.images',
        'displayCond' => 'FIELD:variant:=:moox_community',
        'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
            'images',
            array(
                'reference' => 'image',
                'maxitems' => 3,
                'maxfilesize' => 20480,
                'accepts' => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
                'appearance' => array(
                    'headerThumbnail' => array(
                        'width' => '100',
                        'height' => '100',
                    ),
                    'createNewRelationLinkTitle' => $ll.'tx_mooxcommunity_domain_model_frontendusergroup.images.add'
                ),
                // custom configuration for displaying fields in the overlay/reference table
                // to use the imageoverlayPalette instead of the basicoverlayPalette
                'foreign_types' => array(
                    '0' => array(
                        'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
                    ),
                    \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => array(
                        'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
                    ),
                    \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
                        'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
                    ),
                    \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => array(
                        'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
                    ),
                    \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => array(
                        'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
                    ),
                    \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => array(
                        'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
                    )
                ),
            ),
            $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
        ),
        // special moox configuration
        'moox' => array(
            'extkey' => 'moox_community',
            'plugins' => array(
                'mooxcommunity' => array(
                    'editGroup','detailGroup'
                ),
            ),
            'sortable' => 0,
        ),
    ),
    'zip' => array(
        'exclude' => 0,
        'label' => $ll.'tx_mooxcommunity_domain_model_frontendusergroup.zip',
        'displayCond' => 'FIELD:variant:=:moox_community',
        'config' => array(
            'type' => 'input',
            'size' => 5,
            'eval' => 'integer',
            'range' => array(
                'lower' => 10000,
                'upper' => 99999,
            ),
        ),
        // special moox configuration
        'moox' => array(
            'extkey' => 'moox_community',
            'plugins' => array(
                'mooxcommunity' => array(
                    'listGroups','editGroup','detailGroup'
                ),
            ),
            'sortable' => array(
                'additional_sorting' => 'title ASC',
            ),
        ),
    ),
    'city' => array(
        'exclude' => 0,
        'label' => $ll.'tx_mooxcommunity_domain_model_frontendusergroup.city',
        'displayCond' => 'FIELD:variant:=:moox_community',
        'config' => array(
            'type' => 'input',
            'size' => 40,
            'eval' => 'trim'
        ),
        // special moox configuration
        'moox' => array(
            'extkey' => 'moox_community',
            'plugins' => array(
                'mooxcommunity' => array(
                    'listGroups','editGroup','detailGroup'
                ),
            ),
            'sortable' => 1,
        ),
    ),
    'address' => array(
        'exclude' => 0,
        'label' => $ll.'tx_mooxcommunity_domain_model_frontendusergroup.address',
        'displayCond' => 'FIELD:variant:=:moox_community',
        'config' => array(
            'type' => 'input',
            'size' => 40,
            'eval' => 'trim'
        ),
        // special moox configuration
        'moox' => array(
            'extkey' => 'moox_community',
            'plugins' => array(
                'mooxcommunity' => array(
                    'listGroups','editGroup','detailGroup'
                ),
            ),
            'sortable' => 1,
        ),
    ),
    'telephone' => array(
        'exclude' => 0,
        'label' => $ll.'tx_mooxcommunity_domain_model_frontendusergroup.telephone',
        'displayCond' => 'FIELD:variant:=:moox_community',
        'config' => array(
            'type' => 'input',
            'size' => 40,
            'eval' => 'trim'
        ),
        // special moox configuration
        'moox' => array(
            'extkey' => 'moox_community',
            'plugins' => array(
                'mooxcommunity' => array(
                    'listGroups','editGroup','detailGroup'
                ),
            ),
            'sortable' => 1,
        ),
    ),
    'fax' => array(
        'exclude' => 0,
        'label' => $ll.'tx_mooxcommunity_domain_model_frontendusergroup.fax',
        'displayCond' => 'FIELD:variant:=:moox_community',
        'config' => array(
            'type' => 'input',
            'size' => 40,
            'eval' => 'trim'
        ),
        // special moox configuration
        'moox' => array(
            'extkey' => 'moox_community',
            'plugins' => array(
                'mooxcommunity' => array(
                    'listGroups','editGroup','detailGroup'
                ),
            ),
            'sortable' => 1,
        ),
    ),
    'email' => array(
        'exclude' => 0,
        'label' => $ll.'tx_mooxcommunity_domain_model_frontendusergroup.email',
        'displayCond' => 'FIELD:variant:=:moox_community',
        'config' => array(
            'type' => 'input',
            'size' => 40,
            'eval' => 'email,trim'
        ),
        // special moox configuration
        'moox' => array(
            'extkey' => 'moox_community',
            'plugins' => array(
                'mooxcommunity' => array(
                    'listGroups','editGroup','detailGroup'
                ),
            ),
            'sortable' => 1,
        ),
    ),
    'www' => array(
        'exclude' => 0,
        'label' => $ll.'tx_mooxcommunity_domain_model_frontendusergroup.www',
        'displayCond' => 'FIELD:variant:=:moox_community',
        'config' => array(
            'type' => 'input',
            'size' => 40,
            'eval' => 'trim'
        ),
        // special moox configuration
        'moox' => array(
            'extkey' => 'moox_community',
            'plugins' => array(
                'mooxcommunity' => array(
                    'listGroups','editGroup','detailGroup'
                ),
            ),
            'sortable' => 1,
        ),
    ),
    'latitude' => array(
        'exclude' => 0,
        'label' => $ll.'tx_mooxcommunity_domain_model_frontendusergroup.latitude',
        'displayCond' => 'FIELD:variant:=:moox_community',
        'config' => array(
            'type' => 'input',
            'size' => 40,
            'eval' => 'trim'
        ),
        // special moox configuration
        'moox' => array(
            'extkey' => 'moox_community',
        ),
    ),
    'longitude' => array(
        'exclude' => 0,
        'label' => $ll.'tx_mooxcommunity_domain_model_frontendusergroup.longitude',
        'displayCond' => 'FIELD:variant:=:moox_community',
        'config' => array(
            'type' => 'input',
            'size' => 40,
            'eval' => 'trim'
        ),
        // special moox configuration
        'moox' => array(
            'extkey' => 'moox_community',
        ),
    ),
);

// add special news palette
$newPalettes = [
    'quota' => [
        'showitem' => 'quota_event_note, quota_marketplace'
    ]
];

// merge own palettes with tt_content palettes
$GLOBALS['TCA']['fe_groups']['palettes'] = array_merge([], $newPalettes);

// Add new TCA fields
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_groups', $newFeGroupsColumns, true);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_groups', 'variant', '', 'before:title');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'fe_groups',
    'info,images,zip,city,address,telephone,fax,email,www',
    '',
    'after:title'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'fe_groups',
    'community_admins,community_moderators,latitude,longitude',
    '',
    'after:tx_extbase_type'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'fe_groups',
    ',--palette--;;quota',
    '',
    'after:title'
);

// extend existing fe group fields
$extendFields = array('title');
foreach ($extendFields as $field) {
    if (isset($GLOBALS['TCA']['fe_groups']['columns'][$field])) {
        $GLOBALS['TCA']['fe_groups']['columns'][$field]['moox']= array(
            'extkey' => 'moox_community',
            'plugins' => array(
                'mooxcommunity' => array(
                    'listGroups','editGroup','detailGroup'
                ),
            ),
            'sortable' => 1,
        );
    }
}

// set size of fe group title field
$GLOBALS['TCA']['fe_groups']['columns']['title']['config']['size'] = 40;
$GLOBALS['TCA']['fe_groups']['columns']['title']['config']['max'] = 200;

// add variant field to update fields
if (isset($GLOBALS['TCA']['fe_groups']['ctrl']['requestUpdate']) && $GLOBALS['TCA']['fe_groups']['ctrl']['requestUpdate']!='') {
    $GLOBALS['TCA']['fe_groups']['ctrl']['requestUpdate'] = $GLOBALS['TCA']['fe_groups']['ctrl']['requestUpdate'].',variant';
} else {
    $GLOBALS['TCA']['fe_groups']['ctrl']['requestUpdate'] = 'variant';
}

// hide fe group default fields defined in hide fields string
\DCNGmbH\MooxMarketplace\Service\HelperService::tcaHideDefaultFields($hideFields, 'fe_groups', 'moox_community', '!=');
