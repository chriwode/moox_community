<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

// Set language source file
$ll = 'LLL:EXT:moox_community/Resources/Private/Language/locallang_db.xlf:';

/***************
 * Add extra fields to the be_users record
 */
$newBeUsersColumns = array(
    'custom_csv_export_fields' => array(
        'exclude' => 0,
        'label' => $ll.'be_users.custom_csv_export_fields',
        'config' => array(
            'type' => 'input',
            'size' => 40,
            'eval' => 'trim'
        ),
    ),
    'custom_csv_import_fields' => array(
        'exclude' => 0,
        'label' => $ll.'be_users.custom_csv_import_fields',
        'config' => array(
            'type' => 'input',
            'size' => 40,
            'eval' => 'trim'
        ),
    ),
);

// Add new TCA fields
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('be_users', $newBeUsersColumns, 1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('be_users', 'custom_csv_export_fields,custom_csv_import_fields', '', 'after:file_mountpoints');
