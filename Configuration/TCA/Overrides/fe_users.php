<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

// Get the extensions's configuration
$extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_community']);

// Set language source file
$ll = 'LLL:EXT:moox_community/Resources/Private/Language/locallang_db.xlf:';

// hide fe user default fields for custom fe user variant
$hideFields = 'image';

/***************
 * Add extra fields to the fe_users record
 */
$newFeUsersColumns = [
    /*'pid' => [
        'label' => $ll . 'tx_mooxcommunity_domain_model_frontenduser.pid',
        'config' => [
            'type' => 'passthrough'
        ]
    ],
    'tstamp' => [
        $ll . 'tx_mooxcommunity_domain_model_frontenduser.tstamp',
        'config' => [
            'type' => 'passthrough'
        ]
    ],
    'crdate' => [
        $ll . 'tx_mooxcommunity_domain_model_frontenduser.crdate',
        'config' => [
            'type' => 'passthrough'
        ]
    ],*/
    'variant' => [
        'exclude' => 1,
        'label'    => $ll . 'tx_mooxcommunity_domain_model_frontenduser.variant',
        'config' => [
            'type' => 'select',
            'items' => [
                [$ll . 'tx_mooxcommunity_domain_model_frontenduser.variant.default', 'fe_users'],
                [$ll . 'tx_mooxcommunity_domain_model_frontenduser.variant.community', 'moox_community']
            ],
            'size' => 1,
            'maxitems' => 1
        ],
        // special moox configuration
        'moox' => [
            'extkey' => 'moox_community'
        ]
    ],
    'gender' => [
        'exclude' => 1,
        'label'    => $ll . 'tx_mooxcommunity_domain_model_frontenduser.gender',
        'config' => [
            'type' => 'select',
            'items' => [
                [$ll . 'tx_mooxcommunity_domain_model_frontenduser.gender.none', 0],
                [$ll . 'tx_mooxcommunity_domain_model_frontenduser.gender.male', 1],
                [$ll . 'tx_mooxcommunity_domain_model_frontenduser.gender.female', 2]
            ],
            'size' => 1,
            'maxitems' => 1
        ],
        // special moox configuration
        'moox' => [
            'extkey' => 'moox_community',
            'plugins' => [
                'mooxcommunity' => [
                    'add', 'edit', 'editUser', 'detailUser'
                ]
            ],
            'sortable' => 0
        ]
    ],
    'info' => [
        'exclude' => 0,
        'label' => $ll . 'tx_mooxcommunity_domain_model_frontenduser.info',
        'displayCond' => 'FIELD:variant:=:moox_community',
        'config' => [
            'type' => 'text',
            'cols' => 40,
            'rows' => 10,
            'eval' => 'trim'
        ],
        'defaultExtras' => 'richtext[]',
        // special moox configuration
        'moox' => [
            'extkey' => 'moox_community',
            'plugins' => [
                'mooxcommunity' => [
                    'add', 'edit', 'editUser', 'detailUser'
                ]
            ],
            'sortable' => 0
        ]
    ],
    'fal_images' => [
        'exclude' => 0,
        'label' => $ll . 'tx_mooxcommunity_domain_model_frontenduser.images',
        'displayCond' => 'FIELD:variant:=:moox_community',
        'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
            'fal_images',
            [
                'reference' => 'image',
                'maxitems' => 3,
                'maxfilesize' => 20480,
                'accepts' => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
                'appearance' => [
                    'headerThumbnail' => [
                        'width' => '100',
                        'height' => '100'
                    ],
                    'createNewRelationLinkTitle' => $ll . 'tx_mooxcommunity_domain_model_frontenduser.add_images'
                ],
                // custom configuration for displaying fields in the overlay/reference table
                // to use the imageoverlayPalette instead of the basicoverlayPalette
                'foreign_types' => [
                    '0' => [
                        'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
                    ],
                    \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                        'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
                    ],
                    \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                        'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
                    ],
                    \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                        'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
                    ],
                    \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                        'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
                    ],
                    \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                        'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
                    ]
                ]
            ],
            $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
        ),
        // special moox configuration
        'moox' => [
            'extkey' => 'moox_community',
            'plugins' => [
                'mooxcommunity' => [
                    'add', 'edit', 'editUser', 'detailUser'
                ]
            ],
            'sortable' => 0
        ]
    ],
    'registered' => [
        'exclude' => 1,
        'l10n_mode' => 'mergeIfNotBlank',
        'label' => $ll . 'tx_mooxcommunity_domain_model_frontenduser.registered',
        'config' => [
            'type' => 'input',
            'size' => 13,
            'max' => 20,
            'eval' => 'datetime',
            'checkbox' => 0,
            'default' => 0,
            'range' => [
                'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
            ]
        ],
        // special moox configuration
        'moox' => [
            'extkey' => 'moox_community'
        ]
    ],
    'unregistered' => [
        'exclude' => 1,
        'l10n_mode' => 'mergeIfNotBlank',
        'label' => $ll . 'tx_mooxcommunity_domain_model_frontenduser.unregistered',
        'config' => [
            'type' => 'input',
            'size' => 13,
            'max' => 20,
            'eval' => 'datetime',
            'checkbox' => 0,
            'default' => 0,
            'range' => [
                'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
            ]
        ],
        // special moox configuration
        'moox' => [
            'extkey' => 'moox_community'
        ]
    ],
    'register_hash' => [
        'label' => $ll . 'tx_mooxcommunity_domain_model_frontenduser.register_hash',
        'config' => [
            'type' => 'passthrough'
        ],
        // special moox configuration
        'moox' => [
            'extkey' => 'moox_community'
        ]
    ],
    'register_tstamp' => [
        'label' => $ll . 'tx_mooxcommunity_domain_model_frontenduser.register_tstamp',
        'config' => [
            'type' => 'passthrough'
        ],
        // special moox configuration
        'moox' => [
            'extkey' => 'moox_community'
        ]
    ],
    'profile_values' => [
        'label' => $ll . 'tx_mooxcommunity_domain_model_frontenduser.profile_values',
        'config' => [
            'type' => 'passthrough'
        ],
        // special moox configuration
        'moox' => [
            'extkey' => 'moox_community'
        ]
    ],
    'profile_hash' => [
        'label' => $ll . 'tx_mooxcommunity_domain_model_frontenduser.profile_hash',
        'config' => [
            'type' => 'passthrough'
        ],
        // special moox configuration
        'moox' => [
            'extkey' => 'moox_community'
        ]
    ],
    'profile_tstamp' => [
        'label' => $ll . 'tx_mooxcommunity_domain_model_frontenduser.profile_tstamp',
        'config' => [
            'type' => 'passthrough'
        ],
        // special moox configuration
        'moox' => [
            'extkey' => 'moox_community'
        ]
    ],
    'password_recovery_hash' => [
        'label' => $ll . 'tx_mooxcommunity_domain_model_frontenduser.password_recovery_hash',
        'config' => [
            'type' => 'passthrough'
        ],
        // special moox configuration
        'moox' => [
            'extkey' => 'moox_community'
        ]
    ],
    'password_recovery_tstamp' => [
        'label' => $ll . 'tx_mooxcommunity_domain_model_frontenduser.password_recovery_tstamp',
        'config' => [
            'type' => 'passthrough'
        ],
        // special moox configuration
        'moox' => [
            'extkey' => 'moox_community'
        ]
    ],
    'requested_usergroup' => [
        'label' => $ll . 'tx_mooxcommunity_domain_model_frontenduser.requested_usergroup',
        'config' => [
            'type' => 'select',
            'foreign_table' => 'fe_groups',
            'foreign_table_where' => 'ORDER BY fe_groups.title',
            'size' => '6',
            'minitems' => '1',
            'maxitems' => '50'
        ],
        // special moox configuration
        'moox' => [
            'extkey' => 'moox_community',
            'plugins' => [
                'mooxcommunity' => [
                    'add'
                ]
            ],
            'sortable' => 0
        ]
    ],
    'sorted_usergroup' => [
        'label' => $ll . 'tx_mooxcommunity_domain_model_frontenduser.sorted_usergroup',
        'config' => [
            'type' => 'passthrough'
        ],
        // special moox configuration
        'moox' => [
            'extkey' => 'moox_community'
        ]
    ],
    'abo_membership' => [
        'label' => $ll . 'tx_mooxcommunity_domain_model_frontenduser.abo_membership',
        'config' => [
            'type' => 'inline',
            'foreign_table' => 'tx_mooxcommunity_domain_model_abomembership',
            'foreign_field' => 'fe_user',
            'maxitems' => 1000,
            'minitems' => 0,
            'appearance' => [
                'collapseAll' => 1,
                'expandSingle' => 1
            ]
        ]
    ]
];

// Add new TCA fields
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $newFeUsersColumns, true);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', 'fal_images', '', 'before:image');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', 'gender', '', 'before:name');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', 'variant', '', 'before:username');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', 'abo_membership', '', 'after:usergroup');

// Set image upload folder fallback
if ($extConf['imageUploadFolder'] == '') {
    $extConf['imageUploadFolder'] = 'uploads/tx_mooxcommunity';
}

// Set image upload folder
$GLOBALS['TCA']['fe_users']['columns']['image']['config']['uploadfolder'] = $extConf['imageUploadFolder'];

// extend existing fe user fields
$extendFields = [
    'username',
    'title',
    'first_name',
    'middle_name',
    'last_name',
    'company',
    'address',
    'zip',
    'city',
    'country',
    'telephone',
    'fax',
    'email',
    'www',
    'usergroup'
];
foreach ($extendFields as $field) {
    if (isset($GLOBALS['TCA']['fe_users']['columns'][$field])) {
        $GLOBALS['TCA']['fe_users']['columns'][$field]['moox'] = [
            'extkey' => 'moox_community',
            'plugins' => [
                'mooxcommunity' => [
                    'add', 'edit', 'listUsers', 'editUser', 'detailUser'
                ]
            ],
            'sortable' => 1
        ];
    }
}
$GLOBALS['TCA']['fe_users']['columns']['username']['config']['unique'] = 1;
$GLOBALS['TCA']['fe_users']['columns']['zip']['config']['range'] = [
    'lower' => 0000,
    'upper' => 99999
];

// remove community-groups from group selection
$GLOBALS['TCA']['fe_users']['columns']['usergroup']['config']['foreign_table_where'] =
    'AND variant != "moox_community" ' .
    $GLOBALS['TCA']['fe_users']['columns']['usergroup']['config']['foreign_table_where'];

// add variant field to update fields
if (isset($GLOBALS['TCA']['fe_users']['ctrl']['requestUpdate']) && $GLOBALS['TCA']['fe_users']['ctrl']['requestUpdate'] != '') {
    $GLOBALS['TCA']['fe_users']['ctrl']['requestUpdate'] = $GLOBALS['TCA']['fe_users']['ctrl']['requestUpdate'] . ',variant';
} else {
    $GLOBALS['TCA']['fe_users']['ctrl']['requestUpdate'] = 'variant';
}

// configure special sorting fields
$GLOBALS['TCA']['fe_users']['columns']['crdate']['moox'] = [
    'extkey' => 'moox_community',
    'plugins' => [
        'mooxcommunity' => [
            'list', 'listUsers', 'detail', 'detailUser'
        ]
    ],
    'sortable' => 1
];
$GLOBALS['TCA']['fe_users']['columns']['tstamp']['moox'] = [
    'extkey' => 'moox_community',
    'plugins' => [
        'mooxcommunity' => [
            'list', 'listUsers', 'detail', 'detailUser'
        ],
    ],
    'sortable' => 1,
];
$GLOBALS['TCA']['fe_users']['columns']['password']['moox'] = [
    'extkey' => 'moox_community',
    'plugins' => [
        'mooxcommunity' => [
            'add', 'edit'
        ],
    ],
    'sortable' => 0,
];

// hide fe user default fields defined in hide fields string
\DCNGmbH\MooxMarketplace\Service\HelperService::tcaHideDefaultFields($hideFields, 'fe_users', 'moox_community', '!=');
