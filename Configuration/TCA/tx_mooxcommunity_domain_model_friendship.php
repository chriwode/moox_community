<?php
// Set language source file
$ll = 'LLL:EXT:moox_community/Resources/Private/Language/locallang_db.xlf:';

return array(
    'ctrl' => array(
        'title'    => $ll.'tx_mooxcommunity_domain_model_friendship',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'sortby' => 'sorting',
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'searchFields' => 'title,subject,template',
        'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('moox_community').'Resources/Public/Icons/tx_mooxcommunity_domain_model_friendship.gif',
        'hideTable' => false,
    ),
    'interface' => array(
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, title, fe_user1, fe_user2, confirmed, rejected',
    ),
    'types' => array(
        '1' => array('showitem' => 'title, fe_user1, fe_user2, confirmed, rejected'),
    ),
    'palettes' => array(
        '1' => array('showitem' => ''),
    ),
    'columns' => array(
        'sys_language_uid' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => array(
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => array(
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
                ),
            ),
        ),
        'l10n_parent' => array(
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => array(
                'type' => 'select',
                'items' => array(
                    array('', 0),
                ),
                'foreign_table' => 'tx_mooxcommunity_domain_model_friendship',
                'foreign_table_where' => 'AND tx_mooxcommunity_domain_model_friendship.pid=###CURRENT_PID### AND tx_mooxcommunity_domain_model_friendship.sys_language_uid IN (-1,0)',
            ),
        ),
        'l10n_diffsource' => array(
            'config' => array(
                'type' => 'passthrough',
            ),
        ),
        't3ver_label' => array(
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            )
        ),
        'title' => array(
            'exclude' => 0,
            'label' => $ll.'tx_mooxcommunity_domain_model_friendship.title',
            'config' => array(
                'type' => 'input',
                'size' => 40,
                'eval' => 'required,trim'
            ),
        ),
        'confirmed' => array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => $ll.'tx_mooxcommunity_domain_model_friendship.confirmed',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ),
            ),
        ),
        'rejected' => array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => $ll.'tx_mooxcommunity_domain_model_friendship.rejected',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ),
            ),
        ),
        'fe_user1' => array(
            'exclude' => 0,
            'label' => $ll.'tx_mooxcommunity_domain_model_friendship.fe_user1',
            'config' => array(
                'type' => 'select',
                'foreign_table' => 'fe_users',
                'foreign_table_where' => 'AND variant="moox_community" ORDER BY fe_users.username',
                'size' => 1,
                'maxitems' => 1,
                'minitems' => 1,
                'multiple' => 0,
            ),
        ),
        'fe_user2' => array(
            'exclude' => 0,
            'label' => $ll.'tx_mooxcommunity_domain_model_friendship.fe_user2',
            'config' => array(
                'type' => 'select',
                'foreign_table' => 'fe_users',
                'foreign_table_where' => 'AND variant="moox_community" ORDER BY fe_users.username',
                'size' => 1,
                'maxitems' => 1,
                'minitems' => 1,
                'multiple' => 0,
            ),
        ),
    ),
);
