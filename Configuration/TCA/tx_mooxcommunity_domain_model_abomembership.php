<?php
// Set language source file
$ll = 'LLL:EXT:moox_community/Resources/Private/Language/locallang_db.xlf:';
$llBe = 'LLL:EXT:moox_community/Resources/Private/Language/locallang_be.xlf:';
$iconPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('moox_community');

return [
    'ctrl' => [
        'title' => $ll . 'tx_mooxcommunity_domain_model_abomembership',
        'label' => 'abo',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'sortby' => 'sorting',
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'searchFields' => 'title,subject,template',
        'iconfile' => $iconPath . 'Resources/Public/Icons/tx_mooxcommunity_domain_model_abomembership.gif',
        'hideTable' => true,
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime'
        ]
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, fe_user, abo, abo_begin, abo_end,'
            . ' abo_storno, abo_storno_date, allowed_quota_event_note, allowed_quota_marketplace, used_event_note,'
            . ' used_marketplace',
    ],
    'types' => [
        '0' => [
            'showitem' => '
                --palette--;;paletteCore,
                --palette--;;paletteAbo,
                --div--;' . $llBe . 'tx_mooxcommunity_domain_model_abomembership.tab.quota,
                --palette--;' . $llBe . 'tx_mooxcommunity_domain_model_abomembership.palette.quota;paletteQuota,
                --div--;LLL:EXT:cms/locallang_tca.xlf:pages.tabs.access,
                --palette--;LLL:EXT:cms/locallang_tca.xlf:pages.palettes.visibility;paletteAccess,'
        ]
    ],
    'palettes' => [
        'paletteCore' => [
            'showitem' => 'abo, hidden,'
        ],
        'paletteAccess' => [
            'showitem' => 'starttime;LLL:EXT:cms/locallang_tca.xlf:pages.starttime_formlabel,'
                . 'endtime;LLL:EXT:cms/locallang_tca.xlf:pages.endtime_formlabel',
        ],
        'paletteAbo' => [
            'showitem' => 'abo_begin, abo_end, --linebreak--, abo_storno_date, abo_storno'
        ],
        'paletteQuota' => [
            'showitem' => 'allowed_quota_event_note, allowed_quota_marketplace, --linebreak--,'
                . 'used_event_note, used_marketplace'
        ]
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => [
                    ['LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1],
                    ['LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0]
                ]
            ]
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_mooxcommunity_domain_model_abomembership',
                'foreign_table_where' => 'AND tx_mooxcommunity_domain_model_abomembership.pid=###CURRENT_PID###'
                    . ' AND tx_mooxcommunity_domain_model_abomembership.sys_language_uid IN (-1,0)'
            ]
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255
            ]
        ],
        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
            ],
        ],
        'cruser_id' => [
            'label' => 'cruser_id',
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'pid' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'crdate' => [
            'label' => 'crdate',
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'tstamp' => [
            'label' => 'tstamp',
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'starttime' => [
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ]
            ]
        ],
        'endtime' => [
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ]
            ]
        ],
        'fe_user' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'abo' => [
            'exclude' => 0,
            'label' => $ll . 'tx_mooxcommunity_domain_model_abomembership.abo',
            'config' => [
                'type' => 'input',
                'size' => 40,
                'eval' => 'required,trim'
            ]
        ],
        'abo_begin' => [
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => $ll . 'tx_mooxcommunity_domain_model_abomembership.abo_begin',
            'config' => [
                'type' => 'input',
                'size' => 10,
                'max' => 10,
                'eval' => 'date,required',
                'default' => 0
            ]
        ],
        'abo_end' => [
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => $ll . 'tx_mooxcommunity_domain_model_abomembership.abo_end',
            'config' => [
                'type' => 'input',
                'size' => 10,
                'max' => 10,
                'eval' => 'date',
                'default' => 0
            ]
        ],
        'abo_storno' => [
            'exclude' => 0,
            'label' => $ll . 'tx_mooxcommunity_domain_model_abomembership.abo_storno',
            'config' => [
                'type' => 'check'
            ]
        ],
        'abo_storno_date' => [
            'exclude' => 0,
            'label' => $ll . 'tx_mooxcommunity_domain_model_abomembership.abo_storno_date',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'max' => 10,
                'eval' => 'date',
                'default' => 0
            ]
        ],
        'allowed_quota_event_note' => [
            'exclude' => 0,
            'label' => $ll . 'tx_mooxcommunity_domain_model_abomembership.allowed_quota_event_note',
            'config' => [
                'readOnly' => true,
                'type' => 'input',
                'size' => 15
            ]
        ],
        'allowed_quota_marketplace' => [
            'exclude' => 0,
            'label' => $ll . 'tx_mooxcommunity_domain_model_abomembership.allowed_quota_marketplace',
            'config' => [
                'readOnly' => true,
                'type' => 'input',
                'size' => 15
            ]
        ],
        'used_event_note' => [
            'exclude' => 0,
            'label' => $ll . 'tx_mooxcommunity_domain_model_abomembership.used_event_note',
            'config' => [
                'readOnly' => true,
                'type' => 'input',
                'size' => 15
            ]
        ],
        'used_marketplace' => [
            'exclude' => 0,
            'label' => $ll . 'tx_mooxcommunity_domain_model_abomembership.used_marketplace',
            'config' => [
                'readOnly' => true,
                'type' => 'input',
                'size' => 15
            ]
        ]
    ]
];
