$(document).ready(function(){	
	mooxCommunityInitClearButtons();	
	mooxCommunityInitComplexify();
	mooxCommunityInitFileinput();
	mooxCommunityInitTooltips();
	mooxCommunityInitDropdowns();
	mooxCommunityInitClientValidation();
	
	$('.tx-moox-community-pi4 a.add-membership').on('click', function(event) {
		
		event.preventDefault();
		
		plugin = $(this).parents('.tx-moox-community').first().attr('id');
		id = $(this).attr("id");
		href = $(this).attr("href");
		group = $(this).data("group");
		header = mooxCommunityLang['de'].header.add_membership;
		text = mooxCommunityLang['de'].questions.add_membership.replace("%1",group);
		button = mooxCommunityLang['de'].buttons.add_membership;
		
		mooxCommunityShowModal(plugin,id,header,text,button,href);
		
		return false;
	});
	
	$('.tx-moox-community-pi4 a.remove-membership').on('click', function(event) {
		
		event.preventDefault();
		
		plugin = $(this).parents('.tx-moox-community').first().attr('id');
		id = $(this).attr("id");
		href = $(this).attr("href");
		group = $(this).data("group");
		header = mooxCommunityLang['de'].header.remove_membership;
		text = mooxCommunityLang['de'].questions.remove_membership.replace("%1",group);
		button = mooxCommunityLang['de'].buttons.remove_membership;
		
		mooxCommunityShowModal(plugin,id,header,text,button,href);
		
		return false;
	});
	
	$('.tx-moox-community-pi4 a.add-friendship').on('click', function(event) {
		
		event.preventDefault();
		
		plugin = $(this).parents('.tx-moox-community').first().attr('id');
		id = $(this).attr("id");
		href = $(this).attr("href");
		user = $(this).data("user");
		friend = $(this).data("friend");
		header = mooxCommunityLang['de'].header.add_friendship;
		text = mooxCommunityLang['de'].questions.add_friendship.replace("%1",user);
		button = mooxCommunityLang['de'].buttons.add_friendship;
		
		mooxCommunityShowModal(plugin,id,header,text,button,href);
		
		return false;
	});
	
	$('.tx-moox-community-pi4 a.remove-friendship').on('click', function(event) {
		
		event.preventDefault();
		
		plugin = $(this).parents('.tx-moox-community').first().attr('id');
		id = $(this).attr("id");
		href = $(this).attr("href");
		user = $(this).data("user");
		friend = $(this).data("friend");
		header = mooxCommunityLang['de'].header.remove_friendship;
		text = mooxCommunityLang['de'].questions.remove_friendship.replace("%1",user);
		button = mooxCommunityLang['de'].buttons.remove_friendship;
		
		mooxCommunityShowModal(plugin,id,header,text,button,href);
		
		return false;
	});
	
});
