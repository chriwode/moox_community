$(document).ready(function(){	
	mooxCommunityInitTooltips();
	mooxCommunityInitDropdowns();
	
	$('.tx-moox-community-pi5 a.confirm-membership').on('click', function(event) {
		
		event.preventDefault();
		
		plugin = $(this).parents('.tx-moox-community').first().attr('id');
		id = $(this).attr("id");
		href = $(this).attr("href");
		user = $(this).data("user");
		group = $(this).data("group");
		header = mooxCommunityLang['de'].header.confirm_membership;
		text = mooxCommunityLang['de'].questions.confirm_membership.replace("%1",user).replace("%2",group);
		button = mooxCommunityLang['de'].buttons.confirm_membership;
		
		mooxCommunityShowModal(plugin,id,header,text,button,href);
		
		return false;
	});
	
	$('.tx-moox-community-pi5 a.reject-membership').on('click', function(event) {
		
		event.preventDefault();
		
		plugin = $(this).parents('.tx-moox-community').first().attr('id');
		id = $(this).attr("id");
		href = $(this).attr("href");
		user = $(this).data("user");
		group = $(this).data("group");
		header = mooxCommunityLang['de'].header.reject_membership;
		text = mooxCommunityLang['de'].questions.reject_membership.replace("%1",user).replace("%2",group);
		button = mooxCommunityLang['de'].buttons.reject_membership;
		
		mooxCommunityShowModal(plugin,id,header,text,button,href);
		
		return false;
	});
	
	$('.tx-moox-community-pi5 a.remove-membership-admin').on('click', function(event) {
		
		event.preventDefault();
		
		plugin = $(this).parents('.tx-moox-community').first().attr('id');
		id = $(this).attr("id");
		href = $(this).attr("href");
		user = $(this).data("user");
		group = $(this).data("group");
		header = mooxCommunityLang['de'].header.remove_membership_admin;
		text = mooxCommunityLang['de'].questions.remove_membership_admin.replace("%1",user).replace("%2",group);
		button = mooxCommunityLang['de'].buttons.remove_membership_admin;
		
		mooxCommunityShowModal(plugin,id,header,text,button,href);
		
		return false;
	});
	
	$('.tx-moox-community-pi5 a.remove-membership-user').on('click', function(event) {
		
		event.preventDefault();
		
		plugin = $(this).parents('.tx-moox-community').first().attr('id');
		id = $(this).attr("id");
		href = $(this).attr("href");
		user = $(this).data("user");
		group = $(this).data("group");
		header = mooxCommunityLang['de'].header.remove_membership_user;
		text = mooxCommunityLang['de'].questions.remove_membership_user.replace("%1",group);
		button = mooxCommunityLang['de'].buttons.remove_membership_user;
		
		mooxCommunityShowModal(plugin,id,header,text,button,href);
		
		return false;
	});
	
	$('.tx-moox-community-pi5 a.add-moderator-admin').on('click', function(event) {
		
		event.preventDefault();
		
		plugin = $(this).parents('.tx-moox-community').first().attr('id');
		id = $(this).attr("id");
		href = $(this).attr("href");
		user = $(this).data("user");
		group = $(this).data("group");
		header = mooxCommunityLang['de'].header.add_moderator_admin;
		text = mooxCommunityLang['de'].questions.add_moderator_admin.replace("%1",user).replace("%2",group);
		button = mooxCommunityLang['de'].buttons.add_moderator_admin;
		
		mooxCommunityShowModal(plugin,id,header,text,button,href);
		
		return false;
	});
	
	$('.tx-moox-community-pi5 a.remove-moderator-admin').on('click', function(event) {
		
		event.preventDefault();
		
		plugin = $(this).parents('.tx-moox-community').first().attr('id');
		id = $(this).attr("id");
		href = $(this).attr("href");
		user = $(this).data("user");
		group = $(this).data("group");
		header = mooxCommunityLang['de'].header.remove_moderator_admin;
		text = mooxCommunityLang['de'].questions.remove_moderator_admin.replace("%1",user).replace("%2",group);
		button = mooxCommunityLang['de'].buttons.remove_moderator_admin;
		
		mooxCommunityShowModal(plugin,id,header,text,button,href);
		
		return false;
	});
	
	$('.tx-moox-community-pi5 a.confirm-friendship').on('click', function(event) {
		
		event.preventDefault();
		
		plugin = $(this).parents('.tx-moox-community').first().attr('id');
		id = $(this).attr("id");
		href = $(this).attr("href");
		user = $(this).data("user");		
		header = mooxCommunityLang['de'].header.confirm_friendship;
		text = mooxCommunityLang['de'].questions.confirm_friendship.replace("%1",user);
		button = mooxCommunityLang['de'].buttons.confirm_friendship;
		
		mooxCommunityShowModal(plugin,id,header,text,button,href);
		
		return false;
	});
	
	$('.tx-moox-community-pi5 a.reject-friendship').on('click', function(event) {
		
		event.preventDefault();
		
		plugin = $(this).parents('.tx-moox-community').first().attr('id');
		id = $(this).attr("id");
		href = $(this).attr("href");
		user = $(this).data("user");		
		header = mooxCommunityLang['de'].header.reject_friendship;
		text = mooxCommunityLang['de'].questions.reject_friendship.replace("%1",user);
		button = mooxCommunityLang['de'].buttons.reject_friendship;
		
		mooxCommunityShowModal(plugin,id,header,text,button,href);
		
		return false;
	});
	
	$('.tx-moox-community-pi5 a.remove-friendship').on('click', function(event) {
		
		event.preventDefault();
		
		plugin = $(this).parents('.tx-moox-community').first().attr('id');
		id = $(this).attr("id");
		href = $(this).attr("href");
		user = $(this).data("user");		
		header = mooxCommunityLang['de'].header.remove_friendship;
		text = mooxCommunityLang['de'].questions.remove_friendship.replace("%1",user);
		button = mooxCommunityLang['de'].buttons.remove_friendship;
		
		mooxCommunityShowModal(plugin,id,header,text,button,href);
		
		return false;
	});
	
});
