<?php
namespace DCNGmbH\MooxCommunity\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Saltedpasswords\Salt\SaltFactory;
use TYPO3\CMS\Saltedpasswords\Utility\SaltedPasswordsUtility;

/**
 *
 *
 * @package moox_community
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FrontendUser extends \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
{
        
    /**
     * uid
     *
     * @var int
     */
    protected $uid;
    
    /**
     * pid
     *
     * @var int
     */
    protected $pid;
    
    /**
     * tstamp
     *
     * @var int
     */
    protected $tstamp;
    
    /**
     * starttime
     *
     * @var int
     */
    protected $starttime;
    
    /**
     * endtime
     *
     * @var int
     */
    protected $endtime;
    
    /**
     * crdate
     *
     * @var int
     */
    protected $crdate;
    
    /**
     * lastlogin
     *
     * @var int
     */
    protected $lastlogin;
    
    /**
     * gender
     *
     * @var int
     */
    protected $gender;
    
    /**
     * info
     *
     * @var string
     */
    protected $info;
    
    /**
     * variant
     *
     * @var string
     */
    protected $variant;
    
    /**
     * username
     *
     * @var string
     */
    protected $username;
    
    /**
     * disallow mailing
     *
     * @var bool
     */
    protected $disallowMailing;
    
    /**
     * fal images to use in the gallery
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxCommunity\Domain\Model\UserImageReference>
     * @lazy
     */
    protected $falImages;
    
    /**
     * Sichtbarkeit
     *
     * @var int
     */
    protected $disable;
    
    /**
     * registered
     *
     * @var int
     */
    protected $registered;
    
    /**
     * unregistered
     *
     * @var int
     */
    protected $unregistered;
    
    /**
     * password recovery hash
     *
     * @var string
     */
    protected $passwordRecoveryHash;
    
    /**
     * password recovery timestamp
     *
     * @var int
     */
    protected $passwordRecoveryTstamp;
    
    /**
     * register hash
     *
     * @var string
     */
    protected $registerHash;
    
    /**
     * register timestamp
     *
     * @var int
     */
    protected $registerTstamp;
    
    /**
     * profile values
     *
     * @var string
     */
    protected $profileValues;
    
    /**
     * profile hash
     *
     * @var string
     */
    protected $profileHash;
    
    /**
     * profile timestamp
     *
     * @var int
     */
    protected $profileTstamp;
    
    /**
     * sorted usergroup
     *
     * @var array
     */
    protected $sortedUsergroup;
    
    /**
     * quality
     *
     * @var int
     */
    protected $quality;
    
    /**
     * auto name
     *
     * @var string
     */
    protected $autoName;
    
    /**
     * requested usergroup
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxCommunity\Domain\Model\FrontendUserGroup>
     */
    protected $requestedUsergroup = null;
    
    /**
     * bounces
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMailer\Domain\Model\Bounce>
     */
    protected $bounces = null;
    
    /**
     * errors
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMailer\Domain\Model\Error>
     */
    protected $errors = null;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxCommunity\Domain\Model\AboMembership>
     */
    protected $aboMembership;
      
    /**
     * initialize object
     */
    public function initializeObject()
    {
        parent::initializeObject();
        $this->initStorageObjects();
    }
  
    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->requestedUsergroup = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->bounces = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->errors = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->falImages = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->aboMembership = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }
    
    /**
     * get uid
     *
     * @return int $uid uid
     */
    public function getUid()
    {
        return $this->uid;
    }
     
    /**
     * set uid
     *
     * @param int $uid uid
     * @return void
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }
    
    /**
     * get pid
     *
     * @return int $pid pid
     */
    public function getPid()
    {
        return $this->pid;
    }
     
    /**
     * set pid
     *
     * @param int $pid pid
     * @return void
     */
    public function setPid($pid)
    {
        $this->pid = $pid;
    }
    
    /**
     * get tstamp
     *
     * @return int $tstamp tstamp
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }
     
    /**
     * set tstamp
     *
     * @param int $tstamp tstamp
     * @return void
     */
    public function setTstamp($tstamp)
    {
        $this->tstamp = $tstamp;
    }
    
    /**
     * get starttime
     *
     * @return int $starttime starttime
     */
    public function getStarttime()
    {
        return $this->starttime;
    }
     
    /**
     * set starttime
     *
     * @param int $starttime starttime
     * @return void
     */
    public function setStarttime($starttime)
    {
        $this->starttime = $starttime;
    }
    
    /**
     * get endtime
     *
     * @return int $endtime endtime
     */
    public function getEndtime()
    {
        return $this->endtime;
    }
     
    /**
     * set endtime
     *
     * @param int $endtime endtime
     * @return void
     */
    public function setEndtime($endtime)
    {
        $this->endtime = $endtime;
    }
    
    /**
     * get crdate
     *
     * @return int $crdate crdate
     */
    public function getCrdate()
    {
        return $this->crdate;
    }
     
    /**
     * set crdate
     *
     * @param int $crdate crdate
     * @return void
     */
    public function setCrdate($crdate)
    {
        $this->crdate = $crdate;
    }
    
    /**
     * Get year of crdate
     *
     * @return int
     */
    public function getYearOfCrdate()
    {
        return $this->getCrdate()->format('Y');
    }

    /**
     * Get month of crdate
     *
     * @return int
     */
    public function getMonthOfCrdate()
    {
        return $this->getCrdate()->format('m');
    }

    /**
     * Get day of crdate
     *
     * @return int
     */
    public function getDayOfCrdate()
    {
        return (int)$this->crdate->format('d');
    }
    
    /**
     * get lastlogin
     *
     * @return int $lastlogin lastlogin
     */
    public function getLastlogin()
    {
        return $this->lastlogin;
    }
     
    /**
     * set lastlogin
     *
     * @param int $lastlogin lastlogin
     * @return void
     */
    public function setLastlogin($lastlogin)
    {
        $this->lastlogin = $lastlogin;
    }
    
    /**
     * get gender
     *
     * @return int $gender gender
     */
    public function getGender()
    {
        return $this->gender;
    }
     
    /**
     * set gender
     *
     * @param int $gender gender
     * @return void
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }
    
    /**
     * get username
     *
     * @return string $username username
     */
    public function getUsername()
    {
        return $this->username;
    }
     
    /**
     * set username
     *
     * @param string $username username
     * @return void
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }
    
    /**
     * get disallow mailing
     *
     * @return bool $disallowMailing disallow mailing
     */
    public function getDisallowMailing()
    {
        return $this->disallowMailing;
    }
     
    /**
     * set disallow mailing
     *
     * @param bool $disallowMailing disallow mailing
     * @return void
     */
    public function setDisallowMailing($disallowMailing)
    {
        $this->disallowMailing = $disallowMailing;
    }
    
    /**
     * sets the fal images
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $falImages
     * @return void
     */
    public function setFalImages($falImages)
    {
        $this->falImages = $falImages;
    }
     
    /**
     * get the fal images
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getFalImages()
    {
        return $this->falImages;
    }

    /**
     * adds a fal image
     *
     * @param \DCNGmbH\MooxCommunity\Domain\Model\UserImageReference $falImage
     * @return void
     */
    public function addFalImages(\DCNGmbH\MooxCommunity\Domain\Model\UserImageReference $falImage)
    {
        $this->falImages->attach($falImage);
    }
    
    /**
     * remove a fal image
     *
     * @param \DCNGmbH\MooxCommunity\Domain\Model\UserImageReference $falImage
     * @return void
     */
    public function removeFalImages(\DCNGmbH\MooxCommunity\Domain\Model\UserImageReference $falImage)
    {
        $this->falImages->detach($falImage);
    }
    
    /**
     * Returns the disable
     *
     * @return int $disable
     */
    public function getDisable()
    {
        return $this->disable;
    }

    /**
     * Sets the disable
     *
     * @param int $disable
     * @return void
     */
    public function setDisable($disable)
    {
        $this->disable = $disable;
    }
    
    /**
     * Setter for password. As we have encrypted passwords here, we need to encrypt them before storing!
     *
     * @param string $password
     * @param bool $plain
     * @return void
     */
    public function setPassword($password, $plain = false)
    {
        if ($plain) {
            $this->password = $password;
        } else {
            if (ExtensionManagementUtility::isLoaded('saltedpasswords')) {
                if (SaltedPasswordsUtility::isUsageEnabled('FE')) {
                    $objSalt = SaltFactory::getSaltingInstance(null);
                    if (is_object($objSalt)) {
                        $password = $objSalt->getHashedPassword($password);
                    }
                }
                $this->password = $password;
            } else {
                parent::setPassword($password);
            }
        }
    }
    
    /**
     * get registered
     *
     * @return int $registered registered
     */
    public function getRegistered()
    {
        return $this->registered;
    }
     
    /**
     * set registered
     *
     * @param int $registered registered
     * @return void
     */
    public function setRegistered($registered)
    {
        $this->registered = $registered;
    }
    
    /**
     * get unregistered
     *
     * @return int $unregistered unregistered
     */
    public function getUnregistered()
    {
        return $this->unregistered;
    }
     
    /**
     * set unregistered
     *
     * @param int $unregistered unregistered
     * @return void
     */
    public function setUnregistered($unregistered)
    {
        $this->unregistered = $unregistered;
    }
    
    /**
     * get register hash
     *
     * @return string $registerHash register hash
     */
    public function getRegisterHash()
    {
        return $this->registerHash;
    }
     
    /**
     * set register hash
     *
     * @param string $registerHash register hash
     * @return void
     */
    public function setRegisterHash($registerHash)
    {
        $this->registerHash = $registerHash;
    }
    
    /**
     * get register timestamp
     *
     * @return int $registerTstamp register timestamp
     */
    public function getRegisterTstamp()
    {
        return $this->registerTstamp;
    }
     
    /**
     * set register timestamp
     *
     * @param int $registerTstamp register timestamp
     * @return void
     */
    public function setRegisterTstamp($registerTstamp)
    {
        $this->registerTstamp = $registerTstamp;
    }
    
    /**
     * get profile values
     *
     * @return string $profileValues profile values
     */
    public function getProfileValues()
    {
        return $this->profileValues;
    }
     
    /**
     * set profile values
     *
     * @param string $profileValues profile values
     * @return void
     */
    public function setProfileValues($profileValues)
    {
        $this->profileValues = $profileValues;
    }
    
    /**
     * get profile hash
     *
     * @return string $profileHash profile hash
     */
    public function getProfileHash()
    {
        return $this->profileHash;
    }
     
    /**
     * set profile hash
     *
     * @param string $profileHash profile hash
     * @return void
     */
    public function setProfileHash($profileHash)
    {
        $this->profileHash = $profileHash;
    }
    
    /**
     * get profile timestamp
     *
     * @return int $profileTstamp profile timestamp
     */
    public function getProfileTstamp()
    {
        return $this->profileTstamp;
    }
     
    /**
     * set profile timestamp
     *
     * @param int $profileTstamp profile timestamp
     * @return void
     */
    public function setProfileTstamp($profileTstamp)
    {
        $this->profileTstamp = $profileTstamp;
    }
    
    /**
     * get password recovery hash
     *
     * @return string $passwordRecoveryHash password recovery hash
     */
    public function getPasswordRecoveryHash()
    {
        return $this->passwordRecoveryHash;
    }
     
    /**
     * set password recovery hash
     *
     * @param string $passwordRecoveryHash password recovery hash
     * @return void
     */
    public function setPasswordRecoveryHash($passwordRecoveryHash)
    {
        $this->passwordRecoveryHash = $passwordRecoveryHash;
    }
    
    /**
     * get password recovery timestamp
     *
     * @return int $passwordRecoveryTstamp password recovery timestamp
     */
    public function getPasswordRecoveryTstamp()
    {
        return $this->passwordRecoveryTstamp;
    }
    
    /**
     * set info
     *
     * @param string $info info
     * @return void
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }
    
    /**
     * get info
     *
     * @return string $info info
     */
    public function getInfo()
    {
        return $this->info;
    }
    
    /**
     * set variant
     *
     * @param string $variant variant
     * @return void
     */
    public function setVariant($variant)
    {
        $this->variant = $variant;
    }
    
    /**
     * get variant
     *
     * @return string $variant variant
     */
    public function getVariant()
    {
        return $this->variant;
    }
     
    /**
     * set password recovery timestamp
     *
     * @param int $passwordRecoveryTstamp password recovery timestamp
     * @return void
     */
    public function setPasswordRecoveryTstamp($passwordRecoveryTstamp)
    {
        $this->passwordRecoveryTstamp = $passwordRecoveryTstamp;
    }
        
    /**
     * get sorted usergroup
     *
     * @return array
     */
    public function getSortedUsergroup()
    {
        $user = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('usergroup', 'fe_users', 'uid=' . $this->uid);
        if (!is_null($user) && $user['usergroup'] != '' && $user['usergroup'] != 0) {
            $objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
            $frontendUserGroupRepository = $objectManager->get('DCNGmbH\\MooxCommunity\\Domain\\Repository\\FrontendUserGroupRepository');
            $return = [];
            foreach (GeneralUtility::intExplode(',', $user['usergroup']) as $uid) {
                $group = $frontendUserGroupRepository->findByUid($uid, false);
                if (is_object($group)) {
                    $return[] = $group;
                }
            }
            return $return;
        } else {
            return [];
        }
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $requestedUsergroup
     * @return void
     */
    public function setRequestedUsergroup(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $requestedUsergroup)
    {
        $this->requestedUsergroup = $requestedUsergroup;
    }

    /**
     * @param \DCNGmbH\MooxCommunity\Domain\Model\FrontendUserGroup $requestedUsergroup
     * @return void
     */
    public function addRequestedUsergroup(\DCNGmbH\MooxCommunity\Domain\Model\FrontendUserGroup  $requestedUsergroup)
    {
        $this->requestedUsergroup->attach($requestedUsergroup);
    }

    /**
     * @param \DCNGmbH\MooxCommunity\Domain\Model\FrontendUserGroup $requestedUsergroup
     * @return void
     */
    public function removeRequestedUsergroup(\DCNGmbH\MooxCommunity\Domain\Model\FrontendUserGroup  $requestedUsergroup)
    {
        $this->requestedUsergroup->detach($requestedUsergroup);
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getRequestedUsergroup()
    {
        return $this->requestedUsergroup;
    }
    
    /**
     * get quality
     *
     * @return int $quality quality
     */
    public function getQuality()
    {
        return $this->quality;
    }
    
    /**
     * get auto name
     *
     * @return int $autoName auto name
     */
    public function getAutoName()
    {
        $autoName = $this->firstName.' '.$this->lastName.' ('.$this->username.')';
        return $autoName;
    }
    
    /**
     * Returns the bounces
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMailer\Domain\Model\Bounce> $bounces
     */
    public function getBounces()
    {
        return $this->bounces;
    }
        
    /**
     * Returns the errors
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMailer\Domain\Model\Error> $errors
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Returns the all active abo subscriptions as object storage
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getAboMembership()
    {
        return $this->aboMembership;
    }

    /**
     * Sets the abo subscription object storage
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $aboMembership
     * @return void
     */
    public function setAboMembership($aboMembership)
    {
        $this->aboMembership = $aboMembership;
    }

    /**
     * Add an abo subscription
     *
     * @param \DCNGmbH\MooxCommunity\Domain\Model\AboMembership $aboMembership
     * @return void
     */
    public function addAboMembership(AboMembership $aboMembership)
    {
        if (is_null($this->aboMembership)) {
            $this->aboMembership = new ObjectStorage();
        }

        $this->aboMembership->attach($aboMembership);
    }
}
