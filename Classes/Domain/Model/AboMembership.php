<?php

namespace DCNGmbH\MooxCommunity\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Christian Wolfram <c.wolfram@chriwo.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Class AboMembership
 *
 * @package moox_community
 */
class AboMembership extends AbstractEntity
{
    /**
     * @var string
     */
    protected $abo;

    /**
     * @var \DateTime
     */
    protected $aboBegin;

    /**
     * @var \DateTime
     */
    protected $aboEnd;

    /**
     * @var bool
     */
    protected $aboStorno;

    /**
     * @var \DateTime
     */
    protected $aboStornoDate;

    /**
     * @var int
     */
    protected $allowedQuotaEventNote;

    /**
     * @var int
     */
    protected $allowedQuotaMarketplace;

    /**
     * @var int
     */
    protected $usedEventNote;

    /**
     * @var int
     */
    protected $usedMarketplace;

    /**
     * Returns the name of subscription
     *
     * @return string
     */
    public function getAbo()
    {
        return $this->abo;
    }

    /**
     * Sets the name ob subscription
     *
     * @param string $abo
     * @return void
     */
    public function setAbo($abo)
    {
        $this->abo = $abo;
    }

    /**
     * Returns the subscription begin date
     *
     * @return \DateTime
     */
    public function getAboBegin()
    {
        return $this->aboBegin;
    }

    /**
     * Sets the subscription begin date
     *
     * @param \DateTime $aboBegin
     * @return void
     */
    public function setAboBegin($aboBegin)
    {
        $this->aboBegin = $aboBegin;
    }

    /**
     * Sets the subscription end date
     *
     * @return \DateTime
     */
    public function getAboEnd()
    {
        return $this->aboEnd;
    }

    /**
     * Returns the date of subscription end
     *
     * @param \DateTime $aboEnd
     * @return void
     */
    public function setAboEnd($aboEnd)
    {
        $this->aboEnd = $aboEnd;
    }

    /**
     * Returns the flag of cancellation
     *
     * @return bool
     */
    public function getAboStorno()
    {
        return $this->aboStorno;
    }

    /**
     * Set the flag for cancellation
     *
     * @param bool $aboStorno
     * @return void
     */
    public function setAboStorno($aboStorno)
    {
        $this->aboStorno = $aboStorno;
    }

    /**
     * Returns the cancellation date of subscription
     *
     * @return \DateTime
     */
    public function getAboStornoDate()
    {
        return $this->aboStornoDate;
    }

    /**
     * Sets the cancellation date of subscription
     *
     * @param \DateTime $aboStornoDate
     * @return void
     */
    public function setAboStornoDate($aboStornoDate)
    {
        $this->aboStornoDate = $aboStornoDate;
    }

    /**
     * Returns the available quota event note from frontend user group
     *
     * @return int
     */
    public function getAllowedQuotaEventNote()
    {
        return $this->allowedQuotaEventNote;
    }

    /**
     * Sets the available quota event note from frontend user group
     *
     * @param int $allowedQuotaEventNote
     * @return void
     */
    public function setAllowedQuotaEventNote($allowedQuotaEventNote)
    {
        $this->allowedQuotaEventNote = $allowedQuotaEventNote;
    }

    /**
     * Returns the available marketplace quota from frontend user group
     *
     * @return int
     */
    public function getAllowedQuotaMarketplace()
    {
        return $this->allowedQuotaMarketplace;
    }

    /**
     * Sets the available marketplace quota from frontend user group
     *
     * @param int $allowedQuotaMarketplace
     * @return void
     */
    public function setAllowedQuotaMarketplace($allowedQuotaMarketplace)
    {
        $this->allowedQuotaMarketplace = $allowedQuotaMarketplace;
    }

    /**
     * Returns the used event note quota
     *
     * @return int
     */
    public function getUsedEventNote()
    {
        return $this->usedEventNote;
    }

    /**
     * Set the used event note quota
     *
     * @param int $usedEventNote
     * @return void
     */
    public function setUsedEventNote($usedEventNote)
    {
        $this->usedEventNote = $usedEventNote;
    }

    /**
     * Returns the used inclusive quota of marketplace
     *
     * @return int
     */
    public function getUsedMarketplace()
    {
        return $this->usedMarketplace;
    }

    /**
     * Set the used inclusive quota of marketplace
     *
     * @param int $usedMarketplace
     * @return void
     */
    public function setUsedMarketplace($usedMarketplace)
    {
        $this->usedMarketplace = $usedMarketplace;
    }
}
