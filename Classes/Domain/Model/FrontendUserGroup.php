<?php
namespace DCNGmbH\MooxCommunity\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 *
 *
 * @package moox_community
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FrontendUserGroup extends \TYPO3\CMS\Extbase\Domain\Model\FrontendUserGroup
{
    /**
     * uid
     *
     * @var int
     */
    protected $uid;
    
    /**
     * pid
     *
     * @var int
     */
    protected $pid;
    
    /**
     * tstamp
     *
     * @var int
     */
    protected $tstamp;
    
    /**
     * crdate
     *
     * @var int
     */
    protected $crdate;
    
    /**
     * variant
     *
     * @var string
     */
    protected $variant;
    
    /**
     * title
     *
     * @var string
     */
    protected $title;
    
    /**
     * info
     *
     * @var string
     */
    protected $info;
    
    /**
     * zip
     *
     * @var string
     */
    protected $zip;
    
    /**
     * city
     *
     * @var string
     */
    protected $city;
    
    /**
     * address
     *
     * @var string
     */
    protected $address;
    
    /**
     * telephone
     *
     * @var string
     */
    protected $telephone;
    
    /**
     * fax
     *
     * @var string
     */
    protected $fax;
    
    /**
     * email
     *
     * @var string
     */
    protected $email;
    
    /**
     * www
     *
     * @var string
     */
    protected $www;
    
    /**
     * latitude
     *
     * @var string
     */
    protected $latitude;
    
    /**
     * longitude
     *
     * @var string
     */
    protected $longitude;
    
    /**
     * images
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxCommunity\Domain\Model\GroupImageReference>
     * @lazy
     */
    protected $images;
    
    /**
     * community admins
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxCommunity\Domain\Model\FrontendUser>
     * @lazy
     */
    protected $communityAdmins;
    
    /**
     * community moderators
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxCommunity\Domain\Model\FrontendUser>
     * @lazy
     */
    protected $communityModerators;

    /**
     * @var int
     */
    protected $quotaEventNote;

    /**
     * @var int
     */
    protected $quotaMarketplace;

    /**
     * initialize object
     */
    public function initializeObject()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }
    
    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->images = new ObjectStorage();
        $this->communityAdmins = new ObjectStorage();
        $this->communityModerators = new ObjectStorage();
    }
    
    /**
     * get uid
     *
     * @return int $uid uid
     */
    public function getUid()
    {
        return $this->uid;
    }
     
    /**
     * set uid
     *
     * @param int $uid uid
     * @return void
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }
    
    /**
     * get pid
     *
     * @return int $pid pid
     */
    public function getPid()
    {
        return $this->pid;
    }
     
    /**
     * set pid
     *
     * @param int $pid pid
     * @return void
     */
    public function setPid($pid)
    {
        $this->pid = $pid;
    }
    
    /**
     * get tstamp
     *
     * @return int $tstamp tstamp
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }
     
    /**
     * set tstamp
     *
     * @param int $tstamp tstamp
     * @return void
     */
    public function setTstamp($tstamp)
    {
        $this->tstamp = $tstamp;
    }
    
    /**
     * get crdate
     *
     * @return int $crdate crdate
     */
    public function getCrdate()
    {
        return $this->crdate;
    }
     
    /**
     * set crdate
     *
     * @param int $crdate crdate
     * @return void
     */
    public function setCrdate($crdate)
    {
        $this->crdate = $crdate;
    }
    
    /**
     * Get year of crdate
     *
     * @return int
     */
    public function getYearOfCrdate()
    {
        return $this->getCrdate()->format('Y');
    }

    /**
     * Get month of crdate
     *
     * @return int
     */
    public function getMonthOfCrdate()
    {
        return $this->getCrdate()->format('m');
    }

    /**
     * Get day of crdate
     *
     * @return int
     */
    public function getDayOfCrdate()
    {
        return (int)$this->crdate->format('d');
    }
    
    /**
     * get variant
     *
     * @return string $variant
     */
    public function getVariant()
    {
        return $this->variant;
    }
     
    /**
     * set variant
     *
     * @param string $variant
     * @return void
     */
    public function setVariant($variant)
    {
        $this->variant = $variant;
    }
    
    /**
     * get title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }
     
    /**
     * set title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
    
    /**
     * get info
     *
     * @return string $info
     */
    public function getInfo()
    {
        return $this->info;
    }
     
    /**
     * set info
     *
     * @param string $info
     * @return void
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }
    
    /**
     * get zip
     *
     * @return string $zip
     */
    public function getZip()
    {
        return $this->zip;
    }
     
    /**
     * set zip
     *
     * @param string $zip
     * @return void
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }
    
    /**
     * get city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }
     
    /**
     * set city
     *
     * @param string $city
     * @return void
     */
    public function setCity($city)
    {
        $this->city = $city;
    }
    
    /**
     * get address
     *
     * @return string $address
     */
    public function getAddress()
    {
        return $this->address;
    }
     
    /**
     * set address
     *
     * @param string $address
     * @return void
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }
    
    /**
     * get telephone
     *
     * @return string $telephone
     */
    public function getTelephone()
    {
        return $this->telephone;
    }
     
    /**
     * set telephone
     *
     * @param string $telephone
     * @return void
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }
    
    /**
     * get fax
     *
     * @return string $fax
     */
    public function getFax()
    {
        return $this->fax;
    }
     
    /**
     * set fax
     *
     * @param string $fax
     * @return void
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }
    
    /**
     * get email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }
     
    /**
     * set email
     *
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
    
    /**
     * get www
     *
     * @return string $www
     */
    public function getWww()
    {
        return $this->www;
    }
     
    /**
     * set www
     *
     * @param string $www
     * @return void
     */
    public function setWww($www)
    {
        $this->www = $www;
    }
    
    /**
     * get latitude
     *
     * @return string $latitude
     */
    public function getLatitude()
    {
        return $this->latitude;
    }
     
    /**
     * set latitude
     *
     * @param string $latitude
     * @return void
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }
    
    /**
     * get longitude
     *
     * @return string $longitude
     */
    public function getLongitude()
    {
        return $this->longitude;
    }
     
    /**
     * set longitude
     *
     * @param string $longitude
     * @return void
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }
        
    /**
     * sets the images
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $images
     *
     * @return void
     */
    public function setImages($images)
    {
        $this->images = $images;
    }
     
    /**
     * get the images
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * adds a image
     *
     * @param \DCNGmbH\MooxCommunity\Domain\Model\GroupImageReference $image
     *
     * @return void
     */
    public function addImages(\DCNGmbH\MooxCommunity\Domain\Model\GroupImageReference $image)
    {
        $this->images->attach($image);
    }
    
    /**
     * remove a image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\GroupImageReference $image
     *
     * @return void
     */
    public function removeImages(\TYPO3\CMS\Extbase\Domain\Model\GroupImageReference $image)
    {
        $this->images->detach($image);
    }
    
    /**
     * sets the community admins
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $communityAdmins
     *
     * @return void
     */
    public function setCommunityAdmins($communityAdmins)
    {
        $this->communityAdmins = $communityAdmins;
    }
     
    /**
     * get the community admins
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getCommunityAdmins()
    {
        return $this->communityAdmins;
    }

    /**
     * adds a community admin
     *
     * @param \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $communityAdmin
     *
     * @return void
     */
    public function addCommunityAdmins(\DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $communityAdmin)
    {
        $this->communityAdmins->attach($communityAdmin);
    }
    
    /**
     * remove a community admin
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FrontendUser $communityAdmin
     *
     * @return void
     */
    public function removeCommunityAdmins(\TYPO3\CMS\Extbase\Domain\Model\FrontendUser $communityAdmin)
    {
        $this->communityAdmins->detach($communityAdmin);
    }
    
    /**
     * sets the community moderators
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $communityModerators
     *
     * @return void
     */
    public function setCommunityModerators($communityModerators)
    {
        $this->communityModerators = $communityModerators;
    }
     
    /**
     * get the community moderators
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getCommunityModerators()
    {
        return $this->communityModerators;
    }

    /**
     * adds a community admin
     *
     * @param \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $communityAdmin
     *
     * @return void
     */
    public function addCommunityModerators(\DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $communityAdmin)
    {
        $this->communityModerators->attach($communityAdmin);
    }
    
    /**
     * remove a community admin
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FrontendUser $communityAdmin
     *
     * @return void
     */
    public function removeCommunityModerators(\TYPO3\CMS\Extbase\Domain\Model\FrontendUser $communityAdmin)
    {
        $this->communityModerators->detach($communityAdmin);
    }

    /**
     * Returns the available quota for event notes
     *
     * @return int
     */
    public function getQuotaEventNote()
    {
        return $this->quotaEventNote;
    }

    /**
     * Returns the available quota of marketplace
     *
     * @return int
     */
    public function getQuotaMarketplace()
    {
        return $this->quotaMarketplace;
    }
}
