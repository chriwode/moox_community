<?php
namespace DCNGmbH\MooxCommunity\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 *
 *
 * @package moox_community
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Membership extends AbstractEntity
{
    
    /**
     * Titel
     *
     * @var \string
     * @validate NotEmpty
     */
    protected $title;
    
    /**
     * confirmed
     *
     * @var \integer
     */
    protected $confirmed;
    
    /**
     * rejected
     *
     * @var \integer
     */
    protected $rejected;
    
    /**
     * fe user
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser
     */
    protected $feUser = null;
    
    /**
     * fe group
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Model\FrontendUserGroup
     */
    protected $feGroup = null;
    
    /**
     * Returns the title
     *
     * @return \string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param \string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
    
    /**
     * Returns the confirmed
     *
     * @return \integer $confirmed
     */
    public function getConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * Sets the confirmed
     *
     * @param \integer $confirmed
     * @return void
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;
    }
    
    /**
     * Returns the rejected
     *
     * @return \integer $rejected
     */
    public function getRejected()
    {
        return $this->rejected;
    }

    /**
     * Sets the rejected
     *
     * @param \integer $rejected
     * @return void
     */
    public function setRejected($rejected)
    {
        $this->rejected = $rejected;
    }
    
    /**
     * Returns the fe user
     *
     * @return \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $feUser
     */
    public function getFeUser()
    {
        return $this->feUser;
    }

    /**
     * Sets the fe user
     *
     * @param \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $feUser
     * @return void
     */
    public function setFeUser(\DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $feUser)
    {
        $this->feUser = $feUser;
    }
        
    /**
     * Returns the fe group
     *
     * @return \DCNGmbH\MooxCommunity\Domain\Model\FrontendUserGroup $feGroup
     */
    public function getFeGroup()
    {
        return $this->feGroup;
    }

    /**
     * Sets the fe group
     *
     * @param \DCNGmbH\MooxCommunity\Domain\Model\FrontendUserGroup $feGroup
     * @return void
     */
    public function setFeGroup(\DCNGmbH\MooxCommunity\Domain\Model\FrontendUserGroup $feGroup)
    {
        $this->feGroup = $feGroup;
    }
}
