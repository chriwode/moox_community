<?php
namespace DCNGmbH\MooxCommunity\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Extbase\Persistence\QueryInterface;

/**
 *
 *
 * @package moox_community
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FrontendUserGroupRepository extends \TYPO3\CMS\Extbase\Domain\Repository\FrontendUserGroupRepository
{
    protected $defaultOrderings = array('title' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING);
    
    /**
     * sets query orderings from given array/string
     *
     * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface &$query
     * @param \array|\string|null $orderings
     * @param \string $lookupTcaTable
     * @return \void
     */
    protected function setQueryOrderings(\TYPO3\CMS\Extbase\Persistence\QueryInterface &$query, $orderings = null, $lookupTcaTable = '')
    {
        $setOrderings = array();
        
        if (!is_null($orderings) && is_string($orderings)) {
            $orderings = array($orderings => QueryInterface::ORDER_ASCENDING);
        }
        
        if (is_array($orderings)) {
            foreach ($orderings as $field => $direction) {
                if (strtolower($direction)=='desc') {
                    $setOrderings[$field] = QueryInterface::ORDER_DESCENDING;
                } else {
                    $setOrderings[$field] = QueryInterface::ORDER_ASCENDING;
                }

                if ($lookupTcaTable!='' && isset($GLOBALS['TCA'][$lookupTcaTable]['columns'][$field]['moox']['sortable']['additional_sorting']) && $GLOBALS['TCA'][$lookupTcaTable]['columns'][$field]['moox']['sortable']['additional_sorting']!='') {
                    foreach (explode(',', $GLOBALS['TCA'][$lookupTcaTable]['columns'][$field]['moox']['sortable']['additional_sorting']) as $additionalSorting) {
                        $additionalSorting = explode(' ', $additionalSorting);
                        $field = $additionalSorting[0];
                        $direction = $additionalSorting[1];
                        
                        if (strtolower($direction)=='desc') {
                            $setOrderings[$field] = QueryInterface::ORDER_DESCENDING;
                        } else {
                            $setOrderings[$field] = QueryInterface::ORDER_ASCENDING;
                        }
                    }
                }
            }
            
            if (count($setOrderings)) {
                $query->setOrderings($setOrderings);
            }
        }
    }
    
    /**
     * sets query limits from given values
     *
     * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface &$query
     * @param \integer $offset
     * @param \integer $limit
     * @return \void
     */
    protected function setQueryLimits(\TYPO3\CMS\Extbase\Persistence\QueryInterface &$query, $offset = null, $limit = null)
    {
        if (is_numeric($offset)) {
            $query->setOffset($offset);
        }
        
        if (is_numeric($limit)) {
            $query->setLimit($limit);
        }
    }
    
    /**
     * sets query storage page(s)
     *
     * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface &$query
     * @param \array|\integer|\string $storagePages
     * @return \void
     */
    protected function setQueryStoragePages(\TYPO3\CMS\Extbase\Persistence\QueryInterface &$query, $storagePages = null)
    {
        if (is_string($storagePages)) {
            if ($storagePages=='all') {
                $query->getQuerySettings()->setRespectStoragePage(false);
            } elseif (strpos($storagePages, ',')!==false) {
                $query->getQuerySettings()->setStoragePageIds(explode(',', $storagePages));
            }
        } elseif (is_array($storagePages)) {
            $setStoragePages = array();
            
            foreach ($storagePages as $storagePage) {
                if (is_numeric($storagePage)) {
                    $setStoragePages[] = $storagePage;
                }
            }
            
            if (count($setStoragePages)) {
                $query->getQuerySettings()->setStoragePageIds($setStoragePages);
            }
        } elseif (is_numeric($storagePages)) {
            $query->getQuerySettings()->setStoragePageIds(array($storagePages));
        }
    }
    
    /**
     * Finds all by filter (ordered)
     *
     * @param \array $filter
     * @param \array $orderings
     * @param \integer $offset
     * @param \integer $limit
     * @param \array|\integer $storagePages
     * @param \array|\boolean $enableFieldsToBeIgnored
     * @param \boolean $rawMode if set to true, return is as an array
     * @param \string $lookupTcaTable
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByFilter($filter = null, $orderings = null, $offset = null, $limit = null, $storagePages = null, $enableFieldsToBeIgnored = null, $rawMode = false, $lookupTcaTable = '')
    {
        $query = $this->createQuery();
        $this->setQueryStoragePages($query, $storagePages);
        $this->setQueryOrderings($query, $orderings, $lookupTcaTable);
        $this->setQueryLimits($query, $offset, $limit);
        
        if (is_array($enableFieldsToBeIgnored)) {
            $query->getQuerySettings()->setIgnoreEnableFields(true);
            $query->getQuerySettings()->setEnableFieldsToBeIgnored($enableFieldsToBeIgnored);
        } elseif (!is_null($enableFieldsToBeIgnored) && $enableFieldsToBeIgnored) {
            $query->getQuerySettings()->setIgnoreEnableFields(true);
            $query->getQuerySettings()->setEnableFieldsToBeIgnored(array('disabled','starttime','endtime'));
        }
        
        if ($rawMode) {
            $query->getQuerySettings()->setReturnRawQueryResult(true);
        }
        
        $constraints = $this->createFilterConstraints($query, $filter);
        
        if (is_array($constraints)) {
            return $query->matching(
                $query->logicalAnd($constraints)
            )->execute();
        } else {
            return $query->execute();
        }
    }
    
    /**
     * Returns a constraint array created by a given filter array
     *
     * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
     * @param \array $filter
     * @param \array $constraints
     * @return \TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface|null
     */
    protected function createFilterConstraints(\TYPO3\CMS\Extbase\Persistence\QueryInterface $query, $filter = null, $constraints = null)
    {
        if (is_null($constraints)) {
            $constraints = array();
        }
        
        if (isset($filter['variant']) && is_string($filter['variant']) && $filter['variant']!='') {
            $constraints[] = $query->equals('variant', $filter['variant']);
        }
        
        if (isset($filter['uids']) && is_array($filter['uids'])) {
            $constraints[] = $query->in('uid', $filter['uids']);
        }
        
        if (isset($filter['search']) && is_array($filter['search']) && $filter['search']['query']!='') {
            if (!is_array($filter['search']['fields']) || !count($filter['search']['fields'])) {
                $filter['search']['fields'] = array('title');
            }
            
            $searchConstraints = array();
            foreach ($filter['search']['fields'] as $searchField) {
                $searchConstraints[] =  $query->like($searchField, '%'.$filter['search']['query'].'%');
            }
            
            if (count($searchConstraints)>0) {
                if (count($searchConstraints)==1) {
                    $constraints[] = $searchConstraints[0];
                } else {
                    $constraints[] = $query->logicalOr($searchConstraints);
                }
            }
        }
        
        if (isset($filter['communityAdmin']) && is_object($filter['communityAdmin'])) {
            $constraints[] = $query->contains('communityAdmins', $filter['communityAdmin']);
        }
        
        if (count($constraints)<1) {
            $constraints = null;
        }
        
        return $constraints;
    }
    
    /**
     * Find user groups by pid(list)
     *
     * @param array $pids pids
     * @return Tx_Extbase_Persistence_QueryInterface
     */
    public function findByPids($pids = array())
    {
        $pids    = (is_array($pids))?$pids:array($pids);
        
        $query = $this->createQuery();
        
        if (count($pids)) {
            $query->getQuerySettings()->setStoragePageIds($pids);
        } else {
            $query->getQuerySettings()->setRespectStoragePage(false);
        }
        
        return $query->execute();
    }
    
    /**
     * Find user groups by uid(list)
     *
     * @param array $uids uids
     * @return Tx_Extbase_Persistence_QueryInterface
     */
    public function findByIdList($uids = array())
    {
        $uids    = (is_array($uids))?$uids:array($uids);
        
        $query = $this->createQuery();
                
        $query->getQuerySettings()->setRespectStoragePage(false);
        
        $query->matching(
            $query->in('uid', $uids)
        );
        
        return $query->execute();
    }
    
    /**
     * Override default findByUid function to enable also the option to turn of
     * the enableField setting
     *
     * @param integer $uid id of record
     * @param boolean $respectEnableFields if set to false, hidden records are shown
     * @return \DCNGmbH\MooxCommunity\Domain\Model\FrontendUserGroup
     */
    public function findByUid($uid, $respectEnableFields = true)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->getQuerySettings()->setRespectSysLanguage(false);
        $query->getQuerySettings()->setIgnoreEnableFields(!$respectEnableFields);

        return $query->matching(
            $query->logicalAnd(
                $query->equals('uid', $uid),
                $query->equals('deleted', 0)
            ))->execute()->getFirst();
    }
}
