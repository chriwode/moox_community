<?php
namespace DCNGmbH\MooxCommunity\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \DCNGmbH\MooxCommunity\Domain\Repository\MooxRepository;
use \TYPO3\CMS\Extbase\Persistence\QueryInterface;

/**
 *
 *
 * @package moox_marketplace
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FriendshipRepository extends MooxRepository
{
    protected $defaultOrderings = array('tstamp' => QueryInterface::ORDER_DESCENDING);
    
    /**
     * Returns a constraint array created by a given filter array
     *
     * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
     * @param \array $filter
     * @param \array $constraints
     * @return \TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface|null
     */
    protected function createFilterConstraints(QueryInterface $query, $filter = null, $constraints = null)
    {
        if (is_null($constraints)) {
            $constraints = array();
        }
        
        if (isset($filter['feUser1']) && is_object($filter['feUser1'])) {
            $constraints[] = $query->equals('feUser1', $filter['feUser1']);
        }
        
        if (isset($filter['friend']) && is_object($filter['friend'])) {
            $constraints[] = $query->logicalOr(
                $query->equals('feUser1', $filter['friend']),
                $query->equals('feUser2', $filter['friend'])
            );
        }
        
        if (isset($filter['confirmed']) && $filter['confirmed']==0) {
            $constraints[] = $query->equals('confirmed', 0);
        } elseif (isset($filter['confirmed']) && $filter['confirmed']>0) {
            $constraints[] = $query->greaterThan('confirmed', 0);
        }

        
        if (count($constraints)<1) {
            $constraints = null;
        }
        
        return $constraints;
    }
    
    /**
     * findByFeUserAndFriend
     *
     * @param \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $feUser
     * @param \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $friend
     * @return \DCNGmbH\MooxCommunity\Domain\Model\Friendship
     */
    public function findByFeUserAndFriend(\DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $feUser, \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $friend)
    {
        $query = $this->createQuery();
        
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->getQuerySettings()->setRespectSysLanguage(false);
        
        return $query->matching(
            $query->logicalOr(
                $query->logicalAnd(
                    $query->equals('feUser1', $feUser),
                    $query->equals('feUser2', $friend)
                ),
                $query->logicalAnd(
                    $query->equals('feUser1', $friend),
                    $query->equals('feUser2', $feUser)
                )
            ))
        ->execute()->getFirst();
    }
}
