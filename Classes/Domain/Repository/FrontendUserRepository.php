<?php
namespace DCNGmbH\MooxCommunity\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Extbase\Persistence\QueryInterface;

/**
 *
 *
 * @package moox_community
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FrontendUserRepository extends \TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository
{
    
    /**
     * sets query orderings from given array/string
     *
     * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface &$query
     * @param \array|\string|null
     * @return \void
     */
    protected function setQueryOrderings(\TYPO3\CMS\Extbase\Persistence\QueryInterface &$query, $orderings = null)
    {
        $setOrderings = array();
        
        if (!is_null($orderings) && is_string($orderings)) {
            $orderings = array($orderings => QueryInterface::ORDER_ASCENDING);
        }
        
        if (is_array($orderings)) {
            foreach ($orderings as $field => $direction) {
                if (strtolower($direction)=='desc') {
                    $setOrderings[$field] = QueryInterface::ORDER_DESCENDING;
                } else {
                    $setOrderings[$field] = QueryInterface::ORDER_ASCENDING;
                }
            }
            
            if (count($setOrderings)) {
                $query->setOrderings($setOrderings);
            }
        }
    }
    
    /**
     * sets query limits from given values
     *
     * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface &$query
     * @param \integer $offset
     * @param \integer $limit
     * @return \void
     */
    protected function setQueryLimits(\TYPO3\CMS\Extbase\Persistence\QueryInterface &$query, $offset = null, $limit = null)
    {
        if (is_numeric($offset)) {
            $query->setOffset($offset);
        }
        
        if (is_numeric($limit)) {
            $query->setLimit($limit);
        }
    }
    
    /**
     * sets query storage page(s)
     *
     * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface &$query
     * @param \array|\integer|\string $storagePages
     * @return \void
     */
    protected function setQueryStoragePages(\TYPO3\CMS\Extbase\Persistence\QueryInterface &$query, $storagePages = null)
    {
        if (is_string($storagePages)) {
            if ($storagePages=='all') {
                $query->getQuerySettings()->setRespectStoragePage(false);
            } elseif (strpos($storagePages, ',')!==false) {
                $query->getQuerySettings()->setStoragePageIds(explode(',', $storagePages));
            }
        } elseif (is_array($storagePages)) {
            $setStoragePages = array();
            
            foreach ($storagePages as $storagePage) {
                if (is_numeric($storagePage)) {
                    $setStoragePages[] = $storagePage;
                }
            }
            
            if (count($setStoragePages)) {
                $query->getQuerySettings()->setStoragePageIds($setStoragePages);
            }
        } elseif (is_numeric($storagePages)) {
            $query->getQuerySettings()->setStoragePageIds(array($storagePages));
        }
    }
    
    /**
     * Finds all by filter (ordered)
     *
     * @param \array $filter
     * @param \array $orderings
     * @param \integer $offset
     * @param \integer $limit
     * @param \array|\integer $storagePages
     * @param \array|\boolean $enableFieldsToBeIgnored
     * @param \boolean $rawMode if set to true, return is as an array
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByFilter($filter = null, $orderings = null, $offset = null, $limit = null, $storagePages = null, $enableFieldsToBeIgnored = null, $rawMode = false)
    {
        $query = $this->createQuery();
        $this->setQueryStoragePages($query, $storagePages);
        $this->setQueryOrderings($query, $orderings);
        $this->setQueryLimits($query, $offset, $limit);
        
        if (is_array($enableFieldsToBeIgnored)) {
            $query->getQuerySettings()->setIgnoreEnableFields(true);
            $query->getQuerySettings()->setEnableFieldsToBeIgnored($enableFieldsToBeIgnored);
        } elseif (!is_null($enableFieldsToBeIgnored) && $enableFieldsToBeIgnored) {
            $query->getQuerySettings()->setIgnoreEnableFields(true);
            $query->getQuerySettings()->setEnableFieldsToBeIgnored(array('disabled','starttime','endtime'));
        }
        
        if ($rawMode) {
            $query->getQuerySettings()->setReturnRawQueryResult(true);
        }
        
        $constraints = $this->createFilterConstraints($query, $filter);
        
        if (is_array($constraints)) {
            return $query->matching(
                $query->logicalAnd($constraints)
            )->execute();
        } else {
            return $query->execute();
        }
    }
    
    /**
     * Returns a constraint array created by a given filter array
     *
     * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
     * @param \array $filter
     * @param \array $constraints
     * @return \TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface|null
     */
    protected function createFilterConstraints(\TYPO3\CMS\Extbase\Persistence\QueryInterface $query, $filter = null, $constraints = null)
    {
        if (is_null($constraints)) {
            $constraints = array();
        }
        
        /*
        if(isset($filter['variant']) && is_string($filter['variant']) && $filter['variant']!=""){

            $constraints[] = $query->equals('variant', $filter['variant']);

        }
        */
        
        if (isset($filter['uids']) && is_array($filter['uids'])) {
            $constraints[] = $query->in('uid', $filter['uids']);
        }
        
        if (isset($filter['search']) && is_array($filter['search']) && $filter['search']['query']!='') {
            if (!is_array($filter['search']['fields']) || !count($filter['search']['fields'])) {
                $filter['search']['fields'] = array('username','first_name','last_name');
            }
            
            $searchConstraints = array();
            foreach ($filter['search']['fields'] as $searchField) {
                $searchConstraints[] =  $query->like($searchField, '%'.$filter['search']['query'].'%');
            }
            
            if (count($searchConstraints)>0) {
                if (count($searchConstraints)==1) {
                    $constraints[] = $searchConstraints[0];
                } else {
                    $constraints[] = $query->logicalOr($searchConstraints);
                }
            }
        }
        
        if (count($constraints)<1) {
            $constraints = null;
        }
        
        return $constraints;
    }
    
    /**
     * Finds all frontend users (overwrite)
     *
     * @param array $storagePids
     * @param array $filter
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface The frontend users
     */
    public function findAll($storagePids = array(), $filter = array())
    {
        $query = $this->createQuery();
        
        if (is_numeric($storagePids) && $storagePids>0) {
            $storagePids = array($storagePids);
        }
        
        if (!is_array($storagePids) || count($storagePids)<1) {
            $storagePids = array(0);
        }
        
        //$query->getQuerySettings()->setRespectStoragePage(true);
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setStoragePageIds($storagePids);
        
        $constraints = array();
                
        $constraints[] = $query->equals('deleted', 0);
        
        if (is_numeric($filter['group']) && $filter['group']>0) {
            $constraints[] = $query->contains('usergroup', $filter['group']);
        } elseif ($filter['group']=='nogroup') {
            $constraints[] = $query->equals('usergroup', null);
        }
        if ($filter['mailing']==1) {
            $constraints[] = $query->equals('disallow_mailing', 0);
        } elseif ($filter['mailing']==2) {
            $constraints[] = $query->equals('disallow_mailing', 1);
        }
        if ($filter['admin']==1) {
            $constraints[] = $query->equals('is_company_admin', 1);
        } elseif ($filter['admin']==2) {
            $constraints[] = $query->equals('is_company_admin', 0);
        }
        if ($filter['state']==1) {
            $constraints[] = $query->equals('disable', 0);
        } elseif ($filter['state']==2) {
            $constraints[] = $query->equals('disable', 1);
        }
        if (in_array((string)$filter['quality'], array('0','1','2'))) {
            $constraints[] = $query->equals('quality', $filter['quality']);
        }
        $filter['query'] = trim($filter['query']);
        if ($filter['query']!='') {
            $constraints[] = $query->logicalOr(
                $query->like('username', '%'.$filter['query'].'%'),
                $query->like('name', '%'.$filter['query'].'%'),
                $query->like('first_name', '%'.$filter['query'].'%'),
                $query->like('middle_name', '%'.$filter['query'].'%'),
                $query->like('last_name', '%'.$filter['query'].'%'),
                $query->like('address', '%'.$filter['query'].'%'),
                $query->like('telephone', '%'.$filter['query'].'%'),
                $query->like('fax', '%'.$filter['query'].'%'),
                $query->like('email', '%'.$filter['query'].'%'),
                $query->like('title', '%'.$filter['query'].'%'),
                $query->like('zip', '%'.$filter['query'].'%'),
                $query->like('city', '%'.$filter['query'].'%'),
                $query->like('country', '%'.$filter['query'].'%'),
                $query->like('www', '%'.$filter['query'].'%'),
                $query->like('company', '%'.$filter['query'].'%')
            );
        }
        
        if ($filter['sortDirection']=='DESC') {
            $filter['sortDirection'] = \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING;
        } else {
            $filter['sortDirection'] = \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING;
        }
        
        if ($filter['sortField']=='address') {
            $query->setOrderings(array('zip' => $filter['sortDirection'], 'city' => $filter['sortDirection'], 'address' => $filter['sortDirection']));
        } else {
            $sortFields = explode('|', $filter['sortField']);
            if (count($sortFields)>1) {
                $sortOrderings = array();
                foreach ($sortFields as $sortField) {
                    if ($sortField=='address') {
                        $sortOrderings = array_merge($sortOrderings, array('zip' => $filter['sortDirection'], 'city' => $filter['sortDirection'], 'address' => $filter['sortDirection']));
                    } else {
                        $sortOrderings = array_merge($sortOrderings, array($sortField => $filter['sortDirection']));
                    }
                }
                $query->setOrderings($sortOrderings);
            } else {
                $query->setOrderings(array($filter['sortField'] => $filter['sortDirection']));
            }
        }
                
        return $query->matching($query->logicalAnd($constraints))->execute();
    }
    
    /**
     * Finds all company users
     *
     * @param array $storagePids
     * @param integer $uid uid to exclude from list
     * @param string $company The company to get users from
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface The users
     */
    public function findByCompany($storagePids = array(), $uid = 0, $company = '')
    {
        if ($uid>0 && $company!='') {
            $query = $this->createQuery();
            
            if (is_numeric($storagePids) && $storagePids>0) {
                $storagePids = array($storagePids);
            }
            
            if (!is_array($storagePids) || count($storagePids)<1) {
                $storagePids = array(0);
            }
                
            $query->getQuerySettings()->setStoragePageIds($storagePids);
            
            $query->matching(
                $query->logicalAnd(
                    $query->equals('company', $company),
                    $query->logicalNot(
                        $query->equals('uid', $uid)
                    )
                )
            );
            
            return $query->execute();
        } else {
            return null;
        }
    }
    
    /**
     * Finds user by username
     *
     * @param array $storagePids
     * @param string $username
     * @param boolean $respectEnableFields if set to false, hidden records are shown
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface The users
     */
    public function findByUsername($storagePids = array(), $username = '', $respectEnableFields = true)
    {
        if ($username!='') {
            $query = $this->createQuery();
            
            $query->getQuerySettings()->setIgnoreEnableFields(!$respectEnableFields);
            
            if (is_string($storagePids) && $storagePids=='all') {
                $query->getQuerySettings()->setRespectStoragePage(false);
            } else {
                if (is_numeric($storagePids) && $storagePids>0) {
                    $storagePids = array($storagePids);
                }
                
                if (!is_array($storagePids) || count($storagePids)<1) {
                    $storagePids = array(0);
                }
                    
                $query->getQuerySettings()->setStoragePageIds($storagePids);
            }
            
            if ($respectEnableFields) {
                return $query->matching(
                    $query->equals('username', $username)
                )->execute();
            } else {
                return $query->matching(
                    $query->logicalAnd(
                        $query->equals('username', $username),
                        $query->equals('deleted', 0)
                    )
                )->execute();
            }
        } else {
            return null;
        }
    }
    
    /**
     * Finds user by email
     *
     * @param array $storagePids
     * @param string $email
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface The users
     */
    public function findByEmail($storagePids = array(), $email = '')
    {
        if ($email!='') {
            $query = $this->createQuery();
            
            if (is_numeric($storagePids) && $storagePids>0) {
                $storagePids = array($storagePids);
            }
            
            if (!is_array($storagePids) || count($storagePids)<1) {
                $storagePids = array(0);
            }
                
            $query->getQuerySettings()->setStoragePageIds($storagePids);
            
            $query->matching(
                $query->equals('email', $email)
            );
            
            return $query->execute();
        } else {
            return null;
        }
    }
    
    /**
     * Find by email and register hash
     *
     * @param array $storagePids storage pids
     * @param string $email id of record
     * @param string $hash hash
     * @return \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser
     */
    public function findByEmailAndRegisterHash($storagePids = array(), $email = '', $hash = '')
    {
        if ($email!='' && $hash!='') {
            $query = $this->createQuery();
            
            if (is_numeric($storagePids) && $storagePids>0) {
                $storagePids = array($storagePids);
            }
            
            if (is_array($storagePids) || count($storagePids)>0) {
                $query->getQuerySettings()->setStoragePageIds($storagePids);
            }
            
            $query->getQuerySettings()->setRespectSysLanguage(false);
            $query->getQuerySettings()->setIgnoreEnableFields(true);
            
            return $query->matching(
                $query->logicalAnd(
                    $query->logicalNot(
                        $query->equals('registerHash', '')
                    ),
                    $query->equals('email', $email),
                    $query->equals('registerHash', $hash),
                    $query->equals('deleted', 0)
                ))->execute();
        } else {
            return null;
        }
    }
    
    /**
     * Find by email and profile hash
     *
     * @param array $storagePids storage pids
     * @param string $email id of record
     * @param string $hash hash
     * @return \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser
     */
    public function findByEmailAndProfileHash($storagePids = array(), $email = '', $hash = '')
    {
        if ($email!='' && $hash!='') {
            $query = $this->createQuery();
            
            if (is_numeric($storagePids) && $storagePids>0) {
                $storagePids = array($storagePids);
            }
            
            if (is_array($storagePids) || count($storagePids)>0) {
                $query->getQuerySettings()->setStoragePageIds($storagePids);
            }
            
            $query->getQuerySettings()->setRespectSysLanguage(false);
            $query->getQuerySettings()->setIgnoreEnableFields(true);
            
            return $query->matching(
                $query->logicalAnd(
                    $query->logicalNot(
                        $query->equals('profileHash', '')
                    ),
                    $query->equals('email', $email),
                    $query->equals('profileHash', $hash),
                    $query->equals('deleted', 0)
                ))->execute();
        } else {
            return null;
        }
    }
    
    /**
     * Finds user by username and email
     *
     * @param array $storagePids
     * @param string $username
     * @param string $email
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface The users
     */
    public function findByUsernameAndEmail($storagePids = array(), $username = '', $email = '')
    {
        if ($username!='' && $email!='') {
            $query = $this->createQuery();
            
            if (is_numeric($storagePids) && $storagePids>0) {
                $storagePids = array($storagePids);
            }
            
            if (!is_array($storagePids) || count($storagePids)<1) {
                $storagePids = array(0);
            }
                
            $query->getQuerySettings()->setStoragePageIds($storagePids);
            
            $query->matching(
                $query->logicalAnd(
                    $query->equals('username', $username),
                    $query->equals('email', $email)
                )
            );
            
            return $query->execute();
        } else {
            return null;
        }
    }
    
    /**
     * Finds user by username and password
     *
     * @param array $storagePids
     * @param string $username
     * @return \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser
     */
    public function findOneByUsername($storagePids = array(), $username = '', $raw = false)
    {
        if ($username!='') {
            $query = $this->createQuery();
            
            if (is_numeric($storagePids) && $storagePids>0) {
                $storagePids = array($storagePids);
            }
            
            if (!is_array($storagePids) || count($storagePids)<1) {
                $storagePids = array(0);
            }
                
            $query->getQuerySettings()->setStoragePageIds($storagePids);
            if ($raw) {
                $query->getQuerySettings()->setReturnRawQueryResult(true);
                return    current($query->matching(
                            $query->equals('username', $username)
                        )->setLimit(1)->execute());
            } else {
                return    $query->matching(
                            $query->equals('username', $username)
                        )->execute()->getFirst();
            }
        } else {
            return null;
        }
    }
    
    /**
     * Finds user by uid and hash
     *
     * @param array $storagePids
     * @param integer $uid
     * @param string $hash
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface The users
     */
    public function findByUidAndHash($storagePids = array(), $uid = 0, $hash = '')
    {
        if ($uid>0 && $hash!='') {
            $query = $this->createQuery();
            
            if (is_numeric($storagePids) && $storagePids>0) {
                $storagePids = array($storagePids);
            }
            
            if (!is_array($storagePids) || count($storagePids)<1) {
                $storagePids = array(0);
            }
                
            $query->getQuerySettings()->setStoragePageIds($storagePids);
            
            $query->matching(
                $query->logicalAnd(
                    $query->equals('uid', $uid),
                    $query->equals('passwordRecoveryHash', $hash),
                    $query->greaterThan('passwordRecoveryTstamp', (time()-(86400*2)))
                )
            );
            
            return $query->execute();
        } else {
            return null;
        }
    }
    
    /**
     * Override default findByUid function to enable also the option to turn of
     * the enableField setting
     *
     * @param integer $uid id of record
     * @param boolean $respectEnableFields if set to false, hidden records are shown
     * @return \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser
     */
    public function findByUid($uid, $respectEnableFields = true)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->getQuerySettings()->setRespectSysLanguage(false);
        $query->getQuerySettings()->setIgnoreEnableFields(!$respectEnableFields);

        return $query->matching(
            $query->logicalAnd(
                $query->equals('uid', $uid),
                $query->equals('deleted', 0)
            ))->execute()->getFirst();
    }
    
    /**
     * findReallyAll
     *
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface The users
     */
    public function findReallyAll()
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->getQuerySettings()->setRespectSysLanguage(false);
        $query->getQuerySettings()->setIgnoreEnableFields(true);

        return $query->execute();
    }
    
    /**
     * Find duplicate of given object
     *
     * @param \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $object object
     * @return \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser
     */
    public function findDuplicate($object = null)
    {
        if ($object) {
            $query = $this->createQuery();
            $query->getQuerySettings()->setRespectSysLanguage(false);
            $query->getQuerySettings()->setIgnoreEnableFields(true);

            return $query->matching(
                $query->logicalAnd(
                    $query->equals('gender', $object->getGender()),
                    $query->equals('title', $object->getTitle()),
                    $query->equals('firstName', $object->getFirstName()),
                    $query->equals('middleName', $object->getMiddleName()),
                    $query->equals('lastName', $object->getLastName()),
                    $query->equals('company', $object->getCompany()),
                    $query->equals('address', $object->getAddress()),
                    $query->equals('zip', $object->getZip()),
                    $query->equals('city', $object->getCity()),
                    $query->equals('country', $object->getCountry()),
                    $query->equals('telephone', $object->getTelephone()),
                    $query->equals('fax', $object->getFax()),
                    $query->equals('email', $object->getEmail()),
                    $query->equals('www', $object->getWww()),
                    $query->equals('deleted', 0)
                ))->execute()->getFirst();
        } else {
            return null;
        }
    }
    
    /**
     * Find existing user by uid
     *
     * @param \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $object object
     * @return \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser
     */
    public function findExistingByUid($object = null)
    {
        if ($object) {
            $query = $this->createQuery();
            $query->getQuerySettings()->setRespectSysLanguage(false);
            $query->getQuerySettings()->setIgnoreEnableFields(true);
            
            return $query->matching(
                $query->logicalAnd(
                    $query->equals('uid', $object->getUid()),
                    $query->equals('deleted', 0)
                ))->execute()->getFirst();
        } else {
            return null;
        }
    }
    
    /**
     * Find existing user by username
     *
     * @param \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $object object
     * @return \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser
     */
    public function findExistingByUsername($object = null)
    {
        if ($object) {
            $query = $this->createQuery();
            $query->getQuerySettings()->setRespectSysLanguage(false);
            $query->getQuerySettings()->setIgnoreEnableFields(true);

            return $query->matching(
                $query->logicalAnd(
                    $query->equals('username', $object->getUsername()),
                    $query->equals('deleted', 0)
                ))->execute()->getFirst();
        } else {
            return null;
        }
    }
    
    /**
     * find extended filter items
     *
     * @param \array $field
     * @param \array $storagePages
     * @param \string $table
     * @param \string $where
     * @return \array
     */
    public function findTcaFieldValues($field, $storagePagess = null, $table, $where = '')
    {
        $result = array();
        
        if ($table!='') {
            if (!is_array($storagePages)) {
                $storagePages = array($storagePages);
            }
            
            $query = $this->createQuery();
                    
            if (is_array($storagePages) && count($storagePage)) {
                $query->getQuerySettings()->setRespectStoragePage(false);
                $storagePageIdsStmt = ' AND pid IN ('.implode(',', $storagePages).')';
            }
            $query->getQuerySettings()->setRespectSysLanguage(true);
            $query->getQuerySettings()->setIgnoreEnableFields(false);
            $query->getQuerySettings()->setReturnRawQueryResult(true);
            $query->statement('SELECT uid,title FROM `'.$table.'` WHERE 1=1'.$storagePageIdsStmt.' '.$where);
            $resultDb = $query->execute();
                        
            foreach ($resultDb as $item) {
                $result[] = array('uid' => $item['uid'], 'title' => $item['title']);
            }
        }
        
        return $result;
    }
}
