<?php
namespace DCNGmbH\MooxCommunity\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package moox_community
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class TemplateController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    
    /**
     * templateRepository
     *
     * @var \DCNGmbH\MooxMailer\Domain\Repository\TemplateRepository
     */
    protected $templateRepository;
    
    /**
     * extConf
     *
     * @var boolean
     */
    protected $extConf;
    
    /**
     * initialize the controller
     *
     * @return void
     */
    protected function initializeAction()
    {
        parent::initializeAction();
        $this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_community']);
        $this->templateRepository = $this->objectManager->get('DCNGmbH\\MooxCommunity\\Domain\\Repository\\TemplateRepository');
    }
    
    /**
     * action index
     *
     * @return void
     */
    public function indexAction()
    {
        $this->view->assign('items', $this->templateRepository->findAll(false));
        $this->view->assign('action', 'index');
    }
    
    /**
     * action add
     *
     * @param \array $add
     * @return void
     */
    public function addAction($add = array())
    {
        if (isset($add['save']) || isset($add['saveAndClose']) ||  isset($add['saveAndNew'])) {
            $item = $this->objectManager->get('DCNGmbH\\MooxCommunity\\Domain\\Model\\Template');
            $item->setTitle($add['title']);
            $item->setSubject($add['subject']);
            $item->setCategory($add['category']);
            $item->setTemplate($add['template']);
            
            $this->templateRepository->add($item);
            $this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
            
            $this->flashMessageContainer->add(
                '',
                'Vorlage wurde erfolgreich gespeichert.',
                \TYPO3\CMS\Core\Messaging\FlashMessage::OK);
        }
        if (isset($add['save'])) {
            $this->redirect('edit', null, null, array('uid' => $item->getUid()));
        } elseif (isset($add['saveAndClose'])) {
            $this->redirect('index');
        } elseif (isset($add['saveAndNew'])) {
            $this->redirect('add');
        } else {
            $this->view->assign('item', $add);
            $this->view->assign('categories', $this->getTemplateCategories());
            $this->view->assign('action', 'add');
        }
    }
    
    /**
     * action edit
     *
     * @param \int $uid
     * @param \array $edit
     * @return void
     */
    public function editAction($uid = 0, $edit = array())
    {
        if ($uid>0) {
            $item = $this->templateRepository->findByUid($uid);
            
            if (!count($edit)) {
                $edit['title']        = $item->getTitle();
                $edit['subject']    = $item->getSubject();
                $edit['category']    = $item->getCategory();
                $edit['template']    = $item->getTemplate();
                $edit['uid']        = $item->getUid();
            }
                        
            if (isset($edit['save']) || isset($edit['saveAndClose']) ||  isset($edit['saveAndNew'])) {
                $item->setTitle($edit['title']);
                $item->setSubject($edit['subject']);
                $item->setCategory($edit['category']);
                $item->setTemplate($edit['template']);
                
                $this->templateRepository->update($item);
                $this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
                
                $this->flashMessageContainer->add(
                    '',
                    'Änderungen wurden erfolgreich gespeichert.',
                    \TYPO3\CMS\Core\Messaging\FlashMessage::OK);
            }
            if (isset($edit['saveAndClose'])) {
                $this->redirect('index');
            } elseif (isset($edit['saveAndNew'])) {
                $this->redirect('add');
            } else {
                $this->view->assign('item', $item);
                $this->view->assign('categories', $this->getTemplateCategories());
                $this->view->assign('action', 'edit');
                $this->view->assign('uid', $uid);
            }
        } else {
            $this->redirect('index');
        }
    }
    
    /**
     * action delete
     *
     * @param \int $uid
     * @return void
     */
    public function deleteAction($uid = 0)
    {
        if ($uid>0) {
            $item = $this->templateRepository->findByUid($uid);
            
            $this->templateRepository->remove($item);
            
            $this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
            
            $this->flashMessageContainer->add(
                    '',
                    'Vorlage wurde gelöscht.',
                    \TYPO3\CMS\Core\Messaging\FlashMessage::OK);
        }
        
        $this->redirect('index');
    }
    
    /**
     * action preview iframe
     *
     * @param \int $uid
     * @return void
     */
    public function previewIframeAction($uid = 0)
    {
        if ($uid>0) {
            $template = $this->templateRepository->findByUid($uid);
            
            $data['user']['gender']        = 1;
            $data['user']['title']            = 'Dr.';
            $data['user']['username']        = 'hans.mustermann';
            $data['user']['name']            = 'Hans Mustermann';
            $data['user']['first_name']    = 'Hans';
            $data['user']['middle_name']    = 'Jürgen';
            $data['user']['last_name']        = 'Mustermann';
            $data['user']['email']            = 'email@example.net';
            $data['url']                    = 'http://example.net/recovery.html';
            
            $data['user']['requested_usergroup'][] = array('title' => 'Gruppe 1');
            
            if (!empty($this->extConf['mailRenderingPartialRoot'])) {
                $partialRootPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($this->extConf['mailRenderingPartialRoot']);
                if (!is_dir($partialRootPath)) {
                    unset($partialRootPath);
                }
            }
            
            if ($partialRootPath=='') {
                $conf = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
                $partialRootPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(str_replace('Backend/', '', $conf['view']['partialRootPath']).'Mail');
            }
            
            $previewView = $this->objectManager->create('TYPO3\\CMS\\Fluid\\View\StandaloneView');
            $previewView->setFormat('html');
            $previewView->setTemplateSource($template->getTemplate());
            if ($partialRootPath!='') {
                $previewView->setPartialRootPath($partialRootPath);
            }
            $previewView->assignMultiple($data);
            $preview     = $previewView->render();
            
            $this->view->assign('preview', $preview);
        } else {
            $this->view->assign('preview', 'Vorschau kann nicht angezeigt werden.');
        }
    }
    
    /**
     * Returns template categories
     *
     * @return array
     */
    public function getTemplateCategories()
    {
        $categories = array();
        
        $categories['registerconfirm']            = 'Registrierungs-Bestätigungs-Mail';
        $categories['passwordrecovery']        = 'Passwort-Wiederherstellungs-Mail';
        $categories['registerwelcome']            = 'Willkommens-Mail';
        $categories['profileconfirm']            = 'Profil-Bestätigungs-Mail';
        $categories['adminnewuser']            = 'Administrator-Benachrichtigung [Neuer Benutzer]';
        $categories['communitynotifications']    = 'Community-Benachrichtigungen [Neuer Benutzer]';
        
        return $categories;
    }
    
    /**
     * Returns ext conf
     *
     * @return array
     */
    public function getExtConf()
    {
        return $this->extConf;
    }

    /**
     * Set ext conf
     *
     * @param array $extConf ext conf
     * @return void
     */
    public function setExtConf($extConf)
    {
        $this->extConf = $extConf;
    }
}
