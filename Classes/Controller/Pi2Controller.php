<?php
namespace DCNGmbH\MooxCommunity\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
 
use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use \TYPO3\CMS\Core\Messaging\FlashMessage;
use \TYPO3\CMS\Saltedpasswords\Utility\SaltedPasswordsUtility;
use \TYPO3\CMS\Saltedpasswords\Salt\SaltFactory;
use \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

/**
 *
 *
 * @package moox_community
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Pi2Controller extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * frontendUserRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository;
        
    /**
     * accessControllService
     *
     * @var \DCNGmbH\MooxCommunity\Service\AccessControlService
     * @inject
     */
    protected $accessControllService;
    
    /**
     * helperService
     *
     * @var \DCNGmbH\MooxCommunity\Service\HelperService
     * @inject
     */
    protected $helperService;
    
    /**
     * extConf
     *
     * @var boolean
     */
    protected $extConf;
    
    /**
     * storagePids
     *
     * @var array
     */
    protected $storagePids;
    
    /**
     * Path to the locallang file
     * @var string
     */
    const LLPATH = 'LLL:EXT:moox_community/Resources/Private/Language/locallang.xlf:';
                
    /**
     * initialize action
     *
     * @return void
     */
    public function initializeAction()
    {
        
        // execute parent initialize action
        parent::initializeAction();
        
        // load extension configuration
        $this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_community']);
                
        // initalize storage settings
        $this->initializeStorageSettings();
    }

    /**
     * initialize storage settings
     *
     * @return void
     */
    protected function initializeStorageSettings()
    {
            
        // get typoscript configuration
        $configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
        
        // set storage pid if set by plugin
        if ($this->settings['storagePids']!='') {
            $this->setStoragePids(explode(',', $this->settings['storagePids']));
        } else {
            $this->setStoragePids(array());
        }
        
        // overwrite extbase persistence storage pid
        if (!empty($this->settings['storagePid']) && $this->settings['storagePid']!='TS') {
            if (empty($configuration['persistence']['storagePid'])) {
                $storagePids['persistence']['storagePid'] = $this->settings['storagePid'];
                $this->configurationManager->setConfiguration(array_merge($configuration, $storagePids));
            } else {
                $configuration['persistence']['storagePid'] = $this->settings['storagePid'];
                $this->configurationManager->setConfiguration($configuration);
            }
        }
    }
    
    /**
     * action login
     *
     * @param \array $login
     * @param \string $redirect
     * @param \array $messages
     * @return void
     */
    public function loginAction($login = null, $redirect = '', $messages = null)
    {
                
        // init action arrays
        $errors = array();

        // handle url messages // bugfix
        if (is_array($messages)) {
            $urlMessages = $messages;
            $messages = array();
            foreach ($urlMessages as $message) {
                $messageParts = explode('.', $message);
                $messages[] = array(
                    'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                    'title' => LocalizationUtility::translate(self::LLPATH.$messageParts[0].'.'.$messageParts[1], $this->extensionName),
                    'text' => LocalizationUtility::translate(self::LLPATH.$message, $this->extensionName),
                    'type' => FlashMessage::OK,
                );
            }
        } else {
            $messages = array();
        }
        
        // set redirect uri
        if ($redirect=='') {
            
            // if redirection pid is set by plugin
            if ($this->settings['loginRedirectPid']>0) {
                $redirect = $this->uriBuilder->reset()->setNoCache(true)->setTargetPageUid((int)$this->settings['loginRedirectPid'])->setLinkAccessRestrictedPages(true)->build();
            } else {
                $redirect = $this->uriBuilder->getRequest()->getRequestUri(); //$GLOBALS['TSFE']->baseUrl.$GLOBALS['TSFE']->anchorPrefix;
            }
        }

        // if form submitted
        if ($login) {

            // check username field
            if (trim($login['username'])=='') {
                
                // add message
                $messages[] = array(
                    'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                    'title' => LocalizationUtility::translate(self::LLPATH.'pi2.action_login', $this->extensionName),
                    'text' => LocalizationUtility::translate(self::LLPATH.'form.username.error.empty', $this->extensionName),
                    'type' => FlashMessage::ERROR,
                );
                
                // set error
                $errors['username'] = true;
            }
            
            // check password field
            if (trim($login['password'])=='') {
                
                // add message
                $messages[] = array(
                    'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                    'title' => LocalizationUtility::translate(self::LLPATH.'pi2.action_login', $this->extensionName),
                    'text' => LocalizationUtility::translate(self::LLPATH.'form.password.error.empty', $this->extensionName),
                    'type' => FlashMessage::ERROR,
                );
                
                // set error
                $errors['password'] = true;
            }
            
            // check if login is valid
            if (trim($login['username'])!='' && trim($login['password'])!='') {
                
                // get login user
                $item = $this->frontendUserRepository->findOneByUsername($this->storagePids, trim($login['username']), true);
                
                // check if password is equal to salted password from database
                if ($item['uid']>0) {
                    if (SaltedPasswordsUtility::isUsageEnabled('FE')) {
                        $objSalt = SaltFactory::getSaltingInstance($item['password']);
                        if (is_object($objSalt)) {
                            if (!$objSalt->checkPassword(trim($login['password']), $item['password'])) {
                                $item = null;
                            }
                        }
                    }
                }

                // if login is not valid
                if (!$item) {
                    
                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => LocalizationUtility::translate(self::LLPATH.'pi2.action_login', $this->extensionName),
                        'text' => LocalizationUtility::translate(self::LLPATH.'pi2.action_login.error', $this->extensionName),
                        'type' => FlashMessage::ERROR,
                    );
                    
                    // set errors
                    $errors['username'] = true;
                    $errors['password'] = true;
                }
            }
            
            // no errors -> login user
            if (!count($errors)) {
                
                // login user
                $this->accessControllService->login($item);
                
                // add message
                $messages[] = array(
                    'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                    'title' => LocalizationUtility::translate(self::LLPATH.'pi2.action_login', $this->extensionName),
                    'text' => LocalizationUtility::translate(self::LLPATH.'pi2.action_login.welcome', $this->extensionName),
                    'type' => FlashMessage::OK,
                );
                
                // if redirect uri is set
                if ($redirect!='') {
                    
                    // redirect to uri
                    $this->redirectToUri($redirect);
                }
            }
        }
        
        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
        
        // set template variables
        $this->view->assign('item', $login);
        $this->view->assign('errors', $errors);
        $this->view->assign('redirect', $redirect);
        $this->view->assign('action', 'login');
    }
    
    /**
     * action error
     *
     * @return void
     */
    public function errorAction()
    {
        $this->view->assign('action', 'error');
    }
    
    /**
     * Returns storage pids
     *
     * @return array
     */
    public function getStoragePids()
    {
        return $this->storagePids;
    }

    /**
     * Set storage pids
     *
     * @param array $storagePids storage pids
     * @return void
     */
    public function setStoragePids($storagePids)
    {
        $this->storagePids = $storagePids;
    }
    
    /**
     * Returns ext conf
     *
     * @return array
     */
    public function getExtConf()
    {
        return $this->extConf;
    }

    /**
     * Set ext conf
     *
     * @param array $extConf ext conf
     * @return void
     */
    public function setExtConf($extConf)
    {
        $this->extConf = $extConf;
    }
}
