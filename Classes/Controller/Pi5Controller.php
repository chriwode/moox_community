<?php
namespace DCNGmbH\MooxCommunity\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use \TYPO3\CMS\Core\Messaging\FlashMessage;
use \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use \TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 *
 *
 * @package moox_community
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Pi5Controller extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * accessControllService
     *
     * @var \DCNGmbH\MooxCommunity\Service\AccessControlService
     * @inject
     */
    protected $accessControllService;
    
    /**
     * frontendUserRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository;
    
    /**
     * frontendUserGroupRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\FrontendUserGroupRepository
     * @inject
     */
    protected $frontendUserGroupRepository;
    
    /**
     * membershipRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\MembershipRepository
     * @inject
     */
    protected $membershipRepository;
    
    /**
     * friendshipRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\FriendshipRepository
     * @inject
     */
    protected $friendshipRepository;
    
    /**
     * templateRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\TemplateRepository
     * @inject
     */
    protected $templateRepository;
    
    /**
     * helperService
     *
     * @var \DCNGmbH\MooxCommunity\Service\HelperService
     * @inject
     */
    protected $helperService;
    
    /**
     * persistenceManager
     *
     * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
     * @inject
     */
    protected $persistenceManager;
    
    /**
     * extConf
     *
     * @var boolean
     */
    protected $extConf;
    
    /**
     * storagePids
     *
     * @var array
     */
    protected $storagePids;
        
    /**
     * fieldsUsers
     *
     * @var \array
     */
    protected $fieldsUsers;
    
    /**
     * fieldsGroups
     *
     * @var \array
     */
    protected $fieldsGroups;
    
    /**
     * fieldConfigUsers
     *
     * @var \array
     */
    protected $fieldConfigUsers;
    
    /**
     * fieldConfigGroups
     *
     * @var \array
     */
    protected $fieldConfigGroups;
    
    /**
     * pagination
     *
     * @var \array
     */
    protected $pagination;
    
    /**
     * orderings
     *
     * @var \array
     */
    protected $orderings;
    
    /**
     * allowedOrderBy
     *
     * @var \string
     */
    protected $allowedOrderBy;
    
    /**
     * Path to the locallang file
     * @var string
     */
    const LLPATH = 'LLL:EXT:moox_community/Resources/Private/Language/locallang.xlf:';
    
    /**
     * temp folder
     * @var string
     */
    const TEMPFOLDER = 'moox_community';
    
    /**
     * initialize action
     *
     * @return void
     */
    public function initializeAction()
    {
        
        // execute parent initialize action
        parent::initializeAction();
        
        // load extension configuration
        $this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_community']);
        
        // set items per page
        if ($this->settings['itemsPerPage']>0) {
            $this->pagination['itemsPerPage'] = $this->settings['itemsPerPage'];
        } else {
            $this->pagination['itemsPerPage'] = 10000;
        }
        
        // set orderings
        if (in_array($this->actionMethodName, array('listFriendships','listGroupMembershipsAction'))) {
            $settingsOrderBy = $this->settings['usersOrderBy'];
            $settingsAllowedOrderBy = $this->settings['usersAllowedOrderBy'];
            $orderDefault = 'username';
        } else {
            $settingsOrderBy = $this->settings['groupsOrderBy'];
            $settingsAllowedOrderBy = $this->settings['groupsAllowedOrderBy'];
            $orderDefault = 'title';
        }
        if ($settingsOrderBy!='') {
            $orderBy = $settingsOrderBy;
            $this->allowedOrderBy = $orderBy;
        } else {
            if ($settingsAllowedOrderBy!='') {
                $allowedOrderBy = explode(',', $settingsAllowedOrderBy);
                $orderBy = $allowedOrderBy[0];
                $this->allowedOrderBy = $settingsAllowedOrderBy;
            } else {
                $orderBy = $orderDefault;
                $this->allowedOrderBy = $orderBy;
            }
        }
        if ($this->settings['orderDirection']!='') {
            $orderDirection = $this->settings['orderDirection'];
        } else {
            $orderDirection = 'ASC';
        }
        $this->orderings = array($orderBy=>$orderDirection);
        
        // set plugin group fields
        $this->fieldsGroups = $this->helperService->getPluginFields('fe_groups', 'mooxcommunity', 'all');
        
        // set plugin group fields
        $this->fieldsUsers = $this->helperService->getPluginFields('fe_users', 'mooxcommunity', 'all');
        
        // initalize storage settings
        $this->initializeStorageSettings();
        
        // set field config groups
        $this->fieldConfigGroups = $this->helperService->getFieldConfig($this->fieldsGroups, $this->settings);
        
        // set field config users
        $this->fieldConfigUsers = $this->helperService->getFieldConfig($this->fieldsUsers, $this->settings);
    }

    /**
     * initialize storage settings
     *
     * @return void
     */
    protected function initializeStorageSettings()
    {
            
        // get typoscript configuration
        $configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
        
        // set storage pid if set by plugin
        if ($this->settings['storagePid']!='') {
            $this->setStoragePids(explode(',', $this->settings['storagePid']));
        } else {
            $this->setStoragePids(array());
        }
        
        // overwrite extbase persistence storage pid
        if (!empty($this->settings['storagePid']) && $this->settings['storagePid']!='TS') {
            if (empty($configuration['persistence']['storagePid'])) {
                $storagePids['persistence']['storagePid'] = $this->settings['storagePid'];
                $this->configurationManager->setConfiguration(array_merge($configuration, $storagePids));
            } else {
                $configuration['persistence']['storagePid'] = $this->settings['storagePid'];
                $this->configurationManager->setConfiguration($configuration);
            }
        }
    }
    
    /**
     * action list groups
     *
     * @param \array $filter
     * @param \array $search
     * @return void
     */
    public function listAdminGroupsAction($filter = null, $search = null)
    {
        
        // init action arrays
        $messages = array();
                
        // set default ordering
        if (!$order) {
            $order['by'] = key($this->orderings);
            $order['dir'] = current($this->orderings);
        }
        
        // get order by fields set by plugin
        $orderByFields = $this->helperService->configureFields($this->allowedOrderBy, '', $this->settings, $this->fieldConfigGroups);
        
        // set variant filter
        $filter['variant'] = 'moox_community';
        
        // get only admin groups
        if (true === $this->accessControllService->hasLoggedInFrontendUser()) {
            $feUser = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());
            if ($feUser) {
                $filter['communityAdmin'] = $feUser;
            }
        }
        
        // set search filter
        if ($search) {
            $search['fields'] = array('title','info');
            $filter['search'] = $search;
        }
        
        // get items
        $items = $this->frontendUserGroupRepository->findByFilter($filter, $this->orderings, null, null, $this->storagePids, null);
        
        // get memberships not confirmed
        $feGroups = array();
        foreach ($items as $feGroup) {
            $feGroups[] = $feGroup->getUid();
        }
        $memberships = $this->membershipRepository->findByFilter(array('feGroups'=>$feGroups,'confirmed'=>0), $this->orderings, null, null, $this->storagePids, null);
        
        // set template variables
        $this->view->assign('filter', $filter);
        $this->view->assign('search', $search);
        $this->view->assign('order', $order);
        $this->view->assign('orderByFields', $orderByFields);
        $this->view->assign('pagination', $this->pagination);
        $this->view->assign('items', $items);
        $this->view->assign('memberships', $memberships);
        $this->view->assign('listGroupsPid', $this->settings['listGroupsPid']);
        $this->view->assign('returnUrl', $this->uriBuilder->getRequest()->getRequestUri());
        $this->view->assign('action', 'listAdminGroups');
    }

    /**
     * action list memberships
     *
     * @param array $filter
     * @param array $search
     * @return void
     */
    public function listMembershipsAction($filter = array(), $search = array())
    {
        // init action arrays
        $feUser = null;
        $order['by'] = key($this->orderings);
        $order['dir'] = current($this->orderings);

        // check and get logged in user info
        if (true === $this->accessControllService->hasLoggedInFrontendUser()) {
            $feUser = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());
            if ($feUser) {
                $memberships = $this->membershipRepository->findByFilter(
                    array(
                        'feUser' => $feUser,
                        'confirmed' => 1
                    ),
                    null,
                    null,
                    null,
                    $this->storagePids,
                    null
                );

                foreach ($memberships as $membership) {
                    if (!is_null($membership->getFeGroup())) {
                        $filter['uids'][] = $membership->getFeGroup()->getUid();
                    }
                }
            }
        }
        
        // get order by fields set by plugin
        $orderByFields = $this->helperService->configureFields(
            $this->allowedOrderBy,
            '',
            $this->settings,
            $this->fieldConfigGroups
        );
        
        // set search filter
        if ($search) {
            $search['fields'] = array('title', 'info');
            $filter['search'] = $search;
        }

        // set template variables
        $this->view->assignMultiple(
            array(
                'filter' => $filter,
                'search' => $search,
                'order' => $order,
                'orderByFields' => $orderByFields,
                'pagination' => $this->pagination,
                'items' => $this->frontendUserGroupRepository->findByFilter(
                    $filter,
                    $this->orderings,
                    null,
                    null,
                    $this->storagePids,
                    null
                ),
                'feUser' => $feUser,
                'returnUrl' => $this->uriBuilder->getRequest()->getRequestUri(),
                'action' => 'listMemberships'
            )
        );
    }
    
    /**
     * action list group memberships
     *
     * @param \int $item
     * @param \string $returnUrl
     * @param \array $search
     * @return void
     */
    public function listGroupMembershipsAction($item = null, $returnUrl = null, $search = null)
    {
        // init action arrays
        $members = array();
        
        // check and get logged in user info
        if ($item > 0) {
            $item = $this->frontendUserGroupRepository->findByUid($item);
            if (is_object($item)) {
                if (true === $this->accessControllService->hasLoggedInFrontendUser()) {
                    $feUser = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());
                    if ($this->accessControllService->isGroupEditAllowed($feUser, $item)) {
                        $memberships = $this->membershipRepository->findByFilter(
                            array(
                                'feGroups' => array($item->getUid()),
                                'confirmed' => 1
                            ),
                            null,
                            null,
                            null,
                            $this->storagePids,
                            null
                        );
                        foreach ($memberships as $membership) {
                            if (!is_null($membership->getFeUser())) {
                                $members[] = $membership->getFeUser()->getUid();
                            }
                        }
                    }
                }
            }
        }
        
        // set default ordering
        if (!$order) {
            $order['by'] = key($this->orderings);
            $order['dir'] = current($this->orderings);
        }
        
        // get order by fields set by plugin
        $orderByFields = $this->helperService->configureFields($this->allowedOrderBy, '', $this->settings, $this->fieldConfigUsers);
                
        // get only member users
        $filter['uids'] = $members;
        
        // set search filter
        if ($search) {
            $search['fields'] = array('username','last_name','first_name');
            $filter['search'] = $search;
        }

        // set template variables
        $this->view->assignMultiple(
            array(
                'filter' => $filter,
                'search' => $search,
                'order' => $order,
                'orderByFields' => $orderByFields,
                'pagination' => $this->pagination,
                'items' => $this->frontendUserRepository->findByFilter(
                    $filter,
                    $this->orderings,
                    null,
                    null,
                    $this->storagePids,
                    null
                ),
                'feGroup' => $item,
                'returnUrl' => $this->uriBuilder->getRequest()->getRequestUri(),
                'action' => 'listGroupMemberships'
            )
        );
    }
    
    /**
     * action confirm membership
     *
     * @param \int $item
     * @param \string $returnUrl
     * @return void
     */
    public function confirmMembershipAction($item = null, $returnUrl = null)
    {
        
        // init action arrays and booleans
        $messages = array();
        $confirmable = false;
        
        // check and get logged in user info
        if ($item>0) {
            $item = $this->membershipRepository->findByUid($item);
            if (is_object($item)) {
                $readable = true;
                if (true === $this->accessControllService->hasLoggedInFrontendUser()) {
                    $adminFeUser = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());
                    $feUser = $item->getFeUser();
                    $feGroup = $item->getFeGroup();
                    if ($this->accessControllService->isGroupEditAllowed($adminFeUser, $feGroup)) {
                        $confirmable = true;
                    }
                }
            }
        }
        
        // if membership removeable
        if ($confirmable) {
            $item->setConfirmed(time());
            
            $this->membershipRepository->update($item);
            $this->persistenceManager->persistAll();
            
            // if notifications should be sent
            if ($this->settings['notificationsEmailTemplate'] && $feUser->getEmail()!='') {
                
                // load mail template
                $template = $this->templateRepository->findByUid($this->settings['notificationsEmailTemplate']);
                
                // if mail template exists
                if ($template) {
                    
                    // set member mail field
                    $member = array();
                    foreach (array('username','first_name','last_name','auto_name','email','gender') as $fieldname) {
                        $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                        if (method_exists($feUser, $getMethod)) {
                            $member[$fieldname] = $feUser->$getMethod();
                        }
                    }
                    
                    // set group mail field
                    $group = array();
                    foreach (array('title') as $fieldname) {
                        $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                        if (method_exists($feGroup, $getMethod)) {
                            $group[$fieldname] = $feGroup->$getMethod();
                        }
                    }
                    
                    // set info mail array
                    $mail = array(
                        'sender_name' => $this->settings['notificationsSenderName'],
                        'sender_address' => $this->settings['notificationsSenderAddress'],
                        'receiver_name' => $feUser->getAutoName(),
                        'receiver_address' => $feUser->getEmail(),
                        'subject' => $this->helperService->prepareMailSubject($template->getSubject(), $member),
                        'pid' => $GLOBALS['TSFE']->id,
                        'member' => $member,
                        'group' => $group,
                        'notificationPartial' => 'MembershipConfirmed'
                    );
                    
                    // set mail body
                    $mail['body'] = $this->helperService->prepareMailBody($template, $mail);
                    
                    // send mail
                    $this->helperService->sendMail($mail);
                }
            }
                        
            // add message
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH.'pi5.action_confirm_membership', $this->extensionName),
                'text' => LocalizationUtility::translate(self::LLPATH.'pi5.action_confirm_membership.success', $this->extensionName),
                'type' => FlashMessage::OK
            );
        }
        
        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
        
        // redirect to list view
        if ($returnUrl!='') {
            $this->redirectToUri($returnUrl);
        } else {
            $this->redirect('listAdminGroups');
        }
        exit();
    }
    
    /**
     * action reject membership
     *
     * @param \int $item
     * @param \string $returnUrl
     * @return void
     */
    public function rejectMembershipAction($item = null, $returnUrl = null)
    {
        
        // init action arrays and booleans
        $messages = array();
        $rejectable = false;
        
        // check and get logged in user info
        if ($item>0) {
            $item = $this->membershipRepository->findByUid($item);
            if (is_object($item)) {
                $readable = true;
                if (true === $this->accessControllService->hasLoggedInFrontendUser()) {
                    $adminFeUser = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());
                    $feUser = $item->getFeUser();
                    $feGroup = $item->getFeGroup();
                    if ($this->accessControllService->isGroupEditAllowed($adminFeUser, $feGroup)) {
                        $rejectable = true;
                    }
                }
            }
        }
        
        // if membership removeable
        if ($rejectable) {
            $item->setRejected(time());
            
            $this->membershipRepository->update($item);
            $this->persistenceManager->persistAll();
            $item = $this->membershipRepository->findByUid($item->getUid());
            $this->membershipRepository->remove($item);
            $this->persistenceManager->persistAll();
            
            // if notifications should be sent
            if ($this->settings['notificationsEmailTemplate'] && $feUser->getEmail()!='') {
                
                // load mail template
                $template = $this->templateRepository->findByUid($this->settings['notificationsEmailTemplate']);
                
                // if mail template exists
                if ($template) {
                    
                    // set member mail field
                    $member = array();
                    foreach (array('username','first_name','last_name','auto_name','email','gender') as $fieldname) {
                        $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                        if (method_exists($feUser, $getMethod)) {
                            $member[$fieldname] = $feUser->$getMethod();
                        }
                    }
                    
                    // set group mail field
                    $group = array();
                    foreach (array('title') as $fieldname) {
                        $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                        if (method_exists($feGroup, $getMethod)) {
                            $group[$fieldname] = $feGroup->$getMethod();
                        }
                    }
                    
                    // set info mail array
                    $mail = array(
                        'sender_name' => $this->settings['notificationsSenderName'],
                        'sender_address' => $this->settings['notificationsSenderAddress'],
                        'receiver_name' => $feUser->getAutoName(),
                        'receiver_address' => $feUser->getEmail(),
                        'subject' => $this->helperService->prepareMailSubject($template->getSubject(), $member),
                        'pid' => $GLOBALS['TSFE']->id,
                        'member' => $member,
                        'group' => $group,
                        'notificationPartial' => 'MembershipRejected'
                    );
                    
                    // set mail body
                    $mail['body'] = $this->helperService->prepareMailBody($template, $mail);
                    
                    // send mail
                    $this->helperService->sendMail($mail);
                }
            }
                        
            // add message
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH.'pi5.action_reject_membership', $this->extensionName),
                'text' => LocalizationUtility::translate(self::LLPATH.'pi5.action_reject_membership.success', $this->extensionName),
                'type' => FlashMessage::OK
            );
        }
        
        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
        
        // redirect to list view
        if ($returnUrl!='') {
            $this->redirectToUri($returnUrl);
        } else {
            $this->redirect('listAdminGroups');
        }
        exit();
    }
    
    /**
     * action remove membership
     *
     * @param \int $item
     * @param \int $feUser
     * @param \string $returnUrl
     * @return void
     */
    public function removeMembershipAction($item = null, $feUser = null, $returnUrl = null)
    {
        
        // init action arrays and booleans
        $messages = array();
        $removeable = false;
        
        // check and get logged in user info
        if ($item>0) {
            $item = $this->frontendUserGroupRepository->findByUid($item);
            if (is_object($item)) {
                $readable = true;
                $feUser = $this->frontendUserRepository->findByUid($feUser);
                if ($feUser && true === $this->accessControllService->hasLoggedInFrontendUser()) {
                    $adminFeUser = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());
                    if ($feUser->getUid()==$adminFeUser->getUid() || $this->accessControllService->isGroupEditAllowed($adminFeUser, $item)) {
                        $membership = $this->membershipRepository->findByFeUserAndFeGroup($feUser, $item);
                        if ($membership) {
                            $removeable = true;
                        }
                    }
                }
            }
        }
        
        // if membership removeable
        if ($removeable) {
            $this->membershipRepository->remove($membership);
            $this->persistenceManager->persistAll();
            
            // add message
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH.'pi5.action_remove_membership', $this->extensionName),
                'text' => LocalizationUtility::translate(self::LLPATH.'pi5.action_remove_membership.success', $this->extensionName),
                'type' => FlashMessage::OK
            );
        }
        
        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
        
        // redirect to list view
        if ($returnUrl!='') {
            $this->redirectToUri($returnUrl);
        } else {
            $this->redirect('listMemberships', null, null, array('item'=>$item,'returnUrl'=>$returnUrl));
        }
        exit();
    }
    
    /**
     * action add moderator
     *
     * @param \int $item
     * @param \int $feUser
     * @param \string $returnUrl
     * @return void
     */
    public function addModeratorAction($item = null, $feUser = null, $returnUrl = null)
    {
        
        // init action arrays and booleans
        $messages = array();
        $administrateable = false;
        
        // check and get logged in user info
        if ($item>0) {
            $item = $this->frontendUserGroupRepository->findByUid($item);
            if (is_object($item)) {
                $readable = true;
                $feUser = $this->frontendUserRepository->findByUid($feUser);
                if ($feUser && true === $this->accessControllService->hasLoggedInFrontendUser()) {
                    $adminFeUser = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());
                    if ($this->accessControllService->isGroupEditAllowed($adminFeUser, $item)) {
                        $administrateable = true;
                    }
                }
            }
        }
        
        // if group administrateable
        if ($administrateable) {
            
            // add user to moderators
            if (!$item->getCommunityModerators()->contains($feUser)) {
                $item->getCommunityModerators()->attach($feUser);
            }
            
            // save changes to database
            $this->frontendUserGroupRepository->update($item);
            $this->persistenceManager->persistAll();
            
            // if notifications should be sent
            if ($this->settings['notificationsEmailTemplate'] && $feUser->getEmail()!='') {
                
                // load mail template
                $template = $this->templateRepository->findByUid($this->settings['notificationsEmailTemplate']);
                
                // if mail template exists
                if ($template) {
                    
                    // set moderator mail field
                    $moderator = array();
                    foreach (array('username','first_name','last_name','auto_name','email','gender') as $fieldname) {
                        $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                        if (method_exists($feUser, $getMethod)) {
                            $moderator[$fieldname] = $feUser->$getMethod();
                        }
                    }
                    
                    // set group mail field
                    $group = array();
                    foreach (array('title') as $fieldname) {
                        $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                        if (method_exists($item, $getMethod)) {
                            $group[$fieldname] = $item->$getMethod();
                        }
                    }
                    
                    // set info mail array
                    $mail = array(
                        'sender_name' => $this->settings['notificationsSenderName'],
                        'sender_address' => $this->settings['notificationsSenderAddress'],
                        'receiver_name' => $feUser->getAutoName(),
                        'receiver_address' => $feUser->getEmail(),
                        'subject' => $this->helperService->prepareMailSubject($template->getSubject(), $member),
                        'pid' => $GLOBALS['TSFE']->id,
                        'moderator' => $moderator,
                        'group' => $group,
                        'notificationPartial' => 'ModeratorPromoted'
                    );
                    
                    // set mail body
                    $mail['body'] = $this->helperService->prepareMailBody($template, $mail);
                    
                    // send mail
                    $this->helperService->sendMail($mail);
                }
            }
            
            // add message
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH.'pi5.action_add_moderator', $this->extensionName),
                'text' => LocalizationUtility::translate(self::LLPATH.'pi5.action_add_moderator.success', $this->extensionName),
                'type' => FlashMessage::OK
            );
        }
        
        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
        
        // redirect to list view
        if ($returnUrl!='') {
            $this->redirect('listGroupMemberships', null, null, array('item'=>$item,'returnUrl'=>$returnUrl));
        } else {
            $this->redirect('listGroupMemberships', null, null, array('item'=>$item));
        }
        exit();
    }
    
    /**
     * action remove moderator
     *
     * @param \int $item
     * @param \int $feUser
     * @param \string $returnUrl
     * @return void
     */
    public function removeModeratorAction($item = null, $feUser = null, $returnUrl = null)
    {
        
        // init action arrays and booleans
        $messages = array();
        $administrateable = false;
        
        // check and get logged in user info
        if ($item>0) {
            $item = $this->frontendUserGroupRepository->findByUid($item);
            if (is_object($item)) {
                $readable = true;
                $feUser = $this->frontendUserRepository->findByUid($feUser);
                if ($feUser && true === $this->accessControllService->hasLoggedInFrontendUser()) {
                    $adminFeUser = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());
                    if ($this->accessControllService->isGroupEditAllowed($adminFeUser, $item)) {
                        $administrateable = true;
                    }
                }
            }
        }
        
        // if group administrateable
        if ($administrateable) {
            
            // remove user as moderators
            if ($item->getCommunityModerators()->contains($feUser)) {
                $item->getCommunityModerators()->detach($feUser);
            }
            
            // save changes to database
            $this->frontendUserGroupRepository->update($item);
            $this->persistenceManager->persistAll();
            
            // add message
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH.'pi5.action_remove_moderator', $this->extensionName),
                'text' => LocalizationUtility::translate(self::LLPATH.'pi5.action_remove_moderator.success', $this->extensionName),
                'type' => FlashMessage::OK
            );
        }
        
        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
        
        // redirect to list view
        if ($returnUrl!='') {
            $this->redirect('listGroupMemberships', null, null, array('item'=>$item,'returnUrl'=>$returnUrl));
        } else {
            $this->redirect('listGroupMemberships', null, null, array('item'=>$item));
        }
        exit();
    }
    
    /**
     * action list friendships
     *
     * @param \array $filter
     * @param \array $search
     * @return void
     */
    public function listFriendshipsAction($filter = null, $search = null)
    {
        
        // init action arrays
        $messages = array();
        $friends = array();
        
        // check and get logged in user info
        $friends[] = 0;
        if (true === $this->accessControllService->hasLoggedInFrontendUser()) {
            $feUser = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());
            if ($feUser) {
                $friendships = $this->friendshipRepository->findByFilter(array('friend'=>$feUser,'confirmed'=>1), null, null, null, $this->storagePids, null);
                foreach ($friendships as $friendship) {
                    if (is_object($friendship->getFeUser1()) && is_object($friendship->getFeUser2())) {
                        if ($friendship->getFeUser1()->getUid()!=$feUser->getUid()) {
                            $friends[] = $friendship->getFeUser1()->getUid();
                        } else {
                            $friends[] = $friendship->getFeUser2()->getUid();
                        }
                    }
                }
            }
        }
    
        // set default ordering
        if (!$order) {
            $order['by'] = key($this->orderings);
            $order['dir'] = current($this->orderings);
        }
        
        // get order by fields set by plugin
        $orderByFields = $this->helperService->configureFields($this->allowedOrderBy, '', $this->settings, $this->fieldConfigUsers);
        
        // get only friends
        $filter['uids'] = $friends;
        
        // set search filter
        if ($search) {
            $search['fields'] = array('title','info');
            $filter['search'] = $search;
        }
        
        // get items
        $items = $this->frontendUserRepository->findByFilter($filter, $this->orderings, null, null, $this->storagePids, null);
        
        // get unconfirmed friendships
        $friendships = $this->friendshipRepository->findByFilter(array('feUser1'=>$feUser,'confirmed'=>0), $this->orderings, null, null, $this->storagePids, null);
        
        // set template variables
        $this->view->assign('filter', $filter);
        $this->view->assign('search', $search);
        $this->view->assign('order', $order);
        $this->view->assign('orderByFields', $orderByFields);
        $this->view->assign('pagination', $this->pagination);
        $this->view->assign('items', $items);
        $this->view->assign('friendships', $friendships);
        $this->view->assign('feUser', $feUser);
        $this->view->assign('listGroupsPid', $this->settings['listGroupsPid']);
        $this->view->assign('returnUrl', $this->uriBuilder->getRequest()->getRequestUri());
        $this->view->assign('action', 'listFriendships');
    }
    
    /**
     * action confirm friendship
     *
     * @param \int $item
     * @param \string $returnUrl
     * @return void
     */
    public function confirmFriendshipAction($item = null, $returnUrl = null)
    {
        
        // init action arrays and booleans
        $messages = array();
        $confirmable = false;
        
        // check and get logged in user info
        if ($item>0) {
            $item = $this->friendshipRepository->findByUid($item);
            if (is_object($item)) {
                $readable = true;
                if (true === $this->accessControllService->hasLoggedInFrontendUser()) {
                    $feUser = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());
                    $feUser1 = $item->getFeUser1();
                    $feUser2 = $item->getFeUser2();
                    if ($feUser1->getUid() == $feUser->getUid()) {
                        $confirmable = true;
                    }
                }
            }
        }
        
        // if membership removeable
        if ($confirmable) {
            $item->setConfirmed(time());
            
            $this->friendshipRepository->update($item);
            $this->persistenceManager->persistAll();
            
            // if notifications should be sent
            if ($this->settings['notificationsEmailTemplate'] && $feUser2->getEmail()!='') {
                
                // load mail template
                $template = $this->templateRepository->findByUid($this->settings['notificationsEmailTemplate']);
                
                // if mail template exists
                if ($template) {
                    
                    // set friend mail field
                    $friend = array();
                    foreach (array('username','first_name','last_name','auto_name','email','gender') as $fieldname) {
                        $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                        if (method_exists($feUser2, $getMethod)) {
                            $friend[$fieldname] = $feUser2->$getMethod();
                        }
                    }
                    
                    // set user mail field
                    $user = array();
                    foreach (array('username','first_name','last_name','auto_name','email','gender') as $fieldname) {
                        $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                        if (method_exists($feUser1, $getMethod)) {
                            $user[$fieldname] = $feUser1->$getMethod();
                        }
                    }
                    
                    // set info mail array
                    $mail = array(
                        'sender_name' => $this->settings['notificationsSenderName'],
                        'sender_address' => $this->settings['notificationsSenderAddress'],
                        'receiver_name' => $feUser2->getAutoName(),
                        'receiver_address' => $feUser2->getEmail(),
                        'subject' => $this->helperService->prepareMailSubject($template->getSubject(), $member),
                        'pid' => $GLOBALS['TSFE']->id,
                        'user' => $user,
                        'friend' => $friend,
                        'notificationPartial' => 'FriendshipConfirmed'
                    );
                    
                    // set mail body
                    $mail['body'] = $this->helperService->prepareMailBody($template, $mail);
                    
                    // send mail
                    $this->helperService->sendMail($mail);
                }
            }
                        
            // add message
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH.'pi5.action_confirm_friendship', $this->extensionName),
                'text' => LocalizationUtility::translate(self::LLPATH.'pi5.action_confirm_friendship.success', $this->extensionName),
                'type' => FlashMessage::OK
            );
        }
        
        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
        
        // redirect to list view
        if ($returnUrl!='') {
            $this->redirectToUri($returnUrl);
        } else {
            $this->redirect('listFriendships');
        }
        exit();
    }
    
    /**
     * action reject friendship
     *
     * @param \int $item
     * @param \string $returnUrl
     * @return void
     */
    public function rejectFriendshipAction($item = null, $returnUrl = null)
    {
        
        // init action arrays and booleans
        $messages = array();
        $rejectable = false;
        
        // check and get logged in user info
        if ($item>0) {
            $item = $this->friendshipRepository->findByUid($item);
            if (is_object($item)) {
                $readable = true;
                if (true === $this->accessControllService->hasLoggedInFrontendUser()) {
                    $feUser = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());
                    $feUser1 = $item->getFeUser1();
                    $feUser2 = $item->getFeUser2();
                    if ($feUser1->getUid() == $feUser->getUid()) {
                        $rejectable = true;
                    }
                }
            }
        }
        
        // if membership removeable
        if ($rejectable) {
            $item->setRejected(time());
            
            $this->friendshipRepository->update($item);
            $this->persistenceManager->persistAll();
            $item = $this->friendshipRepository->findByUid($item->getUid());
            $this->friendshipRepository->remove($item);
            $this->persistenceManager->persistAll();
            
            // if notifications should be sent
            if ($this->settings['notificationsEmailTemplate'] && $feUser2->getEmail()!='') {
                
                // load mail template
                $template = $this->templateRepository->findByUid($this->settings['notificationsEmailTemplate']);
                
                // if mail template exists
                if ($template) {
                    
                    // set friend mail field
                    $friend = array();
                    foreach (array('username','first_name','last_name','auto_name','email','gender') as $fieldname) {
                        $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                        if (method_exists($feUser2, $getMethod)) {
                            $friend[$fieldname] = $feUser2->$getMethod();
                        }
                    }
                    
                    // set user mail field
                    $user = array();
                    foreach (array('username','first_name','last_name','auto_name','email','gender') as $fieldname) {
                        $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                        if (method_exists($feUser1, $getMethod)) {
                            $user[$fieldname] = $feUser1->$getMethod();
                        }
                    }
                    
                    // set info mail array
                    $mail = array(
                        'sender_name' => $this->settings['notificationsSenderName'],
                        'sender_address' => $this->settings['notificationsSenderAddress'],
                        'receiver_name' => $feUser2->getAutoName(),
                        'receiver_address' => $feUser2->getEmail(),
                        'subject' => $this->helperService->prepareMailSubject($template->getSubject(), $member),
                        'pid' => $GLOBALS['TSFE']->id,
                        'user' => $user,
                        'friend' => $friend,
                        'notificationPartial' => 'FriendshipRejected'
                    );
                    
                    // set mail body
                    $mail['body'] = $this->helperService->prepareMailBody($template, $mail);
                    
                    // send mail
                    $this->helperService->sendMail($mail);
                }
            }
                        
            // add message
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH.'pi5.action_reject_friendship', $this->extensionName),
                'text' => LocalizationUtility::translate(self::LLPATH.'pi5.action_reject_friendship.success', $this->extensionName),
                'type' => FlashMessage::OK
            );
        }
        
        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
        
        // redirect to list view
        if ($returnUrl!='') {
            $this->redirectToUri($returnUrl);
        } else {
            $this->redirect('listFriendships');
        }
        exit();
    }
    
    /**
     * action remove friendship
     *
     * @param \int $item
     * @param \string $returnUrl
     * @return void
     */
    public function removeFriendshipAction($item = null, $returnUrl = null)
    {
        
        // init action arrays and booleans
        $messages = array();
        $removeable = false;
        
        // check and get logged in user info
        if ($item>0) {
            $item = $this->frontendUserRepository->findByUid($item);
            if (is_object($item)) {
                $readable = true;
                if (true === $this->accessControllService->hasLoggedInFrontendUser()) {
                    $feUser = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());
                    $friendship = $this->friendshipRepository->findByFeUserAndFriend($feUser, $item);
                    if ($friendship) {
                        $removeable = true;
                    }
                }
            }
        }
        
        // if membership removeable
        if ($removeable) {
            $this->friendshipRepository->remove($friendship);
            $this->persistenceManager->persistAll();
            
            // add message
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH.'pi5.action_remove_friendship', $this->extensionName),
                'text' => LocalizationUtility::translate(self::LLPATH.'pi5.action_remove_friendship.success', $this->extensionName),
                'type' => FlashMessage::OK
            );
        }
        
        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
        
        // redirect to list view
        if ($returnUrl!='') {
            $this->redirectToUri($returnUrl);
        } else {
            $this->redirect('listFriendships', null, null, array('item'=>$item,'returnUrl'=>$returnUrl));
        }
        exit();
    }
    
    /**
     * action error
     *
     * @return void
     */
    public function errorAction()
    {
        $this->view->assign('action', 'error');
    }
    
    /**
     * Returns storage pids
     *
     * @return \array
     */
    public function getStoragePids()
    {
        return $this->storagePids;
    }

    /**
     * Set storage pids
     *
     * @param \array $storagePids storage pids
     * @return void
     */
    public function setStoragePids($storagePids)
    {
        $this->storagePids = $storagePids;
    }
    
    /**
     * Returns fields groups
     *
     * @return \array
     */
    public function getFieldsGroups()
    {
        return $this->fieldsGroups;
    }

    /**
     * Set fields groups
     *
     * @param \array $fields
     * @return void
     */
    public function setFieldsGroups($fieldsGroups)
    {
        $this->fieldsGroups = $fieldsGroups;
    }
    
    /**
     * Returns fields users
     *
     * @return \array
     */
    public function getFieldsUsers()
    {
        return $this->fieldsUsers;
    }

    /**
     * Set fields
     *
     * @param \array $fieldsUsers
     * @return void
     */
    public function setFieldsUsers($fieldsUsers)
    {
        $this->fieldsUsers = $fieldsUsers;
    }
    
    /**
     * Returns fieldConfig
     *
     * @return \array
     */
    public function getFieldConfig()
    {
        return $this->fieldConfig;
    }

    /**
     * Set fieldConfig
     *
     * @param \array $fieldConfig fieldConfig
     * @return void
     */
    public function setFieldConfig($fieldConfig)
    {
        $this->fieldConfig = $fieldConfig;
    }
    
    /**
     * Returns ext conf
     *
     * @return \array
     */
    public function getExtConf()
    {
        return $this->extConf;
    }

    /**
     * Set ext conf
     *
     * @param \array $extConf ext conf
     * @return void
     */
    public function setExtConf($extConf)
    {
        $this->extConf = $extConf;
    }
}
