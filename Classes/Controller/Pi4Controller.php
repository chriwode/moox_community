<?php
namespace DCNGmbH\MooxCommunity\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
 
use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use \TYPO3\CMS\Core\Messaging\FlashMessage;
use \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

/**
 *
 *
 * @package moox_community
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Pi4Controller extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * accessControllService
     *
     * @var \DCNGmbH\MooxCommunity\Service\AccessControlService
     * @inject
     */
    protected $accessControllService;
    
    /**
     * storageRepository
     *
     * @var \TYPO3\CMS\Core\Resource\StorageRepository
     * @inject
     */
    protected $storageRepository;
    
    /**
     * fileRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\FileRepository
     * @inject
     */
    protected $fileRepository;
    
    /**
     * frontendUserRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository;
    
    /**
     * frontendUserGroupRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\FrontendUserGroupRepository
     * @inject
     */
    protected $frontendUserGroupRepository;
    
    /**
     * membershipRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\MembershipRepository
     * @inject
     */
    protected $membershipRepository;
    
    /**
     * friendshipRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\FriendshipRepository
     * @inject
     */
    protected $friendshipRepository;
    
    /**
     * newsRepository
     *
     * @var \DCNGmbH\MooxNewsFrontend\Domain\Repository\NewsRepository
     * @inject
     */
    protected $newsRepository;
    
    /**
     * templateRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\TemplateRepository
     * @inject
     */
    protected $templateRepository;
    
    /**
     * helperService
     *
     * @var \DCNGmbH\MooxCommunity\Service\HelperService
     * @inject
     */
    protected $helperService;
    
    /**
     * persistenceManager
     *
     * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
     * @inject
     */
    protected $persistenceManager;
    
    /**
     * extConf
     *
     * @var boolean
     */
    protected $extConf;
    
    /**
     * storagePids
     *
     * @var array
     */
    protected $storagePids;
    
    /**
     * fileStorageUid
     *
     * @var int
     */
    protected $fileStorageUid;
    
    /**
     * fileStorageFolder
     *
     * @var int
     */
    protected $fileStorageFolder;
        
    /**
     * fieldsUsers
     *
     * @var \array
     */
    protected $fieldsUsers;
    
    /**
     * fieldsGroups
     *
     * @var \array
     */
    protected $fieldsGroups;
    
    /**
     * fieldConfigUsers
     *
     * @var \array
     */
    protected $fieldConfigUsers;
    
    /**
     * fieldConfigGroups
     *
     * @var \array
     */
    protected $fieldConfigGroups;
    
    /**
     * pagination
     *
     * @var \array
     */
    protected $pagination;
    
    /**
     * orderings
     *
     * @var \array
     */
    protected $orderings;
    
    /**
     * allowedOrderBy
     *
     * @var \string
     */
    protected $allowedOrderBy;
    
    /**
     * streamTypes
     *
     * @var \array
     */
    protected $streamTypes = null;
    
    
    /**
     * Path to the locallang file
     * @var string
     */
    const LLPATH = 'LLL:EXT:moox_community/Resources/Private/Language/locallang.xlf:';
    
    /**
     * temp folder
     * @var string
     */
    const TEMPFOLDER = 'moox_community';
    
    /**
     * initialize action
     *
     * @return void
     */
    public function initializeAction()
    {
        
        // execute parent initialize action
        parent::initializeAction();
        
        // load extension configuration
        $this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_community']);
        
        // set items per page
        if ($this->settings['itemsPerPage']>0) {
            $this->pagination['itemsPerPage'] = $this->settings['itemsPerPage'];
        } else {
            $this->pagination['itemsPerPage'] = 10000;
        }
        
        // set orderings
        if ($this->settings['orderBy']!='') {
            $orderBy = $this->settings['orderBy'];
            $this->allowedOrderBy = $orderBy;
        } else {
            if ($this->settings['allowedOrderBy']!='') {
                $allowedOrderBy = explode(',', $this->settings['allowedOrderBy']);
                $orderBy = $allowedOrderBy[0];
                $this->allowedOrderBy = $this->settings['allowedOrderBy'];
            } else {
                $orderBy = 'title';
                $this->allowedOrderBy = $orderBy;
            }
        }
        if ($this->settings['orderDirection']!='') {
            $orderDirection = $this->settings['orderDirection'];
        } else {
            $orderDirection = 'ASC';
        }
        $this->orderings = array($orderBy=>$orderDirection);
        
        // set file storage uid
        if ($this->settings['fileStorage']!='') {
            $this->fileStorageUid = $this->settings['fileStorage'];
        } else {
            $this->fileStorageUid = 1;
        }
        
        // set file storage folder
        if ($this->settings['fileStorageFolder']!='') {
            $this->fileStorageFolder = $this->settings['fileStorageFolder'];
        } else {
            $this->fileStorageFolder = 'moox_community';
        }
        
        // set plugin group fields
        $this->fieldsGroups = $this->helperService->getPluginFields('fe_groups', 'mooxcommunity', 'all');
        
        // set plugin group fields
        $this->fieldsUsers = $this->helperService->getPluginFields('fe_users', 'mooxcommunity', 'all');
        
        // initalize storage settings
        $this->initializeStorageSettings();
        
        // set field config groups
        $this->fieldConfigGroups = $this->helperService->getFieldConfig($this->fieldsGroups, $this->settings);
        
        // set field config users
        $this->fieldConfigUsers = $this->helperService->getFieldConfig($this->fieldsUsers, $this->settings);
        
        // check if news extensions is installed to decide if loading of news streams is necessary
        if (ExtensionManagementUtility::isLoaded('moox_news_frontend') && $this->settings['detailGroupLoadStreams'] && $this->settings['detailGroupStreamTypesToLoad']!='') {
            $availableStreamTypes = $this->helperService->getAvailableStreamTypes();
            
            $streamTypes = explode(',', $this->settings['detailGroupStreamTypesToLoad']);
            
            foreach ($streamTypes as $streamType) {
                $this->streamTypes[$streamType] = $availableStreamTypes[$streamType];
            }
        }
    }

    /**
     * initialize storage settings
     *
     * @return void
     */
    protected function initializeStorageSettings()
    {
            
        // get typoscript configuration
        $configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK, $this->extensionName, 'Pi4');
        
        // set storage pid if set by plugin
        if ($this->settings['storagePid']!='') {
            $this->setStoragePids(explode(',', $this->settings['storagePid']));
        } else {
            $this->setStoragePids(array());
        }
        
        // overwrite extbase persistence storage pid
        if (!empty($this->settings['storagePid']) && $this->settings['storagePid']!='TS') {
            if (empty($configuration['persistence']['storagePid'])) {
                $storagePids['persistence']['storagePid'] = $this->settings['storagePid'];
                $this->configurationManager->setConfiguration(array_merge($configuration, $storagePids));
            } else {
                $configuration['persistence']['storagePid'] = $this->settings['storagePid'];
                $this->configurationManager->setConfiguration($configuration);
            }
        }
    }
    
    /**
     * action list groups
     *
     * @param \array $filter
     * @param \array $search
     * @return void
     */
    public function listGroupsAction($filter = null, $search = null)
    {
        
        // init action arrays
        $messages = array();
                
        // set default ordering
        if (!$order) {
            $order['by'] = key($this->orderings);
            $order['dir'] = current($this->orderings);
        }
        
        // get order by fields set by plugin
        $orderByFields = $this->helperService->configureFields($this->allowedOrderBy, '', $this->settings, $this->fieldConfigGroups);
        
        // set variant filter
        $filter['variant'] = 'moox_community';
        
        // set search filter
        if ($search) {
            $search['fields'] = array('title','info');
            $filter['search'] = $search;
        }
        
        // get items
        $items = $this->frontendUserGroupRepository->findByFilter($filter, $this->orderings, null, null, $this->storagePids, null);
            
        // set template variables
        $this->view->assign('filter', $filter);
        $this->view->assign('search', $search);
        $this->view->assign('order', $order);
        $this->view->assign('orderByFields', $orderByFields);
        $this->view->assign('pagination', $this->pagination);
        $this->view->assign('items', $items);
        $this->view->assign('returnUrl', $this->uriBuilder->getRequest()->getRequestUri());
        $this->view->assign('action', 'listGroups');
    }
    
    /**
     * action map groups
     *
     * @param \array $filter
     * @return void
     */
    public function mapGroupsAction($filter = null)
    {
        
        // init action arrays
        $messages = array();
                
        // set default ordering
        if (!$order) {
            $order['by'] = key($this->orderings);
            $order['dir'] = current($this->orderings);
        }
        
        // set variant filter
        $filter['variant'] = 'moox_community';
        
        // get items
        $items = $this->frontendUserGroupRepository->findByFilter($filter, $this->orderings, null, null, $this->storagePids, null);
                
        // set template variables
        $this->view->assign('filter', $filter);
        $this->view->assign('items', $items);
        $this->view->assign('returnUrl', $this->uriBuilder->getRequest()->getRequestUri());
        $this->view->assign('action', 'mapGroups');
    }
    
    /**
     * action detail group
     *
     * @param \int $item
     * @param \string $returnUrl
     * @return void
     */
    public function detailGroupAction($item = null, $returnUrl = null)
    {
        
        // init action arrays and booleans
        $messages = array();
        $fields = array();
        $streams = array();
        $members = array();
        $privateFields = array();
        $readable = false;
        $editable = false;
        $isLoggedIn = false;
        $isMember = false;
        $isUnconfirmedMember = false;
        
        // check and get logged in user info
        if ($item>0) {
            $item = $this->frontendUserGroupRepository->findByUid($item);
            if (is_object($item)) {
                $readable = true;
                if (true === $this->accessControllService->hasLoggedInFrontendUser()) {
                    $feUser = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());
                    if ($feUser) {
                        $isLoggedIn = true;
                        $membership = $this->membershipRepository->findByFeUserAndFeGroup($feUser, $item);
                        if ($membership) {
                            if ($membership->getConfirmed()>0) {
                                $isMember = true;
                                
                                // get private fields set by plugin
                                $privateFields = $this->helperService->configureFields($this->settings['privateDetailGroupFields'], '', $this->settings, $this->fieldConfigGroups);
                            } else {
                                $isUnconfirmedMember = true;
                            }
                        }
                    }
                    if ($feUser && $this->accessControllService->isGroupEditAllowed($feUser, $item)) {
                        $editable = true;
                    }
                }
            }
        }
       
        // if item is readable
        if ($readable) {
            
            // get fields set by plugin
            $fields = $this->helperService->configureFields($this->settings['detailGroupFields'], '', $this->settings, $this->fieldConfigGroups);
            
            // if loading of moox news streams is possible
            if ($this->streamTypes) {
                $newsfilter['feCrgroup'] = $item;
                if (!$isMember) {
                    $newsfilter['privacy'] = 0;
                }
                
                if ($this->settings['detailGroupMergeStreams']) {
                    $newsfilter['types'] = array();
                    ;
                    foreach ($this->streamTypes as $streamType => $streamTypeName) {
                        $newsfilter['types'][] = $streamType;
                    }
                    
                    // get news stream
                    $streams['cumulated']['label'] = LocalizationUtility::translate(self::LLPATH.'pi4.action_detail_group.cumulated_stream', $this->extensionName);
                    $streams['cumulated']['items'] = $this->newsRepository->findByFilter($newsfilter, array('crdate'=>'DESC'), null, null, 'all');
                } else {
                    foreach ($this->streamTypes as $streamType => $streamTypeName) {
                    
                        // set news filter
                        $newsfilter['type'] = $streamType;
                                    
                        // get news stream
                        $streams[$streamType]['label'] = $streamTypeName;
                        $streams[$streamType]['items'] = $this->newsRepository->findByFilter($newsfilter, array('crdate'=>'DESC'), null, null, 'all');
                    }
                }
            }
            
                
            
            // get members if set in plugin options
            if ($this->settings['detailGroupLoadMembers']) {
                $memberships = $this->membershipRepository->findByFilter(array('feGroup'=>$item,'confirmed'=>1), null, null, null, 'all');
                $members = array();
                foreach ($memberships as $membership) {
                    $member = $membership->getFeUser();
                    if (is_object($member)) {
                        $members[] = $member;
                    }
                }
            }
        } else {
            
            // add message
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH.'pi4.action_detail_group', $this->extensionName),
                'text' => LocalizationUtility::translate(self::LLPATH.'pi4.action_detail_group.error.no_access', $this->extensionName),
                'type' => FlashMessage::ERROR
            );
            
            // set flash messages
            $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
            
            // redirect to error view
            $this->redirect('error');
        }
        
        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
        
        // set template variables
        $this->view->assign('item', $item);
        $this->view->assign('fields', $fields);
        $this->view->assign('privateFields', $privateFields);
        $this->view->assign('streams', $streams);
        $this->view->assign('members', $members);
        $this->view->assign('returnUrl', $returnUrl);
        $this->view->assign('editable', $editable);
        $this->view->assign('isLoggedIn', $isLoggedIn);
        $this->view->assign('isMember', $isMember);
        $this->view->assign('isUnconfirmedMember', $isUnconfirmedMember);
        $this->view->assign('action', 'detailGroup');
    }
    
    /**
     * action edit group
     *
     * @param \integer $item
     * @param \array $editGroup
     * @param \string $returnUrl
     * @param \boolean $reload
     * @return void
     */
    public function editGroupAction($item = null, $editGroup = null, $returnUrl = null, $reload = false)
    {
        
        // init action arrays and booleans
        $messages = array();
        $errors = array();
        $fields = array();
        $fileAddOnly = false;
        $editable = false;
        
        // check and get logged in user
        if ($item>0) {
            $item = $this->frontendUserGroupRepository->findByUid($item);
            if (is_object($item)) {
                if (true === $this->accessControllService->hasLoggedInFrontendUser()) {
                    $feUser = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());
                    if ($this->accessControllService->isGroupEditAllowed($feUser, $item)) {
                        $editable = true;
                    }
                }
            }
        }
    
        // if item is editable by current user
        if ($editable) {
                                                        
            // get fields set by plugin
            $fields = $this->helperService->configureFields($this->settings['editGroupFields'], $this->settings['requiredEditGroupFields'], $this->settings, $this->fieldConfigGroups);
            
            // process form values
            if ($editGroup && !$reload) {
                
                // add or remove files by add button
                foreach ($fields as $field) {
                    
                    // if field is a file field
                    if ($field['config']['type']=='file') {
                        
                        // if temporary file form fields are set
                        if (is_array($editGroup[$field['key'].'_tmp'])) {
                            
                            // go through all temporary file form fields
                            foreach ($editGroup[$field['key'].'_tmp'] as $key => $value) {
                                
                                // if remove button for this temporary field is set
                                if (isset($editGroup[$field['key'].'_remove_'.$key])) {
                                    
                                    // prepare get method call
                                    $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($field['key']);
                                    
                                    // prepare remove method call
                                    $removeMethod = 'remove'.GeneralUtility::underscoredToUpperCamelCase($field['key']);
                                    
                                    // if get and remove methods are existing
                                    if (method_exists($item, $getMethod) && method_exists($item, $removeMethod)) {
                                        
                                        // go through all current files
                                        foreach ($item->$getMethod() as $reference) {
                                            
                                            // temporary field key equals reference key from object storage
                                            if ($reference->getUid()==$key) {
                                                
                                                // remove selected file
                                                $item->$removeMethod($reference);
                                                
                                                // save changes to database
                                                $this->frontendUserGroupRepository->update($item);
                                                $this->persistenceManager->persistAll();
                                            }
                                        }
                                    }
                                    
                                    // add message
                                    $messages[] = array(
                                        'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                                        'text' => LocalizationUtility::translate(self::LLPATH.'pi4.action_all.removed.'.$field['config']['reference-type'], $this->extensionName),
                                        'text' => $message,
                                        'type' => FlashMessage::OK
                                    );
                                    
                                    // set flash messages
                                    $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
                                        
                                    // redirect to error view
                                    $this->redirect('editGroup', null, null, array('item'=>$item,'returnUrl'=>$returnUrl));
                                }
                            }
                        }
                        
                        // if form file add button was pressed for this field
                        if (isset($editGroup[$field['key']]) && isset($editGroup[$field['key'].'_add'])) {
                            
                            // if file is selected in this form file field
                            if ($editGroup[$field['key']]['name']!='') {
                                
                                // prepare get method call
                                $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($field['key']);
                                
                                // prepare add method call
                                $addMethod = 'add'.GeneralUtility::underscoredToUpperCamelCase($field['key']);
                                
                                // if get and add methods are existing
                                if (method_exists($item, $getMethod) && method_exists($item, $addMethod)) {
                                    
                                    // get current count of temporary file form fields for this file field
                                    $fileCnt = $item->$getMethod()->count();
                                    
                                    // if maxitems not reached yet for this field
                                    if (!isset($field['config']['maxitems']) || (isset($field['config']['maxitems']) && $fileCnt<$field['config']['maxitems'])) {
                                        
                                        // get file storage
                                        $storage = $this->storageRepository->findByUid($this->fileStorageUid);
                                        
                                        // create file storage folder if not already existing
                                        $folder = $storage->createFolder($this->fileStorageFolder);
                                        
                                        // add file to storage
                                        $fileObject = $storage->addFile(
                                            $editGroup[$field['key']]['tmp_name'], $folder, $editGroup[$field['key']]['name']
                                        );
                                        
                                        // get recently added file object from storage
                                        $fileObject = $storage->getFile($fileObject->getIdentifier());
                                        
                                        // get real file object from file repo
                                        $file = $this->fileRepository->findByUid($fileObject->getProperty('uid'));
                                        
                                        // set reference type depending on field
                                        if ($field['config']['reference-type']=='image') {
                                            $referenceType = 'GroupImageReference';
                                        } else {
                                            $referenceType = 'GroupFileReference';
                                        }
                                        
                                        // set reference extkey
                                        if ($field['config']['extkeyUCC'] && $field['config']['extkeyUCC']!='MooxCommunity') {
                                            $referenceExtkey = $field['config']['extkeyUCC'];
                                        } else {
                                            $referenceExtkey = 'MooxCommunity';
                                        }
                                        
                                        // get new file reference object to reference the added file
                                        $fileReference = $this->objectManager->get('DCNGmbH\\'.$referenceExtkey.'\Domain\Model\\'.$referenceType);
                                        $fileReference->setFile($file);
                                        $fileReference->setCruserId(0);
                                        
                                        // add new file reference to classified
                                        $item->$addMethod($fileReference);
                                        
                                        // save changes to database
                                        $this->frontendUserGroupRepository->update($item);
                                        $this->persistenceManager->persistAll();
                                        
                                        // add message
                                        $messages[] = array(
                                            'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                                            'text' => LocalizationUtility::translate(self::LLPATH.'pi4.action_all.added.'.$field['config']['reference-type'], $this->extensionName),
                                            'text' => $message,
                                            'type' => FlashMessage::OK
                                        );
                                        
                                        // set flash messages
                                        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
                                        
                                        // redirect to error view
                                        $this->redirect('editGroup', null, null, array('item'=>$item,'returnUrl'=>$returnUrl));
                                    } else {
                                        
                                        // add message
                                        $message = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.too_many', $this->extensionName, array($field['config']['maxitems']));
                                        if ($message=='') {
                                            $message = LocalizationUtility::translate(str_replace('moox_marketplace', $field['extkey'], self::LLPATH).'form.'.$field['key'].'.error.too_many', $field['extkey'], array($field['config']['maxitems']));
                                        }
                                        if ($message=='') {
                                            $message = LocalizationUtility::translate(self::LLPATH.'form.error.too_many', $this->extensionName, array($field['config']['maxitems']));
                                        }
                                        $title = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'], $this->extensionName);
                                        if ($title=='') {
                                            $title = LocalizationUtility::translate(str_replace('moox_marketplace', $field['extkey'], self::LLPATH).'form.'.$field['key'], $field['extkey']);
                                        }
                                        $messages[] = array(
                                            'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                                            'title' => $title,
                                            'text' =>  $message,
                                            'type' => FlashMessage::ERROR
                                        );
                                        
                                        // set error
                                        $errors[$field['key']] = true;
                                    }
                                }
                            } else {
                                
                                // add message
                                $message = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.empty', $this->extensionName);
                                if ($message=='') {
                                    $message = LocalizationUtility::translate(str_replace('moox_marketplace', $field['extkey'], self::LLPATH).'form.'.$field['key'].'.error.empty', $field['extkey']);
                                }
                                if ($message=='') {
                                    $message = LocalizationUtility::translate(self::LLPATH.'form.error.empty', $this->extensionName);
                                }
                                $title = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'], $this->extensionName);
                                if ($title=='') {
                                    $title = LocalizationUtility::translate(str_replace('moox_marketplace', $field['extkey'], self::LLPATH).'form.'.$field['key'], $field['extkey']);
                                }
                                $messages[] = array(
                                    'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                                    'title' => $title,
                                    'text' =>  $message,
                                    'type' => FlashMessage::ERROR
                                );

                                // set error
                                $errors[$field['key']] = true;
                            }
                            
                            // set "file add only" to prevent further processing of form values
                            $fileAddOnly = true;
                        }
                    }
                }
                
                // if general save button is pressed
                if (!$fileAddOnly) {
                    
                    // check fields
                    $this->helperService->checkFields($fields, $editGroup, $messages, $errors);
                
                    // set item values
                    foreach ($fields as $field) {
                        if (!$field['passthrough'] && !in_array($field['config']['type'], array('file','tree')) && !in_array($field['key'], array('variant','categories'))) {
                            $setMethod = 'set'.GeneralUtility::underscoredToUpperCamelCase($field['key']);
                            if (method_exists($item, $setMethod) && isset($editGroup[$field['key']])) {
                                if (in_array($field['config']['type'], array('date'))) {
                                    $item->$setMethod($this->helperService->getTimestamp($editGroup[$field['key']]));
                                } elseif (in_array($field['config']['type'], array('datetime'))) {
                                    $item->$setMethod($this->helperService->getTimestamp($editGroup[$field['key']], $editGroup[$field['key'].'_hh'], $editGroup[$field['key'].'_mm']));
                                } else {
                                    $item->$setMethod((is_array($editGroup[$field['key']]))?implode(',', $editGroup[$field['key']]):trim($editGroup[$field['key']]));
                                }
                            }
                        }
                    }

                    // no errors -> save changes
                    if (!count($errors)) {
                                                
                        // set file relations
                        foreach ($fields as $field) {
                            
                            // if field is a file field
                            if (in_array($field['config']['type'], array('file'))) {
                                
                                // if file is selected in this form file field
                                if (isset($editGroup[$field['key']])) {
                                    
                                    // if file is selected in this form file field
                                    if ($editGroup[$field['key']]['name']!='') {
                                        
                                        // prepare get method call
                                        $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($field['key']);
                                        
                                        // prepare add method call
                                        $addMethod = 'add'.GeneralUtility::underscoredToUpperCamelCase($field['key']);
                                        
                                        // if get and add methods are existing
                                        if (method_exists($item, $getMethod) && method_exists($item, $addMethod)) {
                                            
                                            // get file storage
                                            $storage = $this->storageRepository->findByUid($this->fileStorageUid);
                                            
                                            // create file storage folder if not already existing
                                            $folder = $storage->createFolder($this->fileStorageFolder);
                                            
                                            // add file to storage
                                            $fileObject = $storage->addFile(
                                                $editGroup[$field['key']]['tmp_name'], $folder, $editGroup[$field['key']]['name']
                                            );
                                            
                                            // get recently added file object from storage
                                            $fileObject = $storage->getFile($fileObject->getIdentifier());
                                            
                                            // get real file object from file repo
                                            $file = $this->fileRepository->findByUid($fileObject->getProperty('uid'));
                                                    
                                            // set reference type depending on field
                                            if ($field['config']['reference-type']=='image') {
                                                $referenceType = 'GroupImageReference';
                                            } else {
                                                $referenceType = 'GroupFileReference';
                                            }
                                            
                                            // set reference extkey
                                            if ($field['config']['extkeyUCC'] && $field['config']['extkeyUCC']!='MooxCommunity') {
                                                $referenceExtkey = $field['config']['extkeyUCC'];
                                            } else {
                                                $referenceExtkey = 'MooxCommunity';
                                            }
                                            
                                            // get new file reference object to reference the added file
                                            $fileReference = $this->objectManager->get('DCNGmbH\\'.$referenceExtkey.'\Domain\Model\\'.$referenceType);
                                            $fileReference->setFile($file);
                                            $fileReference->setCruserId(0);
                                            
                                            // add new file reference to classified
                                            $item->$addMethod($fileReference);
                                        }
                                    }
                                }
                            }
                        }
                        
                        // save item to database
                        $this->frontendUserGroupRepository->update($item);
                        $this->persistenceManager->persistAll();
                        
                        // add message
                        $messages[] = array(
                            'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                            'title' => LocalizationUtility::translate(self::LLPATH.'pi4.action_edit_group', $this->extensionName),
                            'text' => LocalizationUtility::translate(self::LLPATH.'pi4.action_edit_group.success', $this->extensionName),
                            'type' => FlashMessage::OK
                        );

                        // set flash messages
                        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
                        
                        // redirect to edit view
                        $this->redirect('editGroup', null, null, array('item'=>$item,'returnUrl'=>$returnUrl));
                        exit();
                    }
                }
            }
                
            // preset edit files if existing
            if (!$editGroup && !$reload) {
                
                // go trough all registered fields
                foreach ($fields as $field) {
                    
                    // prepare get method call
                    $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($field['key']);
                    
                    // if get method exists
                    if (method_exists($item, $getMethod)) {
                        
                        // if field is a file field
                        if (in_array($field['config']['type'], array('file'))) {
                                                        
                            // go through all existing files of this field
                            foreach ($item->$getMethod() as $fileReference) {
                                
                                // get file reference
                                $fileReference = $fileReference->getOriginalResource();
                                
                                // get real file
                                $file = $fileReference->getOriginalFile();
                                
                                // get current count of temporary file form fields for this file field
                                $fileCnt = (is_array($editGroup[$field['key'].'_tmp']))?count($editGroup[$field['key'].'_tmp']):0;
                                
                                // set temporary file form fields for existing file
                                $editGroup[$field['key'].'_tmp'][$fileCnt]['name'] = $file->getName();
                                $editGroup[$field['key'].'_tmp'][$fileCnt]['src'] = $file->getPublicUrl();
                                $editGroup[$field['key'].'_tmp'][$fileCnt]['ext'] = $fileReference->getExtension();
                                $editGroup[$field['key'].'_tmp'][$fileCnt]['size'] = $fileReference->getSize();
                                $editGroup[$field['key'].'_tmp'][$fileCnt]['reference'] = $fileReference;
                            }
                        
                        // if field is a tree field
                        } elseif (in_array($field['config']['type'], array('tree'))) {
                            
                            // init tree
                            $treeItems = array();
                            
                            // go through all existing tree items
                            foreach ($item->$getMethod() as $treeItem) {
                                
                                // add to tree
                                $treeItems[] = $treeItem->getUid();
                            }
                            
                            // set temporary file form field for existing tree items
                            $editGroup[$field['key']] = implode(',', $treeItems);
                        } elseif (in_array($field['config']['type'], array('check'))) {
                            
                            // preset form field
                            $editGroup[$field['key']] = ($item->$getMethod()!='')?explode(',', $item->$getMethod()):'';
                        } elseif (in_array($field['config']['type'], array('date')) && $item->$getMethod()!='') {
                            $editGroup[$field['key']] = date('d.m.Y', $item->$getMethod());
                        } elseif (in_array($field['config']['type'], array('datetime')) && $item->$getMethod()!='') {
                            $editGroup[$field['key']] = date('d.m.Y', $item->$getMethod());
                            $editGroup[$field['key'].'_hh'] = date('H', $item->$getMethod());
                            $editGroup[$field['key'].'_mm'] = date('i', $item->$getMethod());
                        } elseif (!$field['passthrough']) {
                            
                            // preset form field
                            $editGroup[$field['key']] = $item->$getMethod();
                        }
                    }
                }
            }
        } else {
            
            // add message
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH.'pi4.action_edit_group', $this->extensionName),
                'text' => LocalizationUtility::translate(self::LLPATH.'pi4.action_edit_group.error.no_access', $this->extensionName),
                'type' => FlashMessage::ERROR
            );
            
            // set flash messages
            $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
            
            // redirect to error view
            $this->redirect('error');
        }
        
        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
        
        // set template variables
        $this->view->assign('item', $item);
        $this->view->assign('editGroup', $editGroup);
        $this->view->assign('fields', $fields);
        $this->view->assign('errors', $errors);
        $this->view->assign('returnUrl', $returnUrl);
        $this->view->assign('action', 'editGroup');
    }
    
    /**
     * action detail user
     *
     * @param \int $item
     * @param \int $feGroup
     * @param \string $returnUrl
     * @return void
     */
    public function detailUserAction($item = null, $feGroup = null, $returnUrl = null)
    {
        
        // init action arrays and booleans
        $messages = array();
        $fields = array();
        $streams = array();
        $friends = array();
        $groups = array();
        $privateFields = array();
        $readable = false;
        $isLoggedIn = false;
        $isFriend = false;
        $isUnconfirmedFriend = false;
        $isMyself = false;
        
        // check and get logged in user info
        if ($item>0) {
            $item = $this->frontendUserRepository->findByUid($item);
            if (is_object($item)) {
                $readable = true;
                
                if (true === $this->accessControllService->hasLoggedInFrontendUser()) {
                    $feUser = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());
                    if ($feUser) {
                        $isLoggedIn = true;
                        if ($feUser->getUid()==$item->getUid()) {
                            $isMyself = true;
                        }
                        $friendship = $this->friendshipRepository->findByFeUserAndFriend($feUser, $item);
                        if ($friendship) {
                            if ($friendship->getConfirmed()>0) {
                                $isFriend = true;
                            } else {
                                $isUnconfirmedFriend = true;
                            }
                        }
                        
                        // get private fields set by plugin
                        if ($isMyself || $isFriend) {
                            $privateFields = $this->helperService->configureFields($this->settings['privateDetailUserFields'], '', $this->settings, $this->fieldConfigUsers);
                        }
                    }
                }
            }
        }
       
        // if item is readable
        if ($readable) {
            
            // get fields set by plugin
            $fields = $this->helperService->configureFields($this->settings['detailUserFields'], '', $this->settings, $this->fieldConfigGroups);
            
            // if loading of moox news streams is possible
            if ($this->streamTypes) {
                $newsfilter['feCruser'] = $item;
                if (!$isFriend && !$isMyself) {
                    $newsfilter['privacy'] = 0;
                }
                
                if ($this->settings['detailUserMergeStreams']) {
                    $newsfilter['types'] = array();
                    ;
                    foreach ($this->streamTypes as $streamType => $streamTypeName) {
                        $newsfilter['types'][] = $streamType;
                    }
                    
                    // get news stream
                    $streams['cumulated']['label'] = LocalizationUtility::translate(self::LLPATH.'pi4.action_detail_user.cumulated_stream', $this->extensionName);
                    $streams['cumulated']['items'] = $this->newsRepository->findByFilter($newsfilter, array('crdate'=>'DESC'), null, null, 'all');
                } else {
                    foreach ($this->streamTypes as $streamType => $streamTypeName) {
                    
                        // set news filter
                        $newsfilter['type'] = $streamType;
                                    
                        // get news stream
                        $streams[$streamType]['label'] = $streamTypeName;
                        $streams[$streamType]['items'] = $this->newsRepository->findByFilter($newsfilter, array('crdate'=>'DESC'), null, null, 'all');
                    }
                }
            }
            
                
            
            // get friends if set in plugin options
            if ($this->settings['detailUserLoadFriends']) {
                $friendships = $this->friendshipRepository->findByFilter(array('friend'=>$item,'confirmed'=>1), null, null, null, 'all');
                foreach ($friendships as $friendship) {
                    if ($friendship->getFeUser1()->getUid()==$item->getUid()) {
                        $friend = $friendship->getFeUser2();
                    } else {
                        $friend = $friendship->getFeUser1();
                    }
                    if (is_object($friend)) {
                        $friends[] = $friend;
                    }
                }
            }
            
            // get groups if set in plugin options
            if ($this->settings['detailUserLoadGroups']) {
                $memberships = $this->membershipRepository->findByFilter(array('feUser'=>$item,'confirmed'=>1), null, null, null, 'all');
                foreach ($memberships as $membership) {
                    $group = $membership->getFeGroup();
                    if (is_object($group)) {
                        $groups[] = $group;
                    }
                }
            }
        } else {
            
            // add message
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH.'pi4.action_detail_user', $this->extensionName),
                'text' => LocalizationUtility::translate(self::LLPATH.'pi4.action_detail_user.error.no_access', $this->extensionName),
                'type' => FlashMessage::ERROR
            );
            
            // set flash messages
            $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
            
            // redirect to error view
            $this->redirect('error');
        }
        
        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
        
        // set template variables
        $this->view->assign('item', $item);
        $this->view->assign('fields', $fields);
        $this->view->assign('privateFields', $privateFields);
        $this->view->assign('streams', $streams);
        $this->view->assign('friends', $friends);
        $this->view->assign('groups', $groups);
        $this->view->assign('returnUrl', $returnUrl);
        $this->view->assign('feGroup', $feGroup);
        $this->view->assign('isLoggedIn', $isLoggedIn);
        $this->view->assign('isFriend', $isFriend);
        $this->view->assign('isMyself', $isMyself);
        $this->view->assign('isUnconfirmedFriend', $isUnconfirmedFriend);
        $this->view->assign('action', 'detailUser');
    }
    
    /**
     * action add membership
     *
     * @param \int $item
     * @param \string $returnUrl
     * @return void
     */
    public function addMembershipAction($item = null, $returnUrl = null)
    {
        
        // init action arrays and booleans
        $messages = array();
        $createable = false;
        $editable = false;
        
        // check and get logged in user info
        if ($item>0) {
            $item = $this->frontendUserGroupRepository->findByUid($item);
            if (is_object($item)) {
                $readable = true;
                if (true === $this->accessControllService->hasLoggedInFrontendUser()) {
                    $feUser = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());
                    if ($feUser) {
                        $membership = $this->membershipRepository->findByFeUserAndFeGroup($feUser, $item);
                        if (!$membership) {
                            $createable = true;
                        }
                    }
                    if ($this->accessControllService->isGroupEditAllowed($feUser, $item)) {
                        $editable = true;
                    }
                }
            }
        }
        
        // if membership createable
        if ($createable) {
            $membership = $this->objectManager->get('DCNGmbH\\MooxCommunity\\Domain\\Model\\Membership');
            $membership->setTitle($feUser->getUsername().' joined '.$item->getTitle());
            $membership->setFeUser($feUser);
            $membership->setFeGroup($item);
            if ($editable) {
                $membership->setConfirmed(time());
            }
            $this->membershipRepository->add($membership);
            $this->persistenceManager->persistAll();
            
            // if notifications should be sent
            if (!$editable && $this->settings['notificationsEmailTemplate']) {
                
                // load mail template
                $template = $this->templateRepository->findByUid($this->settings['notificationsEmailTemplate']);
                
                // if mail template exists
                if ($template) {
                    
                    // set member mail field
                    $member = array();
                    foreach (array('username','first_name','last_name','auto_name','email','gender') as $fieldname) {
                        $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                        if (method_exists($feUser, $getMethod)) {
                            $member[$fieldname] = $feUser->$getMethod();
                        }
                    }
                    
                    // set group mail field
                    $group = array();
                    foreach (array('title') as $fieldname) {
                        $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                        if (method_exists($item, $getMethod)) {
                            $group[$fieldname] = $item->$getMethod();
                        }
                    }
                    
                    if ($item->getCommunityAdmins()->count()>0) {
                        foreach ($item->getCommunityAdmins() as $communityAdmin) {
                            if ($communityAdmin->getEmail()!='') {
                        
                                // set info mail array
                                $mail = array(
                                    'sender_name' => $this->settings['notificationsSenderName'],
                                    'sender_address' => $this->settings['notificationsSenderAddress'],
                                    'subject' => $this->helperService->prepareMailSubject($template->getSubject(), $member),
                                    'pid' => $GLOBALS['TSFE']->id,
                                    'member' => $member,
                                    'group' => $group,
                                    'notificationPartial' => 'MembershipRequested'
                                );
                                $mail['receiver_name'] = $communityAdmin->getAutoName();
                                $mail['receiver_address'] = $communityAdmin->getEmail();
                                if ($this->settings['listAdminGroupsPid']>0) {
                                    $mail['url'] = $this->uriBuilder->reset()->setNoCache(true)->setTargetPageUid($this->settings['listAdminGroupsPid'])->setCreateAbsoluteUri(true)->build();
                                }
                                // set mail body
                                $mail['body'] = $this->helperService->prepareMailBody($template, $mail);
                                
                                // send mail
                                $this->helperService->sendMail($mail);
                            }
                        }
                    }
                }
            }
            
            // add message
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH.'pi4.action_add_membership', $this->extensionName),
                'text' => LocalizationUtility::translate(self::LLPATH.'pi4.action_add_membership.success', $this->extensionName),
                'type' => FlashMessage::OK
            );
        }
        
        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
        
        // redirect to detail view
        $this->redirect('detailGroup', null, null, array('item'=>$item,'returnUrl'=>$returnUrl));
        exit();
    }
    
    /**
     * action add friendship
     *
     * @param \int $item
     * @param \int $feGroup
     * @param \string $returnUrl
     * @return void
     */
    public function addFriendshipAction($item = null, $feGroup = null, $returnUrl = null)
    {
        
        // init action arrays and booleans
        $messages = array();
        $createable = false;
        $self = false;
        
        // check and get logged in user info
        if ($item>0) {
            $item = $this->frontendUserRepository->findByUid($item);
            if (is_object($item)) {
                $readable = true;
                if (true === $this->accessControllService->hasLoggedInFrontendUser()) {
                    $feUser = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());
                    if ($feUser) {
                        $friendship = $this->friendshipRepository->findByFeUserAndFriend($feUser, $item);
                        if (!$friendship) {
                            $createable = true;
                        }
                        if ($feUser->getUid()==$item->getUid()) {
                            $self = true;
                        }
                    }
                }
            }
        }
        
        // if friendship createable
        if ($createable && !$self) {
            $friendship = $this->objectManager->get('DCNGmbH\\MooxCommunity\\Domain\\Model\\Friendship');
            $friendship->setTitle($feUser->getAutoName().' is friend with '.$item->getAutoName());
            $friendship->setFeUser1($item);
            $friendship->setFeUser2($feUser);
            
            $this->friendshipRepository->add($friendship);
            $this->persistenceManager->persistAll();
            
            // if notifications should be sent
            if ($item->getEmail()!='' && $this->settings['notificationsEmailTemplate']) {
                
                // load mail template
                $template = $this->templateRepository->findByUid($this->settings['notificationsEmailTemplate']);
                
                // if mail template exists
                if ($template) {
                    
                    // set friend mail field
                    $friend = array();
                    foreach (array('username','first_name','last_name','auto_name','email','gender') as $fieldname) {
                        $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                        if (method_exists($feUser, $getMethod)) {
                            $friend[$fieldname] = $feUser->$getMethod();
                        }
                    }
                    
                    // set user mail field
                    $user = array();
                    foreach (array('username','first_name','last_name','auto_name','email','gender') as $fieldname) {
                        $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                        if (method_exists($item, $getMethod)) {
                            $user[$fieldname] = $item->$getMethod();
                        }
                    }
                    
                    // set info mail array
                    $mail = array(
                        'sender_name' => $this->settings['notificationsSenderName'],
                        'sender_address' => $this->settings['notificationsSenderAddress'],
                        'receiver_name' => $item->getAutoName(),
                        'receiver_address' => $item->getEmail(),
                        'subject' => $this->helperService->prepareMailSubject($template->getSubject(), $member),
                        'pid' => $GLOBALS['TSFE']->id,
                        'friend' => $friend,
                        'user' => $user,
                        'notificationPartial' => 'FriendshipRequested'
                    );
                    if ($this->settings['listFriendshipsPid']>0) {
                        $mail['url'] = $this->uriBuilder->reset()->setNoCache(true)->setTargetPageUid($this->settings['listFriendshipsPid'])->setCreateAbsoluteUri(true)->build();
                    }
                    // set mail body
                    $mail['body'] = $this->helperService->prepareMailBody($template, $mail);
                    
                    // send mail
                    $this->helperService->sendMail($mail);
                }
            }
            
            // add message
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH.'pi4.action_add_friendship', $this->extensionName),
                'text' => LocalizationUtility::translate(self::LLPATH.'pi4.action_add_friendship.success', $this->extensionName),
                'type' => FlashMessage::OK
            );
        }
        
        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
        
        // redirect to detail view
        $this->redirect('detailUser', null, null, array('item'=>$item,'feGroup'=>$feGroup,'returnUrl'=>$returnUrl));
        exit();
    }
    
    /**
     * action remove membership
     *
     * @param \int $item
     * @param \string $returnUrl
     * @return void
     */
    public function removeMembershipAction($item = null, $returnUrl = null)
    {
        
        // init action arrays and booleans
        $messages = array();
        $removeable = false;
        
        // check and get logged in user info
        if ($item>0) {
            $item = $this->frontendUserGroupRepository->findByUid($item);
            if (is_object($item)) {
                $readable = true;
                if (true === $this->accessControllService->hasLoggedInFrontendUser()) {
                    $feUser = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());
                    if ($feUser) {
                        $membership = $this->membershipRepository->findByFeUserAndFeGroup($feUser, $item);
                        if ($membership) {
                            $removeable = true;
                        }
                    }
                }
            }
        }
        
        // if membership removeable
        if ($removeable) {
            $this->membershipRepository->remove($membership);
            $this->persistenceManager->persistAll();
            
            // add message
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH.'pi4.action_remove_membership', $this->extensionName),
                'text' => LocalizationUtility::translate(self::LLPATH.'pi4.action_remove_membership.success', $this->extensionName),
                'type' => FlashMessage::OK
            );
        }
        
        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
        
        // redirect to detail view
        $this->redirect('detailGroup', null, null, array('item'=>$item,'returnUrl'=>$returnUrl));
        exit();
    }

    /**
     * action remove friendship
     *
     * @param \int $item
     * @param \int $feGroup
     * @param \string $returnUrl
     * @return void
     */
    public function removeFriendshipAction($item = null, $feGroup = null, $returnUrl = null)
    {
        
        // init action arrays and booleans
        $messages = array();
        $removeable = false;
        
        // check and get logged in user info
        if ($item>0) {
            $item = $this->frontendUserRepository->findByUid($item);
            if (is_object($item)) {
                $readable = true;
                if (true === $this->accessControllService->hasLoggedInFrontendUser()) {
                    $feUser = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());
                    $friendship = $this->friendshipRepository->findByFeUserAndFriend($feUser, $item);
                    if ($friendship) {
                        $removeable = true;
                    }
                }
            }
        }
        
        // if membership removeable
        if ($removeable) {
            $this->friendshipRepository->remove($friendship);
            $this->persistenceManager->persistAll();
            
            // add message
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH.'pi4.action_remove_friendship', $this->extensionName),
                'text' => LocalizationUtility::translate(self::LLPATH.'pi4.action_remove_friendship.success', $this->extensionName),
                'type' => FlashMessage::OK
            );
        }
        
        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
        
        // redirect to list view
        if ($returnUrl!='') {
            $this->redirectToUri($returnUrl);
        } else {
            $this->redirect('detailUser', null, null, array('item'=>$item,'feGroup'=>$feGroup,'returnUrl'=>$returnUrl));
        }
        exit();
    }
    
    /**
     * action error
     *
     * @return void
     */
    public function errorAction()
    {
        $this->view->assign('action', 'error');
    }
    
    /**
     * Returns storage pids
     *
     * @return \array
     */
    public function getStoragePids()
    {
        return $this->storagePids;
    }

    /**
     * Set storage pids
     *
     * @param \array $storagePids storage pids
     * @return void
     */
    public function setStoragePids($storagePids)
    {
        $this->storagePids = $storagePids;
    }
    
    /**
     * Returns fields groups
     *
     * @return \array
     */
    public function getFieldsGroups()
    {
        return $this->fieldsGroups;
    }

    /**
     * Set fields groups
     *
     * @param \array $fields
     * @return void
     */
    public function setFieldsGroups($fieldsGroups)
    {
        $this->fieldsGroups = $fieldsGroups;
    }
    
    /**
     * Returns fields users
     *
     * @return \array
     */
    public function getFieldsUsers()
    {
        return $this->fieldsUsers;
    }

    /**
     * Set fields
     *
     * @param \array $fieldsUsers
     * @return void
     */
    public function setFieldsUsers($fieldsUsers)
    {
        $this->fieldsUsers = $fieldsUsers;
    }
    
    /**
     * Returns fieldConfig
     *
     * @return \array
     */
    public function getFieldConfig()
    {
        return $this->fieldConfig;
    }

    /**
     * Set fieldConfig
     *
     * @param \array $fieldConfig fieldConfig
     * @return void
     */
    public function setFieldConfig($fieldConfig)
    {
        $this->fieldConfig = $fieldConfig;
    }
    
    /**
     * Returns ext conf
     *
     * @return \array
     */
    public function getExtConf()
    {
        return $this->extConf;
    }

    /**
     * Set ext conf
     *
     * @param \array $extConf ext conf
     * @return void
     */
    public function setExtConf($extConf)
    {
        $this->extConf = $extConf;
    }
}
