<?php
namespace DCNGmbH\MooxCommunity\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
 
use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use \TYPO3\CMS\Core\Messaging\FlashMessage;
use \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

/**
 *
 *
 * @package moox_community
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Pi3Controller extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * accessControllService
     *
     * @var \DCNGmbH\MooxCommunity\Service\AccessControlService
     * @inject
     */
    protected $accessControllService;
    
    /**
     * helperService
     *
     * @var \DCNGmbH\MooxCommunity\Service\HelperService
     * @inject
     */
    protected $helperService;
    
    /**
     * extConf
     *
     * @var boolean
     */
    protected $extConf;
    
    /**
     * storagePids
     *
     * @var array
     */
    protected $storagePids;
    
    /**
     * Path to the locallang file
     * @var string
     */
    const LLPATH = 'LLL:EXT:moox_community/Resources/Private/Language/locallang.xlf:';
    
    /**
     * initialize action
     *
     * @return void
     */
    public function initializeAction()
    {
        
        // execute parent initialize action
        parent::initializeAction();
        
        // load extension configuration
        $this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_community']);
        
        // initalize storage settings
        $this->initializeStorageSettings();
    }

    /**
     * initialize storage settings
     *
     * @return void
     */
    protected function initializeStorageSettings()
    {
            
        // get typoscript configuration
        $configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
        
        // set storage pid if set by plugin
        if ($this->settings['storagePids']!='') {
            $this->setStoragePids(explode(',', $this->settings['storagePids']));
        } else {
            $this->setStoragePids(array());
        }
        
        // overwrite extbase persistence storage pid
        if (!empty($this->settings['storagePid']) && $this->settings['storagePid']!='TS') {
            if (empty($configuration['persistence']['storagePid'])) {
                $storagePids['persistence']['storagePid'] = $this->settings['storagePid'];
                $this->configurationManager->setConfiguration(array_merge($configuration, $storagePids));
            } else {
                $configuration['persistence']['storagePid'] = $this->settings['storagePid'];
                $this->configurationManager->setConfiguration($configuration);
            }
        }
    }
    
    /**
     * action logoff
     *
     * @param \boolean $logoff
     * @param \string $redirect
     * @return void
     */
    public function logoffAction($logoff = null, $redirect = '')
    {
        
        // set redirect uri
        if ($redirect=='') {
            
            // if redirection pid is set by plugin
            if ($this->settings['logoffRedirectPid']>0) {
                $redirect = $this->uriBuilder->reset()->setNoCache(true)->setTargetPageUid($this->settings['logoffRedirectPid'])->build();
            } else {
                $redirect = $GLOBALS['TSFE']->baseUrl.$GLOBALS['TSFE']->anchorPrefix;
            }
        }
        
        // check if user is logged in and get user item
        $item = null;
        if ($this->accessControllService->hasLoggedInFrontendUser()) {
            $item = $GLOBALS['TSFE']->fe_user->user;
        }
        
        // if form submitted
        if ($logoff || $this->settings['directLogoff']) {
            
            // logoff user
            $this->accessControllService->logoff();
            
            // if redirect uri is set
            if ($redirect!='') {
                    
                // redirect to uri
                $this->redirectToUri($redirect);
            }
        }
        
        // set template variables
        $this->view->assign('redirect', $redirect);
        $this->view->assign('item', $item);
        $this->view->assign('action', 'logoff');
    }
    
    /**
     * action error
     *
     * @return void
     */
    public function errorAction()
    {
        $this->view->assign('action', 'error');
    }
    
    /**
     * Returns storage pids
     *
     * @return array
     */
    public function getStoragePids()
    {
        return $this->storagePids;
    }

    /**
     * Set storage pids
     *
     * @param array $storagePids storage pids
     * @return void
     */
    public function setStoragePids($storagePids)
    {
        $this->storagePids = $storagePids;
    }
    
    /**
     * Returns ext conf
     *
     * @return array
     */
    public function getExtConf()
    {
        return $this->extConf;
    }

    /**
     * Set ext conf
     *
     * @param array $extConf ext conf
     * @return void
     */
    public function setExtConf($extConf)
    {
        $this->extConf = $extConf;
    }
}
