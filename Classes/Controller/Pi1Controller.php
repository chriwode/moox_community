<?php
namespace DCNGmbH\MooxCommunity\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use DCNGmbH\MooxCommunity\Domain\Model\AboMembership;
use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use \TYPO3\CMS\Core\Messaging\FlashMessage;
use \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use \TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 *
 *
 * @package moox_community
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Pi1Controller extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * frontendUserRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository;

    /**
     * frontendUserGroupRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\FrontendUserGroupRepository
     * @inject
     */
    protected $frontendUserGroupRepository;

    /**
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\AboMembershipRepository
     * @inject
     */
    protected $aboMembershipRepository;

    /**
     * storageRepository
     *
     * @var \TYPO3\CMS\Core\Resource\StorageRepository
     * @inject
     */
    protected $storageRepository;
    
    /**
     * fileRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\FileRepository
     * @inject
     */
    protected $fileRepository;
        
    /**
     * templateRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\TemplateRepository
     * @inject
     */
    protected $templateRepository;
    
    /**
     * accessControllService
     *
     * @var \DCNGmbH\MooxCommunity\Service\AccessControlService
     * @inject
     */
    protected $accessControllService;
    
    /**
     * helperService
     *
     * @var \DCNGmbH\MooxCommunity\Service\HelperService
     * @inject
     */
    protected $helperService;
    
    /**
     * persistenceManager
     *
     * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
     * @inject
     */
    protected $persistenceManager;
    
    /**
     * extConf
     *
     * @var boolean
     */
    protected $extConf;
    
    /**
     * storagePids
     *
     * @var array
     */
    protected $storagePids;
    
    /**
     * fileStorageUid
     *
     * @var int
     */
    protected $fileStorageUid;
    
    /**
     * fileStorageFolder
     *
     * @var int
     */
    protected $fileStorageFolder;
    
    /**
     * fields
     *
     * @var array
     */
    protected $fields;
    
    /**
     * fieldConfig
     *
     * @var array
     */
    protected $fieldConfig;
        
    /**
     * Path to the locallang file
     * @var string
     */
    const LLPATH = 'LLL:EXT:moox_community/Resources/Private/Language/locallang.xlf:';
    
    /**
     * fe user variant
     * @var string
     */
    const VARIANT = 'moox_community';
    
    /**
     * temp folder
     * @var string
     */
    const TEMPFOLDER = 'moox_community';
        
    /**
     * initialize action
     *
     * @return void
     */
    public function initializeAction()
    {
        
        // execute parent initialize action
        parent::initializeAction();
        
        // load extension configuration
        $this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_community']);
        
        // set username minlength fallback
        if ($this->settings['usernameMinlength']<3) {
            $this->settings['usernameMinlength'] = 3;
        }
        
        // set password minlength fallback
        if ($this->settings['passwordMinlength']<8) {
            $this->settings['passwordMinlength'] = 8;
        }
        
        // set file storage uid
        if ($this->settings['fileStorage']!='') {
            $this->fileStorageUid = $this->settings['fileStorage'];
        } else {
            $this->fileStorageUid = 1;
        }
        
        // set file storage folder
        if ($this->settings['fileStorageFolder']!='') {
            $this->fileStorageFolder = $this->settings['fileStorageFolder'];
        } else {
            $this->fileStorageFolder = 'moox_community';
        }
        
        // set plugin fields
        $this->fields = $this->helperService->getPluginFields('fe_users', 'mooxcommunity', 'all');
        
        // initalize storage settings
        $this->initializeStorageSettings();
        
        // set field config
        $this->fieldConfig = $this->helperService->getFieldConfig($this->fields, $this->settings);
    }

    /**
     * initialize storage settings
     *
     * @return void
     */
    protected function initializeStorageSettings()
    {
            
        // get typoscript configuration
        $configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
        
        // set storage pid if set by plugin
        if ($this->settings['storagePids']!='') {
            $this->setStoragePids(explode(',', $this->settings['storagePids']));
        } else {
            $this->setStoragePids([]);
        }
        $this->helperService->setStoragePids($this->getStoragePids());
        
        // overwrite extbase persistence storage pid
        if (!empty($this->settings['storagePid']) && $this->settings['storagePid']!='TS') {
            if (empty($configuration['persistence']['storagePid'])) {
                $storagePids['persistence']['storagePid'] = $this->settings['storagePid'];
                $this->configurationManager->setConfiguration(array_merge($configuration, $storagePids));
            } else {
                $configuration['persistence']['storagePid'] = $this->settings['storagePid'];
                $this->configurationManager->setConfiguration($configuration);
            }
        }
    }
        
    /**
     * action profile
     *
     * @param \array $item
     * @return void
     */
    public function profileAction($profile = null)
    {
        
        // init action arrays and boolean
        $messages = [];
        $errors = [];
        $fields = [];
        $emailHasChanged = false;
        $profileDoubleOptIn = false;
        $initProfile = true;
        $fileAddOnly = false;
        $fileRemoveOnly = false;
        
        // get logged in user
        $item = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());
       
        // check if access is allowrd
        if ($item && $this->accessControllService->isAccessAllowed($item)) {
            // get fields set by plugin
            $fields = $this->helperService->configureFields(
                $this->settings['profileFields'],
                $this->settings['requiredProfileFields'],
                $this->settings,
                $this->fieldConfig
            );
            
            // process form values
            if ($profile) {
                // supress profile initialization
                $initProfile = false;
                
                // add or remove files by add button
                foreach ($fields as $field) {
                    if ($field['config']['type']=='file') {
                        if (is_array($profile[$field['key'].'_tmp'])) {
                            foreach ($profile[$field['key'].'_tmp'] as $key => $value) {
                                if (isset($profile[$field['key'].'_remove_'.$key])) {
                                    $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($field['key']);
                                    $removeMethod = 'remove'.GeneralUtility::underscoredToUpperCamelCase($field['key']);
                                    
                                    if (method_exists($item, $getMethod) && method_exists($item, $removeMethod)) {
                                        foreach ($item->$getMethod() as $reference) {
                                            if ($reference->getUid()==$key) {
                                                $item->$removeMethod($reference);
                                                $this->frontendUserRepository->update($item);
                                                $this->persistenceManager->persistAll();
                                            }
                                        }
                                    }
                                    
                                    // add message
                                    $messages[] = [
                                        'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                                        'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_profile', $this->extensionName),
                                        'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_profile.removed.'.$field['key'], $this->extensionName),
                                        'type' => FlashMessage::OK
                                    ];

                                    $fileRemoveOnly = true;
                                }
                            }
                        }
                        
                        if (isset($profile[$field['key']]) && isset($profile[$field['key'].'_add'])) {
                            if ($profile[$field['key']]['name']!='') {
                                $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($field['key']);
                                $addMethod = 'add'.GeneralUtility::underscoredToUpperCamelCase($field['key']);
                                
                                if (method_exists($item, $getMethod) && method_exists($item, $addMethod)) {
                                    $fileCnt = $item->$getMethod()->count();
                                    
                                    if (!isset($field['config']['maxitems']) || (isset($field['config']['maxitems']) && $fileCnt<$field['config']['maxitems'])) {
                                        $storage = $this->storageRepository->findByUid($this->fileStorageUid);
                                        $folder = $storage->createFolder($this->fileStorageFolder);
                                        $fileObject = $storage->addFile(
                                            $profile[$field['key']]['tmp_name'], $folder, $profile[$field['key']]['name']
                                        );
                                        
                                        $fileObject = $storage->getFile($fileObject->getIdentifier());
                                        $file = $this->fileRepository->findByUid($fileObject->getProperty('uid'));
                                            
                                        $fileReference = $this->objectManager->get('DCNGmbH\MooxCommunity\Domain\Model\UserImageReference');
                                        $fileReference->setFile($file);
                                        $fileReference->setCruserId(0);
                                        
                                        $item->$addMethod($fileReference);
                                        
                                        $this->frontendUserRepository->update($item);
                                        $this->persistenceManager->persistAll();
                                        $item = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());
                                                                        
                                        // add message
                                        $messages[] = [
                                            'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                                            'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_profile', $this->extensionName),
                                            'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_profile.added.'.$field['key'], $this->extensionName),
                                            'type' => FlashMessage::OK
                                        ];
                                    } else {
                                        // add message
                                        $messages[] = [
                                            'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                                            'title' => LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'], $this->extensionName),
                                            'text' => LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.too_many', $this->extensionName, [$field['config']['maxitems']]),
                                            'type' => FlashMessage::ERROR
                                        ];

                                        // set error
                                        $errors[$field['key']] = true;
                                    }
                                }
                            } else {
                                // add message
                                $messages[] = [
                                    'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                                    'title' => LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'], $this->extensionName),
                                    'text' => LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.empty', $this->extensionName, [$field['config']['maxitems']]),
                                    'type' => FlashMessage::ERROR
                                ];

                                // set error
                                $errors[$field['key']] = true;
                            }
                            
                            $fileAddOnly = true;
                        }
                    }
                }
                
                // if general save button is pressed
                if (!$fileAddOnly && !$fileRemoveOnly) {
                    // set email es username if set in settings
                    if ($this->settings['saveEmailAsUsername']) {
                        $profile['username'] = $profile['email'];
                        if ($item->getEmail()==$profile['email']) {
                            unset($fields['email']['config']['unique']);
                            unset($fields['email']['config']['data']['data-unique']);
                        }
                    }
                    
                    // check if email address has changed
                    if ($item->getEmail()!=$profile['email']) {
                        $emailHasChanged = true;
                    }
                    
                    // check left fields
                    $this->helperService->checkFields($fields, $profile, $messages, $errors);
                                    
                    // set item values
                    foreach ($fields as $field) {
                        if (!in_array($field['config']['type'], ['file']) && !in_array($field['key'], ['usergroup', 'requested_usergroup']) && ($field['key']!='password' || $profile['password']!='') && ($field['key']!='email' || !$emailHasChanged || !$this->settings['profileDoubleOptIn'])) {
                            $setMethod = 'set'.GeneralUtility::underscoredToUpperCamelCase($field['key']);
                            if (method_exists($item, $setMethod)) {
                                $item->$setMethod(trim($profile[$field['key']]));
                            }
                        }
                    }

                    // no errors -> save changes
                    if (!count($errors)) {
                                                                
                        // set item name
                        $item->setName($this->helperService->generateName($item));
                        
                        // set file relations
                        foreach ($fields as $field) {
                            if (in_array($field['config']['type'], ['file'])) {
                                if (isset($profile[$field['key']])) {
                                    if ($profile[$field['key']]['name']!='') {
                                        $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($field['key']);
                                        $addMethod = 'add'.GeneralUtility::underscoredToUpperCamelCase($field['key']);
                                        
                                        if (method_exists($item, $getMethod) && method_exists($item, $addMethod)) {
                                            $storage = $this->storageRepository->findByUid($this->fileStorageUid);
                                            $folder = $storage->createFolder($this->fileStorageFolder);
                                            $fileObject = $storage->addFile(
                                                $profile[$field['key']]['tmp_name'], $folder, $profile[$field['key']]['name']
                                            );
                                                
                                            $fileObject = $storage->getFile($fileObject->getIdentifier());
                                            $file = $this->fileRepository->findByUid($fileObject->getProperty('uid'));
                                                    
                                            $fileReference = $this->objectManager->get('DCNGmbH\MooxCommunity\Domain\Model\UserImageReference');
                                            $fileReference->setFile($file);
                                            $fileReference->setCruserId(0);
                                                
                                            $item->$addMethod($fileReference);
                                        }
                                    }
                                }
                            }
                        }
                        
                        // if email address has changed
                        if ($emailHasChanged) {
                        
                            // save email as username if set by plugin
                            if ($this->settings['saveEmailAsUsername'] && !$this->settings['profileDoubleOptIn']) {
                                $item->setUsername($item->getEmail());
                            }
                            
                            // set double opt in confirmation fields
                            if ($this->settings['profileDoubleOptIn']) {
                                $profileValues = [];
                                $profileValues['email'] = $profile['email'];
                                if ($this->settings['saveEmailAsUsername']) {
                                    $profileValues['username'] = $profile['email'];
                                }
                                $item->setProfileValues(serialize($profileValues));
                                $item->setProfileHash(md5($item->getEmail().time().$GLOBALS['TSFE']->id));
                                $item->setProfileTstamp(time());
                                $profileDoubleOptIn = true;
                            }
                        }
                        
                        // save item to database
                        $this->frontendUserRepository->update($item);
                        $this->persistenceManager->persistAll();
                        
                        // if double opt in is needed
                        if ($profileDoubleOptIn) {
                            
                            // load mail template
                            $template = $this->templateRepository->findByUid($this->settings['profileEmailTemplate']);
                            
                            // if mail template exists
                            if ($template) {
                                
                                // set user mail field
                                $user = [];
                                foreach ($this->fields as $fieldname => $field) {
                                    $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                                    if (method_exists($item, $getMethod)) {
                                        $user[$fieldname] = $item->$getMethod();
                                    }
                                }
                                
                                // get root page uid
                                $GLOBALS['TSFE']->getPageAndRootline();
                                $storageSiterootPids = $GLOBALS['TSFE']->getStorageSiterootPids();
                                if (is_array($storageSiterootPids) && isset($storageSiterootPids['_SITEROOT'])) {
                                    $rootPageUid = $storageSiterootPids['_SITEROOT'];
                                }
                                
                                // if root page to call plugin action has found
                                if ($rootPageUid>0) {
                                    
                                    // set info mail array
                                    $mail = [
                                        'sender_name' => $this->settings['profileSenderName'],
                                        'sender_address' => $this->settings['profileSenderAddress'],
                                        'receiver_name' => ($item->getFirstName()!='')?$item->getFirstName().' '.$item->getLastName():$item->getLastName(),
                                        'receiver_address' => $item->getEmail(),
                                        'subject' => $this->helperService->prepareMailSubject($template->getSubject(), $user),
                                        'pid' => $GLOBALS['TSFE']->id,
                                        'hash' => $item->getProfileHash(),
                                        'url' => $this->uriBuilder->reset()->setNoCache(true)->setTargetPageType(999001)->setTargetPageUid($rootPageUid)->setCreateAbsoluteUri(true)->uriFor('confirmProfile', ['email' => $user['email'], 'hash' => $item->getProfileHash(), 'pids' => $this->storagePids, 'redirect' => $this->settings['profileRedirectPid']], 'Pi1', 'MooxCommunity', 'Pi1'),
                                        'user' => $user
                                    ];
                                    // set mail body
                                    $mail['body'] = $this->helperService->prepareMailBody($template, $mail);
                                    
                                    // send mail
                                    $this->helperService->sendMail($mail);
                                    
                                    // add message
                                    $messages[] = [
                                        'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                                        'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_profile', $this->extensionName),
                                        'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_profile.success.opt_in', $this->extensionName),
                                        'type' => FlashMessage::OK
                                    ];
                                }
                            } else {
                                
                                // add message
                                $messages[] = [
                                    'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                                    'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_register', $this->extensionName),
                                    'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_register.error.no_template', $this->extensionName),
                                    'type' => FlashMessage::ERROR
                                ];
                            }
                        } else {
                        
                            // add message
                            $messages[] = [
                                'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                                'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_profile', $this->extensionName),
                                'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_profile.success', $this->extensionName),
                                'type' => FlashMessage::OK
                            ];
                        }

                        unset($profile['password']);
                        unset($profile['password_repeat']);
                    }
                }
            }

            if ($initProfile) {
                
                // set profile user
                $profile = [];
                foreach ($fields as $field) {
                    if (!in_array($field['config']['type'], ['file'])) {
                        $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($field['key']);
                        if (method_exists($item, $getMethod)) {
                            $profile[$field['key']] = $item->$getMethod();
                        }
                    }
                }
                unset($profile['password']);
            }
                
            // set profile files
            foreach ($fields as $field) {
                if (in_array($field['config']['type'], ['file'])) {
                    unset($profile[$field['key'].'_tmp']);
                    $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($field['key']);
                    if (method_exists($item, $getMethod)) {
                        foreach ($item->$getMethod() as $fileReference) {
                            $fileReference = $fileReference->getOriginalResource();
                            $file = $fileReference->getOriginalFile();
                            $fileCnt = (is_array($profile[$field['key'].'_tmp']))?count($profile[$field['key'].'_tmp']):0;
                            $profile[$field['key'].'_tmp'][$fileCnt]['name'] = $file->getName();
                            $profile[$field['key'].'_tmp'][$fileCnt]['src'] = $file->getPublicUrl();
                            $profile[$field['key'].'_tmp'][$fileCnt]['ext'] = $fileReference->getExtension();
                            $profile[$field['key'].'_tmp'][$fileCnt]['size'] = $fileReference->getSize();
                            $profile[$field['key'].'_tmp'][$fileCnt]['reference'] = $fileReference;
                        }
                    }
                }
            }
        } else {
            
            // add message
            $messages[] = [
                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_profile', $this->extensionName),
                'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_profile.error.no_access', $this->extensionName),
                'type' => FlashMessage::ERROR
            ];
        }
        
        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
        
        // set template variables
        $this->view->assign('profile', $profile);
        $this->view->assign('fields', $fields);
        $this->view->assign('errors', $errors);
        $this->view->assign('action', 'profile');
    }
    
    /**
     * action confirm profile
     *
     * @param \string $email
     * @param \string $hash
     * @param \array $pids
     * @param \int $redirect
     * @return void
     */
    public function confirmProfileAction($email = '', $hash = '', $pids = null, $redirect = 0)
    {
        
        // overwrite pids by url
        if ($pids) {
            if (!is_array($pids)) {
                $this->storagePids = [$pids];
            } else {
                $this->storagePids = $pids;
            }
        }
        
        // if confirmation fields are set
        if ($email!='' && $hash!='') {
            
            // find valid user to confirm registration
            $items = $this->frontendUserRepository->findByEmailAndProfileHash($this->storagePids, $email, $hash);
            
            // check if found user is unique and hash is still usable
            if ($items->count()==1) {
                
                // set user to confirm registration
                $item = $items->getFirst();
                
                // set confirmed values
                $values = $item->getProfileValues();
                $values = unserialize($values);
                if (is_array($values)) {
                    // set item values
                    foreach ($values as $key => $value) {
                        $setMethod = 'set'.GeneralUtility::underscoredToUpperCamelCase($key);
                        if (method_exists($item, $setMethod)) {
                            $item->$setMethod($value);
                        }
                    }
                }
                $item->setProfileValues('');
                $item->setProfileTstamp(0);
                $item->setProfileHash('');
                
                // save changes to database
                $this->frontendUserRepository->update($item);
                $this->persistenceManager->persistAll();
                
                // add message
                $messages[] = [
                    'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                    'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_profile', $this->extensionName),
                    'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_confirm_profile.success', $this->extensionName),
                    'type' => FlashMessage::OK
                ];
            } elseif ($items->count()>0) {
                
                // add message
                $messages[] = [
                    'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                    'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_profile', $this->extensionName),
                    'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_confirm_profile.error.not_unique', $this->extensionName),
                    'type' => FlashMessage::ERROR
                ];
            } else {
                
                // add message
                $messages[] = [
                    'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                    'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_profile', $this->extensionName),
                    'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_confirm_profile.error.already_confirmed', $this->extensionName),
                    'type' => FlashMessage::ERROR
                ];
            }
            
            // set flash messages
            $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
            
            // if profile redirect pid is set by plugin or url
            if (!$this->settings['profileRedirectPid'] && $redirect>0) {
                $this->settings['profileRedirectPid'] = $redirect;
            }
            if ($this->settings['profileRedirectPid']>0) {
                
                // redirect to uri
                $this->redirectToUri($this->uriBuilder->reset()->setNoCache(true)->setTargetPageUid($this->settings['profileRedirectPid'])->setLinkAccessRestrictedPages(true)->build());
            
            // redirect to profile action
            } else {
                $this->redirect('profile');
            }
        } else {
            
            // redirect to error form
            $this->redirect('error');
        }
        
        // set template variables
        $this->view->assign('action', 'confirmProfile');
    }
    
    /**
     * action password recovery
     *
     * @param array $recovery
     * @return void
     */
    public function passwordRecoveryAction($recovery = null)
    {
        
        // init action arrays
        $errors = [];
        $messages = [];
        
        // load mail template
        $template = $this->templateRepository->findByUid($this->settings['recoveryEmailTemplate']);
        
        // if mail template exists
        if ($template) {
            
            // if form submitted
            if ($recovery) {
                
                // check recovery fields
                if (trim($recovery['username'])=='' && trim($recovery['email'])=='') {
                    
                    // add message
                    $messages[] = [
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_password_recovery', $this->extensionName),
                        'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_password_recovery.error.no_input', $this->extensionName),
                        'type' => FlashMessage::ERROR,
                    ];
                    
                    // set errors
                    $errors['username']    = true;
                    $errors['email'] = true;
                } elseif (trim($recovery['email'])!='' && !GeneralUtility::validEmail(trim($recovery['email']))) {
                    
                    // add message
                    $messages[] = [
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_password_recovery', $this->extensionName),
                        'text' => LocalizationUtility::translate(self::LLPATH.'form.email.error.invalid', $this->extensionName),
                        'type' => FlashMessage::ERROR,
                    ];
                    
                    // set error
                    $errors['email'] = true;
                }
                
                // no errors -> find user in database
                if (!count($errors)) {
                    $notUnique = false;
                    
                    // find user by username and email
                    if (trim($recovery['username'])!='' && trim($recovery['email'])!='') {
                        $feUsers = $this->frontendUserRepository->findByUsernameAndEmail($this->storagePids, trim($recovery['username']), trim($recovery['email']));
                        if ($feUsers->count()==1) {
                            $item = $feUsers->getFirst();
                        } elseif ($feUsers->count()>1) {
                            $notUnique = true;
                        }
                    }
                    
                    // find user by username, if not found in previous step
                    if (trim($recovery['username'])!='' && !$item) {
                        $feUsers = $this->frontendUserRepository->findByUsername($this->storagePids, trim($recovery['username']));
                        if ($feUsers->count()==1) {
                            $item = $feUsers->getFirst();
                        } elseif ($feUsers->count()>1) {
                            $notUnique = true;
                        }
                    }
                    
                    // find user by email, if not found in previous step
                    if (trim($recovery['email'])!='' && !$item) {
                        $feUsers = $this->frontendUserRepository->findByEmail($this->storagePids, trim($recovery['email']));
                        if ($feUsers->count()==1) {
                            $item = $feUsers->getFirst();
                        } elseif ($feUsers->count()>1) {
                            $notUnique = true;
                        }
                    }
                    
                    // if unique user has found
                    if ($item) {
                        
                        // if found user has email address
                        if ($item->getEmail()!='') {
                            
                            // set recovery fields
                            $item->setPasswordRecoveryHash(md5($item->getUid().time()));
                            $item->setPasswordRecoveryTstamp(time());
                            
                            // save recovery field changes to database
                            $this->frontendUserRepository->add($item);
                            $this->persistenceManager->persistAll();
                            
                            // add message
                            $messages[] = [
                                'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                                'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_password_recovery', $this->extensionName),
                                'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_password_recovery.success', $this->extensionName),
                                'type' => FlashMessage::OK,
                            ];
                            
                            // set user mail field
                            $user = [];
                            $user['uid'] = $item->getUid();
                            foreach ($this->fields as $fieldname => $field) {
                                $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                                if (method_exists($item, $getMethod)) {
                                    $user[$fieldname] = $item->$getMethod();
                                }
                            }
                                                        
                            // set info mail array
                            $mail = [
                                'sender_name' => $this->settings['recoverySenderName'],
                                'sender_address' => $this->settings['recoverySenderAddress'],
                                'receiver_name' => $item->getName(),
                                'receiver_address' => $item->getEmail(),
                                'subject' => $this->helperService->prepareMailSubject($template->getSubject(), $user),
                                'pid' => $GLOBALS['TSFE']->id,
                                'hash' => $item->getPasswordRecoveryHash(),
                                'url' => $this->uriBuilder->reset()->setTargetPageUid($GLOBALS['TSFE']->id)->setNoCache(true)->setCreateAbsoluteUri(true)->uriFor('newPassword', ['uid' => $user['uid'], 'hash' => $item->getPasswordRecoveryHash()], 'Pi1', 'MooxCommunity', 'Pi1'),
                                'user' => $user
                            ];
                            
                            // set mail body
                            $mail['body'] = $this->helperService->prepareMailBody($template, $mail);
                            
                            // send mail
                            $this->helperService->sendMail($mail);
                        } else {
                            
                            // add message
                            $messages[] = [
                                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                                'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_password_recovery', $this->extensionName),
                                'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_password_recovery.error.no_email', $this->extensionName),
                                'type' => FlashMessage::ERROR,
                            ];
                        }
                        
                        // unset recovery item
                        $recovery = null;
                    } elseif ($notUnique) {
                        
                        // add message
                        $messages[] = [
                            'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                            'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_password_recovery', $this->extensionName),
                            'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_password_recovery.error.not_unique', $this->extensionName),
                            'type' => FlashMessage::ERROR,
                        ];
                        
                        // set errors
                        $errors['username']    = true;
                        $errors['email'] = true;
                    } else {
                        
                        // add message
                        $messages[] = [
                            'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                            'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_password_recovery', $this->extensionName),
                            'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_password_recovery.error.not_found', $this->extensionName),
                            'type' => FlashMessage::ERROR,
                        ];

                        // set errors
                        $errors['username']    = true;
                        $errors['email'] = true;
                    }
                }
            }
        } else {
            
            // add message
            $messages[] = [
                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_password_recovery', $this->extensionName),
                'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_password_recovery.error.no_template', $this->extensionName),
                'type' => FlashMessage::ERROR,
            ];
        }
        
        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
        
        // set template variables
        $this->view->assign('item', $recovery);
        $this->view->assign('errors', $errors);
        $this->view->assign('action', 'passwordRecovery');
    }
    
    /**
     * action password recovery
     *
     * @param array $newPassword
     * @param integer $uid
     * @param string $hash
     * @return void
     */
    public function newPasswordAction($newPassword = null, $uid = 0, $hash = '')
    {
        
        // init action arrays and boolans
        $messages = [];
        $errors = [];
        $newPasswordAllowed = true;
        
        // find valid user
        $items = $this->frontendUserRepository->findByUidAndHash($this->storagePids, ($newPassword)?$newPassword['uid']:$uid, ($newPassword)?$newPassword['hash']:$hash);
        
        // check if reset of password is allowed
        if (is_object($items) && $items->count()>1) {
            $newPasswordAllowed = false;
        } elseif (!is_object($items)) {
            $newPasswordAllowed = false;
        } else {
            
            // get user to set new passwort
            $item = $items->getFirst();
            
            // if item is valid user object
            if (is_object($item)) {
                
                // if form submitted
                if ($newPassword) {
                    
                    // check password field
                    if (trim($newPassword['password'])=='') {
                        
                        // add message
                        $messages[] = [
                            'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                            'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_new_password', $this->extensionName),
                            'text' => LocalizationUtility::translate(self::LLPATH.'form.password.error.empty', $this->extensionName),
                            'type' => FlashMessage::ERROR
                        ];
                        
                        // set error
                        $errors['password'] = true;
                    } elseif (strlen(trim($newPassword['password']))<$this->settings['passwordMinlength']) {
                    
                        // add message
                        $messages[] = [
                            'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                            'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_new_password', $this->extensionName),
                            'text' => LocalizationUtility::translate(self::LLPATH.'form.password.error.too_short', $this->extensionName, [$this->settings['passwordMinlength']]),
                            'type' => FlashMessage::ERROR
                        ];
                        
                        // set error
                        $errors['password'] = true;
                    } elseif (trim($newPassword['password'])!='' && trim($newPassword['password'])!=trim($newPassword['password_repeat'])) {
                        
                        // add message
                        $messages[] = [
                            'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                            'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_new_password', $this->extensionName),
                            'text' => LocalizationUtility::translate(self::LLPATH.'form.password.error.not_equal', $this->extensionName),
                            'type' => FlashMessage::ERROR
                        ];
                        
                        // set errors
                        $errors['password'] = true;
                        $errors['password_repeat'] = true;
                    }
                    
                    // no errors -> set new password
                    if (!count($errors)) {
                        
                        // set password fields
                        $item->setPassword(trim($newPassword['password']));
                        $item->setPasswordRecoveryHash('');
                        $item->setPasswordRecoveryTstamp(0);
                        
                        // save new password to database
                        $this->frontendUserRepository->update($item);
                        $this->persistenceManager->persistAll();
                                                
                        // add message
                        $messages[] = [
                            'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                            'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_new_password', $this->extensionName),
                            'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_new_password.success', $this->extensionName),
                            'type' => FlashMessage::OK
                        ];
                        
                        // if login pid is set by plugin
                        if ($this->settings['loginPid']>0) {
                            
                            // set flash messages
                            $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
                            
                            // redirect to uri
                            $this->redirectToUri($this->uriBuilder->reset()->setNoCache(true)->setTargetPageUid($this->settings['loginPid'])->build());
                        }
                        
                        // unset new password item
                        unset($newPassword);
                    }
                }
            } else {
                $newPasswordAllowed = false;
            }
        }
        
        // if setting new password is not allowed
        if (!$newPasswordAllowed) {
            
            // add message
            $messages[] = [
                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_new_password', $this->extensionName),
                'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_new_password.error.invalid_url', $this->extensionName),
                'type' => FlashMessage::ERROR
            ];

            // set flash messages
            $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
            
            // redirect to password recovery form
            $this->redirect('passwordRecovery');
        }
        
        // if action opened by recovery link
        if (is_null($newPassword)) {
            $newPassword = ['uid' => $uid, 'hash' => $hash];
        }
        
        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
        
        // set template variables
        $this->view->assign('item', $newPassword);
        $this->view->assign('errors', $errors);
        $this->view->assign('newPasswordAllowed', $newPasswordAllowed);
        $this->view->assign('action', 'newPassword');
    }
    
    /**
     * action register
     *
     * @param array $register
     * @param string $email
     * @param string $hash
     * @return void
     */
    public function registerAction($register = null, $email = '', $hash = '')
    {
        // init action arrays and booleans
        $messages = [];
        $errors = [];
        $fields = [];
        $fileAddOnly = false;
        $fileRemoveOnly = false;
        
        // get fields set by plugin
        $fields = $this->helperService->configureFields(
            $this->settings['registerFields'],
            $this->settings['requiredRegisterFields'],
            $this->settings,
            $this->fieldConfig
        );
        
        // get selectable user groups
        $selectableUsergroups = $this->helperService->getUsergroups($this->settings['selectableUsergroups']);
        
        // get requestable user groups
        $requestableUsergroups = $this->helperService->getUsergroups($this->settings['requestableUsergroups']);
        
        // process form values
        if ($register) {
            // add or remove temp files by add button
            foreach ($fields as $field) {
                if ($field['config']['type']=='file') {
                    if (is_array($register[$field['key'].'_tmp'])) {
                        foreach ($register[$field['key'].'_tmp'] as $key => $value) {
                            if (isset($register[$field['key'].'_remove_'.$key])) {
                                unset($register[$field['key'].'_tmp'][$key]);
                                
                                // add message
                                $messages[] = [
                                    'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                                    'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_register', $this->extensionName),
                                    'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_register.removed.'.$field['key'], $this->extensionName),
                                    'type' => FlashMessage::OK
                                ];

                                $fileRemoveOnly = true;
                            }
                        }
                    }
                    
                    if (isset($register[$field['key']]) && isset($register[$field['key'].'_add'])) {
                        if ($register[$field['key']]['name']!='') {
                            $fileCnt = (is_array($register[$field['key'].'_tmp']))?count($register[$field['key'].'_tmp']):0;
                            
                            if (!isset($field['config']['maxitems']) || (isset($field['config']['maxitems']) && $fileCnt<$field['config']['maxitems'])) {
                                $fileInfo = $this->helperService->getFileInfo($register[$field['key']]['name']);
                                $fileName = $fileInfo['name'].'.'.$fileInfo['extension'];
                                $fileTemp = $this->helperService->copyToTemp($register[$field['key']]['tmp_name'], $fileName, self::TEMPFOLDER);
                                $register[$field['key'].'_tmp'][$fileCnt]['name'] = $fileName;
                                $register[$field['key']]['tmp_name_local'] = $fileTemp['path'];
                                $register[$field['key'].'_tmp'][$fileCnt]['src'] = $fileTemp['src'];
                                $register[$field['key'].'_tmp'][$fileCnt]['ext'] = $fileInfo['extension'];
                                $register[$field['key'].'_tmp'][$fileCnt]['size'] = $register[$field['key']]['size'];
                                $register[$field['key'].'_tmp'][$fileCnt]['object'] = serialize($register[$field['key']]);
                            
                                // add message
                                $messages[] = [
                                    'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                                    'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_register', $this->extensionName),
                                    'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_register.added.'.$field['key'], $this->extensionName),
                                    'type' => FlashMessage::OK
                                ];
                            } else {
                                
                                // add message
                                $messages[] = [
                                    'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                                    'title' => LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'], $this->extensionName),
                                    'text' => LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.too_many', $this->extensionName, [$field['config']['maxitems']]),
                                    'type' => FlashMessage::ERROR
                                ];

                                // set error
                                $errors[$field['key']] = true;
                            }
                        } else {
                            
                            // add message
                            $messages[] = [
                                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                                'title' => LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'], $this->extensionName),
                                'text' => LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.empty', $this->extensionName, [$field['config']['maxitems']]),
                                'type' => FlashMessage::ERROR
                            ];

                            // set error
                            $errors[$field['key']] = true;
                        }
                        
                        $fileAddOnly = true;
                    }
                }
            }
            
            // if general save button is pressed
            if (!$fileAddOnly && !$fileRemoveOnly) {
            
                //  set email es username if set in settings
                if ($this->settings['saveEmailAsUsername']) {
                    $register['username'] = $register['email'];
                }
                
                // check fields
                $this->helperService->checkFields($fields, $register, $messages, $errors);
                
                // set item values
                /**@var \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $item*/
                $item = $this->objectManager->get('DCNGmbH\\MooxCommunity\\Domain\\Model\\FrontendUser');
                foreach ($fields as $field) {
                    if (!in_array($field['config']['type'], ['file']) && !in_array($field['key'], ['usergroup', 'requested_usergroup'])) {
                        $setMethod = 'set'.GeneralUtility::underscoredToUpperCamelCase($field['key']);
                        if (method_exists($item, $setMethod)) {
                            $item->$setMethod(trim($register[$field['key']]));
                        }
                    }
                }
                
                // set usergroup for new users
                $addedGroups = [];
                if ($this->settings['usergroups'] != '') {
                    $groupSettings = GeneralUtility::intExplode(',', $this->settings['usergroups'], true);
                    $usedGroups = $this->frontendUserGroupRepository->findByIdList($groupSettings);
                    foreach ($usedGroups as $group) {
                        $addedGroups[] = $group;
                        $item->addUsergroup($group);
                    }
                    unset($groupSettings);
                    unset($usedGroups);
                }

                if (trim($register['usergroup']) != '') {
                    $groupSettings = GeneralUtility::intExplode(',', trim($register['usergroup']), true);
                    foreach ($groupSettings as $group) {
                        if (!in_array($group, $addedGroups) && isset($selectableUsergroups[$group])) {
                            $group = $this->frontendUserGroupRepository->findByUid($group);
                            $item->addUsergroup($group);
                        }
                    }
                }
                
                // set requested usergroup
                if (trim($register['requested_usergroup']) != '') {
                    $groups = GeneralUtility::intExplode(',', trim($register['requested_usergroup'], true));
                    foreach ($groups as $group) {
                        if (isset($requestableUsergroups[$group])) {
                            $group = $this->frontendUserGroupRepository->findByUid($group);
                            $item->addRequestedUsergroup($group);
                        }
                    }
                }
                
                // no errors -> register user
                if (!count($errors)) {
                    $item->setVariant(self::VARIANT);
                    $item->setName($this->helperService->generateName($item));

                    // @todo move file relation into utility class
                    // set file relations
                    foreach ($fields as $field) {
                        if (in_array($field['config']['type'], ['file'])) {
                            if ($register[$field['key']]['name']!='') {
                                $fileCnt = (is_array($register[$field['key'].'_tmp']))?count($register[$field['key'].'_tmp']):0;
                                $fileInfo = $this->helperService->getFileInfo($register[$field['key']]['name']);
                                $fileName = $fileInfo['name'].'.'.$fileInfo['extension'];
                                $fileTemp = $this->helperService->copyToTemp($register[$field['key']]['tmp_name'], $fileName, self::TEMPFOLDER);
                                $register[$field['key'] . '_tmp'][$fileCnt]['name'] = $fileName;
                                $register[$field['key']]['tmp_name_local'] = $fileTemp['path'];
                                $register[$field['key'] . '_tmp'][$fileCnt]['src'] = $fileTemp['src'];
                                $register[$field['key'] . '_tmp'][$fileCnt]['ext'] = $fileInfo['extension'];
                                $register[$field['key'] . '_tmp'][$fileCnt]['size'] = $register[$field['key']]['size'];
                                $register[$field['key'] . '_tmp'][$fileCnt]['object'] = serialize($register[$field['key']]);
                            }
                            
                            if (isset($register[$field['key'] . '_tmp']) && is_array($register[$field['key'] . '_tmp']) && count($register[$field['key'] . '_tmp'])) {
                                $getMethod = 'get' . GeneralUtility::underscoredToUpperCamelCase($field['key']);
                                $addMethod = 'add' . GeneralUtility::underscoredToUpperCamelCase($field['key']);
                                
                                if (method_exists($item, $getMethod) && method_exists($item, $addMethod)) {
                                    $storage = $this->storageRepository->findByUid($this->fileStorageUid);
                                    $folder = $storage->createFolder($this->fileStorageFolder);
                                    
                                    foreach ($register[$field['key'] . '_tmp'] as $tmpFile) {
                                        $tmpFile['object'] = unserialize($tmpFile['object']);
                                        
                                        $fileObject = $storage->addFile(
                                            $tmpFile['object']['tmp_name_local'], $folder, $tmpFile['object']['name']
                                        );
                                            
                                        $fileObject = $storage->getFile($fileObject->getIdentifier());
                                        $file = $this->fileRepository->findByUid($fileObject->getProperty('uid'));
                                                
                                        $fileReference = $this->objectManager->get('DCNGmbH\MooxCommunity\Domain\Model\UserImageReference');
                                        $fileReference->setFile($file);
                                        $fileReference->setCruserId(0);
                                            
                                        $item->$addMethod($fileReference);
                                        
                                        if (file_exists($tmpFile['object']['tmp_name_local'])) {
                                            unlink($tmpFile['object']['tmp_name_local']);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    // set double opt in confirmation fields
                    if ($this->settings['registerDoubleOptIn']) {
                        $item->setDisable(true);
                        $item->setRegisterHash(md5($item->getEmail().time().$GLOBALS['TSFE']->id));
                        $item->setRegisterTstamp(time());
                    }
                    
                    // save email as username if set by plugin
                    if ($this->settings['saveEmailAsUsername']) {
                        $item->setUsername($item->getEmail());
                    }

                    // add abo subscription
                    $subscriptionGroup = null;
                    if (!is_null($item->getRequestedUsergroup()->current())) {
                        $subscriptionGroup = $item->getRequestedUsergroup()->current();
                    } elseif (is_null($item->getRequestedUsergroup()->current()) && !is_null($item->getUsergroup()->current())) {
                        $subscriptionGroup = $item->getUsergroup()->current();
                    }

                    $subscription = $this->objectManager->get(AboMembership::class);
                    $subscription->setAbo($subscriptionGroup->getTitle());
                    $subscription->setAboBegin(new \DateTime());
                    $subscription->setAllowedQuotaEventNote($subscriptionGroup->getQuotaEventNote());
                    $subscription->setAllowedQuotaMarketplace($subscriptionGroup->getQuotaMarketplace());

                    if ($subscriptionGroup->getTitle() !== 'Kostenfrei') {
                        $endDate = new \DateTime();
                        $endDate->modify('+1 year');
                        $subscription->setAboEnd($endDate);
                    }

                    $item->addAboMembership($subscription);

                    // save item to database
                    $this->frontendUserRepository->add($item);
                    $this->persistenceManager->persistAll();
                    
                    // send double opt in mail
                    if ($this->settings['registerDoubleOptIn']) {
                        
                        // load mail template
                        $template = $this->templateRepository->findByUid($this->settings['registerEmailTemplate']);
                        
                        // if mail template exists
                        if ($template) {
                            
                            // set user mail field
                            $user = [];
                            foreach ($this->fields as $fieldname => $field) {
                                $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                                if (method_exists($item, $getMethod)) {
                                    $user[$fieldname] = $item->$getMethod();
                                }
                            }
                            
                            // set info mail array
                            $mail = [
                                'sender_name' => $this->settings['registerSenderName'],
                                'sender_address' => $this->settings['registerSenderAddress'],
                                'receiver_name' => ($item->getFirstName()!='')?$item->getFirstName().' '.$item->getLastName():$item->getLastName(),
                                'receiver_address' => $item->getEmail(),
                                'subject' => $this->helperService->prepareMailSubject($template->getSubject(), $user),
                                'pid' => $GLOBALS['TSFE']->id,
                                'hash' => $item->getRegisterHash(),
                                'url' => $this->uriBuilder->reset()->setNoCache(true)->setTargetPageUid($GLOBALS['TSFE']->id)->setCreateAbsoluteUri(true)->uriFor('confirmRegistration', ['email' => $user['email'], 'hash' => $item->getRegisterHash()], 'Pi1', 'MooxCommunity', 'Pi1'),
                                'user' => $user
                            ];
                            // set mail body
                            $mail['body'] = $this->helperService->prepareMailBody($template, $mail);
                            
                            // send mail
                            $this->helperService->sendMail($mail);
                            
                            // add message
                            $messages[] = [
                                'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                                'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_register', $this->extensionName),
                                'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_register.success.opt_in', $this->extensionName),
                                'type' => FlashMessage::OK
                            ];
                                                    
                            // if register redirect pid is set by plugin
                            if ($this->settings['registerRedirectPid']>0) {
                                
                                // set flash messages
                                $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
                                
                                // redirect to uri
                                $this->redirectToUri($this->uriBuilder->reset()->setNoCache(true)->setTargetPageUid($this->settings['registerRedirectPid'])->setArguments(['tx_mooxcommunity_pi2'=>['messages'=>['pi1.action_register.success.opt_in']]])->build());
                            }
                        } else {
                            
                            // add message
                            $messages[] = [
                                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                                'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_register', $this->extensionName),
                                'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_register.error.no_template', $this->extensionName),
                                'type' => FlashMessage::ERROR
                            ];
                        }
                    // if welcome mail should be sent
                    } elseif ($this->settings['registerSendWelcomeEmail']) {
                        
                        // load mail template
                        $template = $this->templateRepository->findByUid($this->settings['welcomeEmailTemplate']);
                        
                        // if mail template exists
                        if ($template) {
                            
                            // set user mail field
                            $user = [];
                            foreach ($this->fields as $fieldname => $field) {
                                $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                                if (method_exists($item, $getMethod)) {
                                    $user[$fieldname] = $item->$getMethod();
                                }
                            }
                            
                            // set info mail array
                            $mail = [
                                'sender_name' => $this->settings['registerSenderName'],
                                'sender_address' => $this->settings['registerSenderAddress'],
                                'receiver_name' => ($item->getFirstName()!='')?$item->getFirstName().' '.$item->getLastName():$item->getLastName(),
                                'receiver_address' => $item->getEmail(),
                                'subject' => $this->helperService->prepareMailSubject($template->getSubject(), $user),
                                'pid' => $GLOBALS['TSFE']->id,
                                'hash' => $item->getRegisterHash(),
                                'url' => ($this->settings['loginPid']>0)?$this->uriBuilder->reset()->setNoCache(true)->setCreateAbsoluteUri(true)->setTargetPageUid($this->settings['loginPid'])->build():$GLOBALS['TSFE']->baseUrl,
                                'user' => $user
                            ];
                            // set mail body
                            $mail['body'] = $this->helperService->prepareMailBody($template, $mail);
                            
                            // send mail
                            $this->helperService->sendMail($mail);
                        }
                            
                        // add message
                        $messages[] = [
                            'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                            'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_register', $this->extensionName),
                            'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_register.success', $this->extensionName),
                            'type' => FlashMessage::OK
                        ];
                        
                        // send admin notifications
                        $this->sendNotifications(['newuser', 'newuserwithgroup'], $item, $this->settings['adminNotificationSenderName'], $this->settings['adminNotificationSenderAddress']);

                        // if register redirect pid is set by plugin
                        if ($this->settings['registerRedirectPid']>0) {
                            
                            // set flash messages
                            $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
                            
                            // redirect to uri
                            $this->redirectToUri($this->uriBuilder->reset()->setNoCache(true)->setTargetPageUid($this->settings['registerRedirectPid'])->setArguments(['tx_mooxcommunity_pi2'=>['messages'=>['pi1.action_register.success']]])->build());
                        }
                    }
                    
                    // unser register object
                    unset($register);
                }
            }
        }
        
        // set flash messages
        $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);
        
        // set template variables
        $this->view->assign('register', $register);
        $this->view->assign('fields', $fields);
        $this->view->assign('selectableGroups', $selectableUsergroups);
        $this->view->assign('requestableGroups', $requestableUsergroups);
        $this->view->assign('errors', $errors);
        $this->view->assign('action', 'register');
    }
    
    /**
     * action confirm registration
     *
     * @param \string $email
     * @param \string $hash
     * @return void
     */
    public function confirmRegistrationAction($email = '', $hash = '')
    {
        
        // if confirmation fields are set
        if ($email!='' && $hash!='') {
            
            // find valid user to confirm registration
            $items = $this->frontendUserRepository->findByEmailAndRegisterHash($this->storagePids, $email, $hash);
            
            // check if found user is unique and hash is still usable
            if ($items->count()==1) {
                
                // set user to confirm registration
                /**@var \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $item*/
                $item = $items->getFirst();
                
                // if user is not enabled yet
                if ($item->getDisable()==1) {
                    
                    // enable user and renew register hash
                    $item->setDisable(0);
                    $item->setRegisterHash(md5($item->getRegisterHash().time()));

                    // update active subscription
                    $subscription = $item->getAboMembership()->current();
                    $subscription->setAboBegin(new \DateTime());

                    if ($subscription->getAbo() !== 'Kostenfrei') {
                        $endDate = new \DateTime();
                        $endDate->modify('+1 year');
                        $subscription->setAboEnd($endDate);
                    }

                    // save changes to database
                    $this->frontendUserRepository->update($item);
                    $this->persistenceManager->persistAll();
                    
                    // if welcome mail should be sent
                    if ($this->settings['registerSendWelcomeEmail']) {
                    
                        // load mail template
                        $template = $this->templateRepository->findByUid($this->settings['welcomeEmailTemplate']);
                        
                        // if mail template exists
                        if ($template) {
                            
                            // set user mail field
                            $user = [];
                            foreach ($this->fields as $fieldname => $field) {
                                $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                                if (method_exists($item, $getMethod)) {
                                    $user[$fieldname] = $item->$getMethod();
                                }
                            }
                            
                            // set info mail array
                            $mail = [
                                'sender_name' => $this->settings['registerSenderName'],
                                'sender_address' => $this->settings['registerSenderAddress'],
                                'receiver_name' => ($item->getFirstName()!='')?$item->getFirstName().' '.$item->getLastName():$item->getLastName(),
                                'receiver_address' => $item->getEmail(),
                                'subject' => $this->helperService->prepareMailSubject($template->getSubject(), $user),
                                'pid' => $GLOBALS['TSFE']->id,
                                'hash' => $item->getRegisterHash(),
                                'url' => ($this->settings['loginPid']>0)?$this->uriBuilder->reset()->setNoCache(true)->setCreateAbsoluteUri(true)->setTargetPageUid($this->settings['loginPid'])->build():$GLOBALS['TSFE']->baseUrl,
                                'user' => $user
                            ];
                            // set mail body
                            $mail['body'] = $this->helperService->prepareMailBody($template, $mail);
                            
                            // send mail
                            $this->helperService->sendMail($mail);
                        }
                    }
                    
                    // add message
                    $messages[] = [
                        'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                        'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_register', $this->extensionName),
                        'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_confirm_registration.success', $this->extensionName),
                        'type' => FlashMessage::OK
                    ];
                    
                    // send admin notifications
                    $this->sendNotifications(['newuser', 'newuserwithgroup'], $item, $this->settings['adminNotificationSenderName'], $this->settings['adminNotificationSenderAddress']);
                } else {
                    
                    // add message
                    $messages[] = [
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_register', $this->extensionName),
                        'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_confirm_registration.error.already_active', $this->extensionName),
                        'type' => FlashMessage::ERROR
                    ];
                }
            } elseif ($items->count()>0) {
                
                // add message
                $messages[] = [
                    'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                    'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_register', $this->extensionName),
                    'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_confirm_registration.error.not_unique', $this->extensionName),
                    'type' => FlashMessage::ERROR
                ];
            } else {
                
                // add message
                $messages[] = [
                    'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                    'title' => LocalizationUtility::translate(self::LLPATH.'pi1.action_register', $this->extensionName),
                    'text' => LocalizationUtility::translate(self::LLPATH.'pi1.action_confirm_registration.error.already_confirmed', $this->extensionName),
                    'type' => FlashMessage::ERROR
                ];
            }
            
            // set flash messages
            $this->helperService->setFlashMessages($this->flashMessageContainer, $messages);

            // if register redirect pid is set by plugin
            if ($this->settings['registerRedirectPid']>0) {
                
                // redirect to uri
                $this->redirectToUri($this->uriBuilder->reset()->setNoCache(true)->setTargetPageUid($this->settings['registerRedirectPid'])->setArguments(['tx_mooxcommunity_pi2'=>['messages'=>['pi1.action_confirm_registration.success']]])->build());
            }
        } else {
            
            // redirect to error form
            $this->redirect('error');
        }
        
        // set template variables
        $this->view->assign('action', 'confirmRegistration');
    }
    
    /**
     * action unregister
     *
     * @param \string $email
     * @param \string $hash
     * @return void
     */
    public function unregisterAction($email = '', $hash = '')
    {
        if ($email!='' && $hash!='') {
            
            /*
            $addresses = $this->addressRepository->findByEmailAndRegisterHash($this->storagePids,$email,$hash);
            if($addresses->count()==1){
                $address = $addresses->getFirst();
                if($address->getHidden()==0){
                    $successMessage = "Ihre Registrierung wurde erfolgreich gelöscht.";
                    if($this->settings['deleteUnregistered']){
                        $this->addressRepository->remove($address);
                    } else {
                        if(!$this->settings['unsetMailingAllowedOnly']){
                            $address->setHidden(1);
                        }
                        $address->setUnregistered(time());
                        $address->setMailingAllowed(0);
                        $this->addressRepository->update($address);
                    }
                    $this->persistenceManager->persistAll();

                    if($this->settings['unregisterSuccessPid']>0){
                        $uri = $this->uriBuilder->reset()->setNoCache(true)->setTargetPageUid($id);
                        $this->redirectToURI($uri);
                    }

                } else {
                    $errorMessage = "Ihre Registrierung wurde bereits gelöscht.";
                }
            } elseif($addresses->count()>0){
                $errorMessage = "Ihre Registrierung konnte nicht eindeutig zugeordnet werden. Wenden Sie sich an den Administrator.";
            } else {
                $errorMessage = "Ihre Registrierung wurde bereits gelöscht oder konnte nicht gefunden. Wenden Sie sich gegebenenfalls an den Administrator.";
            }
            $this->view->assign('successMessage', $successMessage);
            $this->view->assign('errorMessage', $errorMessage);

            */
        } else {
            $this->redirect('register');
        }
    }
    
    /**
     * action error
     *
     * @return void
     */
    public function errorAction()
    {
        $this->view->assign('action', 'error');
    }
    
    /**
     * Returns storage pids
     *
     * @return array
     */
    public function getStoragePids()
    {
        return $this->storagePids;
    }

    /**
     * Set storage pids
     *
     * @param array $storagePids storage pids
     * @return void
     */
    public function setStoragePids($storagePids)
    {
        $this->storagePids = $storagePids;
    }
    
    /**
     * Returns fields
     *
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Set fields
     *
     * @param array $fields fields
     * @return void
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
    }
    
    /**
     * Returns fieldConfig
     *
     * @return array
     */
    public function getFieldConfig()
    {
        return $this->fieldConfig;
    }

    /**
     * Set fieldConfig
     *
     * @param array $fieldConfig fieldConfig
     * @return void
     */
    public function setFieldConfig($fieldConfig)
    {
        $this->fieldConfig = $fieldConfig;
    }
    
    /**
     * Returns ext conf
     *
     * @return array
     */
    public function getExtConf()
    {
        return $this->extConf;
    }

    /**
     * Set ext conf
     *
     * @param array $extConf ext conf
     * @return void
     */
    public function setExtConf($extConf)
    {
        $this->extConf = $extConf;
    }
    
    /**
     * send notifications
     *
     * @param \array $allowed allowed notifications
     * @param \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $item item
     * @param \string $senderName sender name
     * @param \string $senderAddress sender sender address
     * @return void
     */
    public function sendNotifications($allowed = [], $item = null, $senderName = '', $senderAddress = '')
    {
        // if user set
        if ($item) {
            // if at least one admin notification is selected an admin mail address is set
            if ($this->settings['adminNotifications']!='' && $this->settings['adminAddress']!='') {
                // get notifications
                $notifications = [];
                foreach (explode(',', $this->settings['adminNotifications']) as $notification) {
                    if (in_array($notification, $allowed)) {
                        $notifications[] = $notification;
                    }
                }
                
                // if notifications available
                if (count($notifications)) {
                    // set user mail field
                    $user = [];
                    foreach ($this->fields as $fieldname => $field) {
                        $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                        if (method_exists($item, $getMethod)) {
                            $user[$fieldname] = $item->$getMethod();
                        }
                    }

                    // new user notification
                    if (in_array('newuser', $notifications) || (in_array('newuserwithgroup', $notifications))) {
                        // load mail template
                        $template = $this->templateRepository->findByUid($this->settings['adminNewUserEmailTemplate']);

                        // if mail template exists
                        if ($template) {
                            // set info mail array
                            $mail = [
                                'sender_name' => ($senderName!='')?$senderName:$this->settings['adminAddress'],
                                'sender_address' => ($senderAddress!='')?$senderAddress:$this->settings['adminAddress'],
                                'receiver_name' => $this->settings['adminAddress'],
                                'receiver_address' => $this->settings['adminAddress'],
                                'subject' => $this->helperService->prepareMailSubject($template->getSubject(), $user),
                                'pid' => $GLOBALS['TSFE']->id,
                                'user' => $user
                            ];
                            // set mail body
                            $mail['body'] = $this->helperService->prepareMailBody($template, $mail);

                            // send mail
                            $this->helperService->sendMail($mail);
                        }
                    }
                }
            }
        }
    }
}
