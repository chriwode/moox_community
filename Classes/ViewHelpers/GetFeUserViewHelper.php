<?php
namespace DCNGmbH\MooxCommunity\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 * 
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 *
 *
 * @package moox_community
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class GetFeUserViewHelper extends AbstractViewHelper
{
    
    /**
     * frontendUserRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository;

    /**
     * accessControllService
     *
     * @var \DCNGmbH\MooxCommunity\Service\AccessControlService
     * @inject
     */
    protected $accessControllService;

    /**
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('uid', 'integer', 'If specified, this UID will be used to fetch user data instead of using the current session user uid.', false, 0);
    }
    
    /**
     * @return \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $feUser
     */
    public function render()
    {
        $feUser = null;
        if (($uid == 0 || !$uid) && true === $this->accessControllService->hasLoggedInFrontendUser()) {
            $feUser = $this->frontendUserRepository->findByUid($this->accessControllService->getFrontendUserUid());
        } elseif ($uid>0) {
            $feUser = $this->frontendUserRepository->findByUid($uid);
        }
        return $feUser;
    }
}
