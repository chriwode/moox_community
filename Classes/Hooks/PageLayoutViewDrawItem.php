<?php
namespace DCNGmbH\MooxCommunity\Hooks;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package moox_community
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class PageLayoutViewDrawItem implements \TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface
{
    
    /**
     * Path to the locallang file
     * @var string
     */
    const LLPATH = 'LLL:EXT:moox_community/Resources/Private/Language/locallang_be.xlf:';
    
    /**
     * Preprocesses the preview rendering of a content element.
     *
     * @param	\TYPO3\CMS\Backend\View\PageLayoutView	$parentObject:  Calling parent object
     * @param	\boolean         						$drawItem:      Whether to draw the item using the default functionalities
     * @param	\string	        						$headerContent: Header content
     * @param	\string	        						$itemContent:   Item content
     * @param	\array									$row:           Record row of tt_content
     * @return	\void
     */
    public function preProcess(\TYPO3\CMS\Backend\View\PageLayoutView &$parentObject, &$drawItem, &$headerContent, &$itemContent, array &$row)
    {
        switch ($row['list_type']) {
            case 'mooxcommunity_pi1':
                
                // unset draw item
                $drawItem = false;
                
                // set plugin preview header
                $headerContent = $headerContent.'<strong>'.$GLOBALS['LANG']->sL(self::LLPATH.'pi1.title').'</strong><br />';
                
                // try to read flexform data of row
                $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
                $flexFormService = $objectManager->get('TYPO3\\CMS\\Extbase\\Service\\FlexFormService');
                $flexformData = $flexFormService->convertFlexFormContentToArray($row['pi_flexform']);
                
                // get current controller action string from flexform
                $actions = $flexformData['switchableControllerActions'];
                
                // if action string is not empty
                if (!empty($actions)) {
                    
                    // explode action list to array
                    $actionList = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(';', $actions);
                    
                    // replace the first action with its translation
                    $actionTranslationKey = \TYPO3\CMS\Core\Utility\GeneralUtility::camelCaseToLowerCaseUnderscored(str_replace('Pi1->', '', $actionList[0]));
                    $actionTranslation = $GLOBALS['LANG']->sL(self::LLPATH.'pi1.controller_selection.' . $actionTranslationKey);
                    
                    // set current mode
                    $mode = $actionTranslation;
                } else {
                    
                    // set current mode
                    $mode = $GLOBALS['LANG']->sL(self::LLPATH.'pi1.controller_selection.not_configured');
                }
                
                // add special content depending of current action
                if (is_array($flexformData)) {
                    switch ($actionTranslationKey) {
                        case 'register':
                            break;
                        case 'profile':
                            break;
                        case 'passwordrecovery':
                            break;
                        case 'state':
                            break;
                        default:
                    }
                }
                
                // set plugin preview content
                $itemContent = $GLOBALS['LANG']->sL(self::LLPATH.'pi1.preview.mode').': '.$mode.'<br />';
                
            break;
        }
    }
}
