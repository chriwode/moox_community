<?php
namespace DCNGmbH\MooxCommunity\Hooks;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 *
 *
 * @package moox_marketplace
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class TcaFormHelper
{
    
    /**
     * objectManager
     *
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    protected $objectManager;
    
    /**
     * helperService
     *
     * @var \DCNGmbH\MooxCommunity\Service\HelperService
     */
    protected $helperService;
    
    /**
     * frontendUserRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository;
    
    /**
     * extConf
     *
     * @var \array
     */
    protected $extConf;
    
    /**
     * Path to the locallang file
     * @var string
     */
    const LLPATH = 'LLL:EXT:moox_community/Resources/Private/Language/locallang_be.xlf:';
    
    /**
     * initialize action
     *
     * @return void
     */
    public function initialize()
    {
        
        // initialize object manager
        $this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
        
        // init helper service
        $this->helperService = $this->objectManager->get('DCNGmbH\\MooxCommunity\\Service\\HelperService');

        // init frontend user repository
        $this->frontendUserRepository = $this->objectManager->get('DCNGmbH\\MooxCommunity\\Domain\\Repository\\FrontendUserRepository');
        
        // get extensions's configuration
        $this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_community']);
    }
    
    /**
     * Modifies the select box of community users for admin selection
     *
     * @param array &$config configuration array
     * @return void
     */
    public function communityAdmins(array &$config)
    {
        
        // initialize
        $this->initialize();
        
        // init items array
        $config['items'] = array();
        
        // get items
        $items = $this->frontendUserRepository->findByFilter(null, array('first_name'=>'ASC'), null, null, 'all', null);
        
        // set selection
        foreach ($items as $item) {
            $config['items'][] = array($item->getFirstName().' '.$item->getLastName().' ('.$item->getUsername().') [UID: '.$item->getUid().']',$item->getUid());
        }
    }
    
    /**
     * Modifies the select box of community users for moderator selection
     *
     * @param array &$config configuration array
     * @return void
     */
    public function communityModerators(array &$config)
    {
        
        // initialize
        $this->initialize();
        
        // init items array
        $config['items'] = array();
        
        // get items
        $items = $this->frontendUserRepository->findByFilter(null, array('first_name'=>'ASC'), null, null, 'all', null);
        
        // set selection
        foreach ($items as $item) {
            $config['items'][] = array($item->getFirstName().' '.$item->getLastName().' ('.$item->getUsername().') [UID: '.$item->getUid().']',$item->getUid());
        }
    }
}
