<?php
namespace DCNGmbH\MooxCommunity\Hooks;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 *
 *
 * @package moox_community
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class PageNotFoundHandler
{
    
    /**
     * objectManager
     *
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    protected $objectManager;
    
    /**
     * uriBuilder
     *
     * @var \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder
     */
    protected $uriBuilder;
    
    /**
     * Redirect to login page
     *
     * @param array $params: "currentUrl", "reasonText" and "pageAccessFailureReasons"
     * @param object $tsfeObj: object type "tslib_fe"
     */
    public function pageNotFound(&$params, &$tsfeObj)
    {
                            
        // Get the extensions's configuration
        $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_community']);
        
        // get base url from php server array
        /*
        $baseUrl = $GLOBALS['_SERVER']['SERVER_NAME']."/";
        if($GLOBALS['_SERVER']['https']=="on"){
            $baseUrl = "https://".$baseUrl;
        } else {
            $baseUrl = "http://".$baseUrl;
        }
        */
        
        // get login alias from extension configuration or fallback
        if ($extConf['loginAlias']!='') {
            $loginAlias = '/'.$extConf['loginAlias'];
        }
        
        // access restricted page
        if ($loginAlias!='' && is_array($params['pageAccessFailureReasons']['fe_group']) && array_shift($params['pageAccessFailureReasons']['fe_group'])) {
            $tsfeObj->pageErrorHandler(
                $loginAlias.'?tx_mooxcommunity_pi2[redirect]=' . urlencode($params['currentUrl']),
                $GLOBALS['TYPO3_CONF_VARS']['FE']['accessRestrictedPages_handling_statheader'],
                $params['pageAccessFailureReasons']['reasonText']
            );
        // 404 not found
        } else {
            
            // handle default language
            if ($extConf['errorPage']!='') {
                $tsfeObj->pageErrorHandler('/'.$extConf['errorPage'], 'HTTP/1.0 404 Not Found');
            } else {
                $tsfeObj->pageErrorHandler('', 'HTTP/1.0 404 Not Found');
            }
        }
    }
}
