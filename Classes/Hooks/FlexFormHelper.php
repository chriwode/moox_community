<?php
namespace DCNGmbH\MooxCommunity\Hooks;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use \DCNGmbH\MooxCommunity\Controller\TemplateController;

/**
 *
 *
 * @package moox_community
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FlexFormHelper
{
    
    /**
     * objectManager
     *
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    protected $objectManager;
    
    /**
     * configurationManager
     *
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     */
    protected $configurationManager;
    
    /**
     * flexFormService
     *
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    protected $flexFormService;
    
    /**
     * helperService
     *
     * @var \DCNGmbH\MooxCommunity\Service\HelperService
     */
    protected $helperService;
    
    /**
     * pageRepository
     *
     * @var \TYPO3\CMS\Frontend\Page\PageRepository
     */
    protected $pageRepository;
    
    /**
     * contentRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\ContentRepository
     */
    protected $contentRepository;
    
    /**
     * frontendUserGroupRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\FrontendUserGroupRepository
     */
    protected $frontendUserGroupRepository;
    
    /**
     * templateRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\RemplateRepository
     */
    protected $templateRepository;
    
    /**
     * configuration
     *
     * @var \array
     */
    protected $configuration;
    
    /**
     * extConf
     *
     * @var \array
     */
    protected $extConf;
    
    /**
     * Path to the locallang file
     * @var string
     */
    const LLPATH = 'LLL:EXT:moox_community/Resources/Private/Language/locallang_be.xlf:';
    
    /**
     * initialize action
     *
     * @return void
     */
    public function initialize()
    {
        
        // initialize object manager
        $this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
        
        // initialize configuration manager
        $this->configurationManager = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');
        
        // initialize flex form service
        $this->flexFormService = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Service\\FlexFormService');
        
        // init helper service
        $this->helperService = $this->objectManager->get('DCNGmbH\\MooxCommunity\\Service\\HelperService');
        
        // initialize page repository
        $this->pageRepository = $this->objectManager->get('TYPO3\\CMS\\Frontend\\Page\\PageRepository');
        
        // initialize content repository
        $this->contentRepository = $this->objectManager->get('DCNGmbH\\MooxCommunity\\Domain\\Repository\\ContentRepository');
        
        // initialize frontend user group repository
        $this->frontendUserGroupRepository = $this->objectManager->get('DCNGmbH\\MooxCommunity\\Domain\\Repository\\FrontendUserGroupRepository');
        
        // initialize template repository
        $this->templateRepository = $this->objectManager->get('DCNGmbH\\MooxCommunity\\Domain\\Repository\\TemplateRepository');
        
        // get typoscript configuration
        $this->configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK, 'MooxCommunity');
        
        // get extensions's configuration
        $this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_community']);
    }
    
    /**
     * Itemsproc function to extend the selection of storage pid in flexform
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function storagePid(array &$config, &$pObj)
    {
        
        // initialize
        $this->initialize();
        
        // get flex form data array
        $flexformData = $this->flexFormService->convertFlexFormContentToArray($config['row']['pi_flexform']);
        
        // initialize temporary array of storage pid values
        $storagePidsTmp    = explode(',', $flexformData['settings']['storagePids']);
        
        // set storage pids from temporary array
        $storagePids = array();
        foreach ($storagePidsTmp as $storagePid) {
            
            // extract uid from value
            $storagePid = explode('|', $storagePid);
            
            // add uid to storage pids array
            $storagePids[] = $storagePid[0];
        }
        
        // if valid persistence storage pid is set within typoscript setup
        if ($this->configuration['persistence']['storagePid']!='' && in_array($this->configuration['persistence']['storagePid'], $storagePids)) {
            
            // get page info for storage pid set by typoscript
            $page = $this->pageRepository->getPage($configuration['persistence']['storagePid']);
            $definedByTs = array(array('[Defined by TS]: '.$page['title'].' [PID: '.$this->configuration['persistence']['storagePid'].']','TS'));
        }
        
        // add pid postfix to item array element and remove invalid pids from array
        for ($i=0;$i<count($config['items']);$i++) {
            if (in_array($config['items'][$i][1], $storagePids)) {
                if ($config['items'][$i][1]!='') {
                    $config['items'][$i][0] = $config['items'][$i][0].' [PID: '.$config['items'][$i][1].']';
                }
            } else {
                unset($config['items'][$i]);
            }
        }
        
        // if available add defined by ts value to items array
        if ($definedByTsTxt) {
            $config['items'] = array_merge($definedByTs, $config['items']);
        }
    }
    
    /**
     * Itemsproc function to extend the selection of storage pids in flexform
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function storagePids(array &$config, &$pObj)
    {
        
        // add pid postfix to item array element
        for ($i=0;$i<count($config['items']);$i++) {
            if ($config['items'][$i][1]!='') {
                $config['items'][$i][0] = $config['items'][$i][0].' [PID: '.$config['items'][$i][1].']';
            }
        }
    }
    
    /**
     * Itemsproc function to get a selection of allowed stream types
     *
     * @param array &$config configuration array
     * @return void
     */
    public function streamTypes(array &$config)
    {
        
        // initialize
        $this->initialize();
        
        // init items array
        $config['items'] = array();
        
        // get all types
        foreach ($this->helperService->getAvailableStreamTypes() as $key => $option) {
            $config['items'][] = array($option,$key);
        }
    }
    
    /**
     * Itemsproc function to prepare selection of register plugin pids
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function registerPid(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // get items for action "Pi1->register"
        $config['items'] = $this->getPluginPidItems($config, $pObj, 'Pi1', 'register');
    }
    
    /**
     * Itemsproc function to prepare selection of login plugin pids
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function loginPid(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // get items for action "Pi1->login"
        $config['items'] = $this->getPluginPidItems($config, $pObj, 'Pi2', 'login');
    }
    
    /**
     * Itemsproc function to prepare selection of password recovery plugin pids
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function passwordRecoveryPid(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // get items for action "Pi1->passwordRecovery"
        $config['items'] = $this->getPluginPidItems($config, $pObj, 'Pi1', 'passwordRecovery');
    }
    
    /**
     * Itemsproc function to prepare selection of password profile plugin pids
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function profilePid(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // get items for action "Pi1->profile"
        $config['items'] = $this->getPluginPidItems($config, $pObj, 'Pi1', 'profile');
    }
    
    /**
     * Itemsproc function to prepare selection of list group plugin pids
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function listGroupsPid(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // get items for action "Pi4->listGroups"
        $config['items'] = $this->getPluginPidItems($config, $pObj, 'Pi4', 'listGroups');
    }
    
    /**
     * Itemsproc function to prepare selection of list admin group plugin pids
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function listAdminGroupsPid(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // get items for action "Pi5->listAdminGroups"
        $config['items'] = $this->getPluginPidItems($config, $pObj, 'Pi5', 'listAdminGroups');
    }
    
    /**
     * Itemsproc function to prepare selection of list friendships plugin pids
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function listFriendshipsPid(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // get items for action "Pi5->listFriendships"
        $config['items'] = $this->getPluginPidItems($config, $pObj, 'Pi5', 'listFriendships');
    }
    
    /**
     * get plugin pid items
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @param string $controller controller
     * @param string $action action
     * @return void
     */
    public function getPluginPidItems(array &$config, &$pObj, $controller, $action)
    {
        
        // initialize
        $this->initialize();
        
        // init items array
        $items = array();
        
        // add item element for
        $items[] = array(0 => 'Keine Auswahl', 1 => '0');
        
        // build controller action
        $controllerAction = ucfirst($controller).'->'.$action;
        
        // get all plugin contents for "mooxcommunity_pi1"
        $plugins = $this->contentRepository->findByListType('mooxcommunity_'.strtolower($controller));
        
        // check controller pi1 and pi4
        if (in_array(strtolower($controller), array('pi1','pi4','pi5'))) {
            
            // add all plugin pid for given action as item
            foreach ($plugins as $plugin) {
                
                // get flex form data array
                $flexformData = $this->flexFormService->convertFlexFormContentToArray($plugin->getPiFlexform());
                
                // check if given action matches plugin action
                if (explode(';', $flexformData['switchableControllerActions'])[0]==$controllerAction) {
                    
                    // get page info for matching plugin pid
                    $page = $this->pageRepository->getPage($plugin->getPid());
                    
                    // set item label
                    $label = $pageInfo['title'].' [PID: '.$page['uid'].']';
                    if ($plugin->getHeader()!='') {
                        $label .= ' &raquo; '.$plugin->getHeader().' [UID: '.$plugin->getUid().']';
                    }
                    
                    // add new item to array
                    $items[] = array(0 => $label, 1 => $page['uid']);
                }
            }
        } else {
            
            // add all plugin pid for given action as item
            foreach ($plugins as $plugin) {
                
                // get page info for matching plugin pid
                $page = $this->pageRepository->getPage($plugin->getPid());
                    
                // set item label
                $label = $pageInfo['title'].' [PID: '.$page['uid'].']';
                if ($plugin->getHeader()!='') {
                    $label .= ' &raquo; '.$plugin->getHeader().' [UID: '.$plugin->getUid().']';
                }
                    
                // add new item to array
                $items[] = array(0 => $label, 1 => $page['uid']);
            }
        }
        
        return $items;
    }
    
    /**
     * Itemsproc function to process the selection of usergroups in flexform
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function usergroups(array &$config, &$pObj)
    {
        
        // initialize
        $this->initialize();
        
        // init items array
        $config['items'] = array();
                            
        // get flex form data array
        $flexformData = $this->flexFormService->convertFlexFormContentToArray($config['row']['pi_flexform']);
        
        // if one or more storage pids selected
        if ($flexformData['settings']['storagePids']!='') {
            
            // initialize temporary array of storage pid values
            $storagePidsTmp    = explode(',', $flexformData['settings']['storagePids']);
            
            // set storage pids from temporary array
            $storagePids = array();
            foreach ($storagePidsTmp as $storagePid) {
                
                // extract uid from value
                $storagePid = explode('|', $storagePid);
                
                // add uid to storage pids array
                $storagePids[] = $storagePid[0];
            }
            
            // if one or more storage pids selected
            if (count($storagePids)) {
                
                // get all frontend user groups for given storage pids
                $groups = $this->frontendUserGroupRepository->findByPids($storagePids);
                
                // if at least one group is found in database
                if ($groups) {
                    
                    // add frontend user groups to item array
                    foreach ($groups as $group) {
                        $config['items'][] = array(0 => $group->getTitle().' [UID: '.$group->getUid().']', 1 => $group->getUid());
                    }
                }
            }
        }
    }
    
    /**
     * Itemsproc function to generate the selection of template category
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function templateCategory(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // get template catgories from template controller
        $categories = TemplateController::getTemplateCategories();
        
        // add found categories to items array
        foreach ($categories as $key => $category) {
            $config['items'][] = array(0 => $category, 1 => $key);
        }
    }
    
    /**
     * Itemsproc function to generate the selection of email templates for given category
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @param string $category template category
     * @return void
     */
    public function getTemplateItems(array &$config, &$pObj, $category = '')
    {
        
        // initialize
        $this->initialize();
        
        // init items array
        $items = array();
        
        // if category is given
        if ($category!='') {
            
            // get email templates from database
            $templates = $this->templateRepository->findAll(false);
            
            // add templates with matching keys to item array
            foreach ($templates as $template) {
                if ($template->getCategory()==$category) {
                    $items[] = array(0 => $template->getTitle(), 1 => $template->getUid());
                }
            }
        }
        
        return $items;
    }
    
    /**
     * Itemsproc function to generate the selection of recovery email templates
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function recoveryEmailTemplate(array &$config, &$pObj)
    {
                
        // init items array
        $config['items'] = array();
        
        // get items for category "passwordrecovery"
        $config['items'] = $this->getTemplateItems($config, $pObj, 'passwordrecovery');
    }
    
    /**
     * Itemsproc function to generate the selection of register email templates
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function registerEmailTemplate(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // get items for category "registerconfirm"
        $config['items'] = $this->getTemplateItems($config, $pObj, 'registerconfirm');
    }
    
    /**
     * Itemsproc function to generate the selection of welcome email templates
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function welcomeEmailTemplate(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // get items for category "registerconfirm"
        $config['items'] = $this->getTemplateItems($config, $pObj, 'registerwelcome');
    }
    
    /**
     * Itemsproc function to generate the selection of profile email templates
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function profileEmailTemplate(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // get items for category "profileconfirm"
        $config['items'] = $this->getTemplateItems($config, $pObj, 'profileconfirm');
    }
    
    /**
     * Itemsproc function to generate the selection of admin new user email templates
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function adminNewUserEmailTemplate(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // get items for category "adminnewuser"
        $config['items'] = $this->getTemplateItems($config, $pObj, 'adminnewuser');
    }
    
    /**
     * Itemsproc function to generate the selection of notifications email templates
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function notificationsEmailTemplate(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // set empty item
        $config['items'][] = array($GLOBALS['LANG']->sL(self::LLPATH.'pi4.notifications_email_template.none'),0);
        
        // get items for category "communitynotifications"
        foreach ($this->getTemplateItems($config, $pObj, 'communitynotifications') as $template) {
            $config['items'][] = array($template[0],$template[1]);
        }
    }
    
    /**
     * Itemsproc function to generate the selection of switchable controller actions
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function pi1SwitchableControllerActions(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // define action list for each switchable controller actions element
        $actionsRegisterList = array('register','unregister','confirmRegistration','error');
        $actionsProfileList    = array('profile','confirmProfile','error');
        $actionsPasswordRecoveryList = array('passwordRecovery','newPassword','error');
        $actionsErrorList = array('error');
        
        // set actions array for "register"
        $actionsRegister = array();
        foreach ($actionsRegisterList as $actionRegister) {
            $actionsRegister[] = 'Pi1->'.$actionRegister;
        }
        
        // set label for "register"
        $labelRegister = $GLOBALS['LANG']->sL(self::LLPATH.'pi1.controller_selection.register');
                
        // set actions array for "profile"
        $actionsProfile = array();
        foreach ($actionsProfileList as $actionProfile) {
            $actionsProfile[] = 'Pi1->'.$actionProfile;
        }
        
        // set label for "profile"
        $labelProfile = $GLOBALS['LANG']->sL(self::LLPATH.'pi1.controller_selection.profile');
        
        // set actions array for "password recovery"
        $actionsPasswordRecovery = array();
        foreach ($actionsPasswordRecoveryList as $actionPasswordRecovery) {
            $actionsPasswordRecovery[] = 'Pi1->'.$actionPasswordRecovery;
        }
        
        // set label for "password recovery"
        $labelPasswordRecovery = $GLOBALS['LANG']->sL(self::LLPATH.'pi1.controller_selection.password_recovery');
        
        // add items
        $config['items'][] = array(0 => $labelRegister, 1 => implode(';', $actionsRegister));
        $config['items'][] = array(0 => $labelProfile, 1 => implode(';', $actionsProfile));
        $config['items'][] = array(0 => $labelPasswordRecovery, 1 => implode(';', $actionsPasswordRecovery));
        
        // set default item
        $config['config']['default'] = implode(';', $actionsRegister);
    }
    
    /**
     * Itemsproc function to generate the selection of switchable controller actions
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function pi4SwitchableControllerActions(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // define action list for each switchable controller actions element
        $actionsListGroupsList        = array('listGroups','mapGroups','detailGroup','editGroup','detailUser','editUser','addMembership','removeMembership','addFriendship','removeFriendship','error');
        $actionsMapGroupsList        = array('mapGroups','listGroups','detailGroup','editGroup','detailUser','editUser','addMembership','removeMembership','addFriendship','removeFriendship','error');
        
        // set actions array for "list groups"
        $actionsListGroups = array();
        foreach ($actionsListGroupsList as $actionListGroup) {
            $actionsListGroups[] = 'Pi4->'.$actionListGroup;
        }
        
        // set label for "list groups"
        $labelListGroups = $GLOBALS['LANG']->sL(self::LLPATH.'pi4.controller_selection.list_groups');
                
        // set actions array for "map groups"
        $actionsMapGroups = array();
        foreach ($actionsMapGroupsList as $actionMapGroup) {
            $actionsMapGroups[] = 'Pi4->'.$actionMapGroup;
        }
        
        // set label for "map groups"
        $labelMapGroups = $GLOBALS['LANG']->sL(self::LLPATH.'pi4.controller_selection.map_groups');
                
        // add items
        $config['items'][] = array(0 => $labelListGroups, 1 => implode(';', $actionsListGroups));
        $config['items'][] = array(0 => $labelMapGroups, 1 => implode(';', $actionsMapGroups));
        
        // set default item
        $config['config']['default'] = implode(';', $actionsListGroups);
    }
    
    /**
     * Itemsproc function to generate the selection of switchable controller actions
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function pi5SwitchableControllerActions(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // define action list for each switchable controller actions element
        $actionsListAdminGroupsList = array('listAdminGroups','listMemberships','listFriendships','confirmMembership','rejectMembership','confirmFriendship','rejectFriendship','listGroupMemberships','removeMembership','removeFriendship','addModerator','removeModerator','error');
        $actionsListMembershipsList = array('listMemberships','listAdminGroups','listFriendships','confirmMembership','rejectMembership','confirmFriendship','rejectFriendship','listGroupMemberships','removeMembership','removeFriendship','addModerator','removeModerator','error');
        $actionsListFriendshipsList = array('listFriendships','listAdminGroups','listMemberships','confirmMembership','rejectMembership','confirmFriendship','rejectFriendship','listGroupMemberships','removeMembership','removeFriendship','addModerator','removeModerator','error');
        
        // set actions array for "list admin groups"
        $actionsListAdminGroups = array();
        foreach ($actionsListAdminGroupsList as $actionsListAdminGroup) {
            $actionsListAdminGroups[] = 'Pi5->'.$actionsListAdminGroup;
        }
        
        // set label for "list admin groups"
        $labelListAdminGroups = $GLOBALS['LANG']->sL(self::LLPATH.'pi5.controller_selection.list_admin_groups');
        
        // set actions array for "list memberships"
        $actionsListMemberships = array();
        foreach ($actionsListMembershipsList as $actionsListMembershipsGroup) {
            $actionsListMemberships[] = 'Pi5->'.$actionsListMembershipsGroup;
        }
        
        // set label for "list memberships"
        $labelListMemberships = $GLOBALS['LANG']->sL(self::LLPATH.'pi5.controller_selection.list_memberships');
        
        // set actions array for "list friendships"
        $actionsListFriendships = array();
        foreach ($actionsListFriendshipsList as $actionsListFriendshipsGroup) {
            $actionsListFriendships[] = 'Pi5->'.$actionsListFriendshipsGroup;
        }
        
        // set label for "list friendships"
        $labelListFriendships = $GLOBALS['LANG']->sL(self::LLPATH.'pi5.controller_selection.list_friendships');
        
        // add items
        $config['items'][] = array(0 => $labelListAdminGroups, 1 => implode(';', $actionsListAdminGroups));
        $config['items'][] = array(0 => $labelListMemberships, 1 => implode(';', $actionsListMemberships));
        $config['items'][] = array(0 => $labelListFriendships, 1 => implode(';', $actionsListFriendships));
        
        // set default item
        $config['config']['default'] = implode(';', $actionsListAdminGroups);
    }
    
    /**
     * Itemsproc function to generate list of available fields
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @param string $action action
     * @param string $table table
     * @param array $excludeFields exclude fields
     * @param boolean $excludeHeader exclude header fields
     * @return void
     */
    public function fields(array &$config, &$pObj, $action = '', $table = 'fe_users', $excludeFields = array(), $excludeHeader = true)
    {
        
        // initialize
        $this->initialize();
        
        // set local language path
        $llpath = 'LLL:EXT:moox_community/Resources/Private/Language/locallang.xlf:';
        
        // init items array
        $items = array();
                        
        // get frontend user dummy
        if ($table=='fe_groups') {
            $dummy = $this->objectManager->get('DCNGmbH\\MooxCommunity\\Domain\\Model\\FrontendUserGroup');
        } else {
            $dummy = $this->objectManager->get('DCNGmbH\\MooxCommunity\\Domain\\Model\\FrontendUser');
        }
        
        // get flex form data array
        $flexformData = $this->flexFormService->convertFlexFormContentToArray($config['row']['pi_flexform']);
        
        // filter sort fields only
        if ($action=='sort') {
            $action = 'list';
            $sortable = 1;
        } elseif ($action=='sortGroups') {
            $action = 'listGroups';
            $sortable = 1;
        } elseif ($action=='sortUsers') {
            $action = 'listUsers';
            $sortable = 1;
        }
        
        // get plugin fields
        $pluginFields = $this->helperService->getPluginFields($table, 'mooxcommunity', $action);
        
        // set translation lookup array
        if (in_array($action, array('add','edit'))) {
            $lookup = 'form';
        } elseif (in_array($action, array('listGroups'))) {
            $lookup = 'list_groups';
        } else {
            $lookup = $action;
        }
        
        // add valid fields to items array
        foreach ($pluginFields as $fieldname => $field) {
            if (!($fieldname=='username' && $flexformData['settings']['saveEmailAsUsername']) && (!$excludeHeader || !$field['moox']['header']) && !in_array($fieldname, $excludeFields) && (!$sortable || $field['moox']['sortable'])) {
                
                // check if field has valid setter/getter method
                $setMethod = 'set'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                $getMethod = 'get'.GeneralUtility::underscoredToUpperCamelCase($fieldname);
                if ((in_array($action, array('add','edit','editGroup')) && method_exists($dummy, $setMethod)) || (in_array($action, array('list','detail','listGroups','listUsers','detailGroup','detailUser')) && method_exists($dummy, $getMethod)) || (!$excludeHeader && $field['moox']['header'])) {
                    $label = $GLOBALS['LANG']->sL($llpath.$lookup.'.'.$fieldname);
                    if ($label=='') {
                        $label = $GLOBALS['LANG']->sL($llpath.'form.'.$fieldname);
                    }
                    if ($label=='') {
                        $label = $GLOBALS['LANG']->sL($field['label']);
                    }
                    if ($label=='') {
                        $label = $fieldname;
                    }
                    if ($field['moox']['header']) {
                        $label = '['.$GLOBALS['LANG']->sL($llpath.'header').'] '.$label;
                    } elseif ($field['moox']['extkey']!='moox_community') {
                        $label = '['.$field['moox']['extkey'].'] '.$label;
                    }
                    $items[] = array(0 => $label, 1 => $fieldname);
                }
            }
        }
        
        return $items;
    }
    
    /**
     * Itemsproc function to generate list of available register fields
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function registerFields(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // get register fields
        $config['items'] = $this->fields($config, $pObj, 'add', 'fe_users');
    }
    
    /**
     * Itemsproc function to generate list of available profile fields
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function profileFields(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // get register fields
        $config['items'] = $this->fields($config, $pObj, 'edit', 'fe_users');
    }
    
    /**
     * Itemsproc function to generate list of available register fields (required)
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function requiredRegisterFields(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // get required register fields
        $config['items'] = $this->fields($config, $pObj, 'add', 'fe_users', array('username'));
    }
    
    /**
     * Itemsproc function to generate list of available profile fields (required)
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function requiredProfileFields(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // get required profile fields
        $config['items'] = $this->fields($config, $pObj, 'edit', 'fe_users', array('username'));
    }
    
    /**
     * Itemsproc function to generate list of available group detail fields
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function detailGroupFields(array &$config, &$pObj)
    {
        
        // set local language path
        $llpath = 'LLL:EXT:moox_community/Resources/Private/Language/locallang.xlf:';
        
        // init items array
        $config['items'] = array();
        
        // add items
        $config['items'][] = array($GLOBALS['LANG']->sL($llpath.'form.contact', true),'contact');
        $config['items'][] = array($GLOBALS['LANG']->sL($llpath.'form.location', true),'location');

        // get detail group fields
        foreach ($this->fields($config, $pObj, 'detailGroup', 'fe_groups') as $option) {
            $config['items'][] = array($option[0],$option[1]);
        }
    }
    
    /**
     * Itemsproc function to generate list of available private group detail fields
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function privateDetailGroupFields(array &$config, &$pObj)
    {
        
        // initialize
        $this->initialize();
        
        // set local language path
        $llpath = 'LLL:EXT:moox_community/Resources/Private/Language/locallang.xlf:';
        
        // get flex form data array
        $flexformData = $this->flexFormService->convertFlexFormContentToArray($config['row']['pi_flexform']);
        
        // get public fields
        $publicFields = array();
        if ($flexformData['settings']['detailGroupFields']!='') {
            $publicFieldsTmp = explode(',', $flexformData['settings']['detailGroupFields']);
        } else {
            $publicFieldsTmp = array();
        }
        foreach ($publicFieldsTmp as $publicField) {
            $publicFields[] = explode('|', $publicField)[0];
        }
        
        // init items array
        $config['items'] = array();
        
        // add items
        if (!in_array('contact', $publicFields)) {
            $config['items'][] = array($GLOBALS['LANG']->sL($llpath.'form.contact', true),'contact');
        }
        if (!in_array('location', $publicFields)) {
            $config['items'][] = array($GLOBALS['LANG']->sL($llpath.'form.location', true),'location');
        }

        // get detail group fields
        foreach ($this->fields($config, $pObj, 'detailGroup', 'fe_groups') as $option) {
            if (!in_array($option[1], $publicFields)) {
                $config['items'][] = array($option[0],$option[1]);
            }
        }
    }
    
    /**
     * Itemsproc function to generate list of available user detail fields
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function detailUserFields(array &$config, &$pObj)
    {
        
        // set local language path
        $llpath = 'LLL:EXT:moox_community/Resources/Private/Language/locallang.xlf:';
        
        // init items array
        $config['items'] = array();
        
        // add items
        $config['items'][] = array($GLOBALS['LANG']->sL($llpath.'form.full_name', true),'full_name');
        $config['items'][] = array($GLOBALS['LANG']->sL($llpath.'form.contact', true),'contact');
        $config['items'][] = array($GLOBALS['LANG']->sL($llpath.'form.location', true),'location');

        // get detail group fields
        foreach ($this->fields($config, $pObj, 'detailUser', 'fe_users') as $option) {
            $config['items'][] = array($option[0],$option[1]);
        }
    }
    
    /**
     * Itemsproc function to generate list of available private user detail fields
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function privateDetailUserFields(array &$config, &$pObj)
    {
        
        // initialize
        $this->initialize();
        
        // set local language path
        $llpath = 'LLL:EXT:moox_community/Resources/Private/Language/locallang.xlf:';
        
        // get flex form data array
        $flexformData = $this->flexFormService->convertFlexFormContentToArray($config['row']['pi_flexform']);
        
        // get public fields
        $publicFields = array();
        if ($flexformData['settings']['detailUserFields']!='') {
            $publicFieldsTmp = explode(',', $flexformData['settings']['detailUserFields']);
        } else {
            $publicFieldsTmp = array();
        }
        foreach ($publicFieldsTmp as $publicField) {
            $publicFields[] = explode('|', $publicField)[0];
        }
        
        // init items array
        $config['items'] = array();
        
        // add items
        if (!in_array('full_name', $publicFields)) {
            $config['items'][] = array($GLOBALS['LANG']->sL($llpath.'form.full_name', true),'full_name');
        }
        if (!in_array('contact', $publicFields)) {
            $config['items'][] = array($GLOBALS['LANG']->sL($llpath.'form.contact', true),'contact');
        }
        if (!in_array('location', $publicFields)) {
            $config['items'][] = array($GLOBALS['LANG']->sL($llpath.'form.location', true),'location');
        }

        // get detail group fields
        foreach ($this->fields($config, $pObj, 'detailUser', 'fe_users') as $option) {
            if (!in_array($option[1], $publicFields)) {
                $config['items'][] = array($option[0],$option[1]);
            }
        }
    }
    
    /**
     * Itemsproc function to generate list of available order fields
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function groupsOrderBy(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // add item
        $config['items'][] = array($GLOBALS['LANG']->sL(self::LLPATH.'pi4.order_by.userdefined', true),'');
        
        // get sort fields from list fields
        foreach ($this->fields($config, $pObj, 'sortGroups', 'fe_groups') as $option) {
            $config['items'][] = array($option[0],$option[1]);
        }
    }
    
    /**
     * Itemsproc function to generate list of allowed order fields
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function groupsAllowedOrderBy(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // get sort fields from list fields
        $config['items'] = $this->fields($config, $pObj, 'sortGroups', 'fe_groups');
    }
    
    /**
     * Itemsproc function to generate list of available order fields
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function usersOrderBy(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // add item
        $config['items'][] = array($GLOBALS['LANG']->sL(self::LLPATH.'pi4.order_by.userdefined', true),'');
        
        // get sort fields from list fields
        foreach ($this->fields($config, $pObj, 'sortUsers', 'fe_users') as $option) {
            $config['items'][] = array($option[0],$option[1]);
        }
    }
    
    /**
     * Itemsproc function to generate list of allowed order fields
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function usersAllowedOrderBy(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // get sort fields from list fields
        $config['items'] = $this->fields($config, $pObj, 'sortUsers', 'fe_users');
    }
    
    /**
     * Itemsproc function to generate list of edit group fields
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function editGroupFields(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // get sort fields from list fields
        $config['items'] = $this->fields($config, $pObj, 'editGroup', 'fe_groups');
    }
    
    /**
     * Itemsproc function to generate list of required edit group fields
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function requiredEditGroupFields(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        // get sort fields from list fields
        $config['items'] = $this->fields($config, $pObj, 'editGroup', 'fe_groups');
    }
    
    /**
     * Itemsproc function to generate the selection of admin notifications
     *
     * @param array &$config configuration array
     * @param mixed &$pObj configuration array
     * @return void
     */
    public function adminNotifications(array &$config, &$pObj)
    {
        
        // init items array
        $config['items'] = array();
        
        $config['items'][] = array(0 => $GLOBALS['LANG']->sL(self::LLPATH.'pi1.admin_notifications.newuser'), 1 => 'newuser');
        $config['items'][] = array(0 => $GLOBALS['LANG']->sL(self::LLPATH.'pi1.admin_notifications.newuserwithgroup'), 1 => 'newuserwithgroup');
    }
}
