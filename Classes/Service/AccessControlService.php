<?php
namespace DCNGmbH\MooxCommunity\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package moox_community
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class AccessControlService implements \TYPO3\CMS\Core\SingletonInterface
{
    
    /**
     * objectManager
     *
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     * @inject
     */
    protected $objectManager;
    
    /**
     * frontendUserRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository;
    
    /**
     * Do we have a logged in feuser
     *
     * @return boolean
     */
    public function hasLoggedInFrontendUser()
    {
        return ($GLOBALS['TSFE']->loginUser == 1) ? true : false;
    }
 
    /**
     * Get the uid of the current feuser
     *
     * @return mixed
     */
    public function getFrontendUserUid()
    {
        if ($this->hasLoggedInFrontendUser() && !empty($GLOBALS['TSFE']->fe_user->user['uid'])) {
            return intval($GLOBALS['TSFE']->fe_user->user['uid']);
        }
        return null;
    }
     
    /**
     * isAccessAllowed
     *
     * @param \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $frontendUser
     * @return boolean
     */
    public function isAccessAllowed(\DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $frontendUser)
    {
        return $this->getFrontendUserUid() === $frontendUser->getUid() ? true : false;
    }
    
    /**
     * isGroupEditAllowed
     *
     * @param \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $frontendUser
     * @param \DCNGmbH\MooxCommunity\Domain\Model\FrontendUserGroup $frontendUserGroup
     * @return boolean
     */
    public function isGroupEditAllowed(\DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $frontendUser, \DCNGmbH\MooxCommunity\Domain\Model\FrontendUserGroup $frontendUserGroup)
    {
        $admins = array();
        foreach ($frontendUserGroup->getCommunityAdmins() as $admin) {
            $admins[] = $admin->getUid();
        }
        if (in_array($frontendUser->getUid(), $admins)) {
            return true;
        }
        return false;
    }
    
    /**
     * isCreateAllowed
     *
     * @param \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $adminUser
     * @param \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $deleteUser
     * @return boolean
     */
    public function isDeleteAllowed(\DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $adminUser = null, \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $deleteUser = null)
    {
        
        // Get the extensions's configuration
        $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_community']);
        
        if ($adminUser && $deleteUser) {
            if ($extConf['useCompanyAdmin'] && $this->isAccessAllowed($adminUser) && $adminUser->getIsCompanyAdmin() && $adminUser->getCompany()!='' && $adminUser->getCompany()==$deleteUser->getCompany()) {
                return true;
            }
        }
        
        return null;
    }
    
    /**
     * isCreateAllowed
     *
     * @param \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $adminUser
     * @param \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $editUser
     * @return boolean
     */
    public function isEditAllowed(\DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $adminUser = null, \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $editUser = null)
    {
        
        // Get the extensions's configuration
        $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_community']);
        
        if ($adminUser && $editUser) {
            if ($extConf['useCompanyAdmin'] && $this->isAccessAllowed($adminUser) && $adminUser->getIsCompanyAdmin() && $adminUser->getCompany()!='' && $adminUser->getCompany()==$editUser->getCompany()) {
                return true;
            }
        }
        
        return null;
    }
    
    /**
     * isCreateAllowed
     *
     * @param \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $adminUser
     * @return boolean
     */
    public function isCreateAllowed(\DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $adminUser = null)
    {
        
        // Get the extensions's configuration
        $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_community']);
        
        if ($adminUser) {
            if ($extConf['useCompanyAdmin'] && $this->isAccessAllowed($adminUser) && $adminUser->getIsCompanyAdmin() && $adminUser->getCompany()!='') {
                return true;
            }
        }
        
        return null;
    }
    
    /**
     * login
     *
     * @param  \array $userdata
     * @return void
     */
    public function login($userdata = array())
    {
        if ($userdata['uid']>0 && !$this->hasLoggedInFrontendUser()) {
            
            /** @var $feUser \TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication */
            $feUser = $this->objectManager->get('TYPO3\\CMS\\Frontend\\Authentication\\FrontendUserAuthentication');

            $feUser->lockIP = $GLOBALS['TYPO3_CONF_VARS']['FE']['lockIP'];
            $feUser->checkPid = 0;//$GLOBALS['TYPO3_CONF_VARS']['FE']['checkFeUserPid'];
            $feUser->lifetime = intval($GLOBALS['TYPO3_CONF_VARS']['FE']['lifetime']);
            // List of pid's acceptable
            //$feUser->checkPid_value = $this->database->cleanIntList(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('pid'));
            
            $feUser->dontSetCookie = 0;
            if ($GLOBALS['TYPO3_CONF_VARS']['FE']['dontSetCookie']) {
                //$feUser->dontSetCookie = 1;
            }

            $feUser->start();
            $feUser->unpack_uc('');
            $feUser->fetchSessionData();
            
            $userdata[$feUser->lastLogin_column] = $GLOBALS['EXEC_TIME'];
            $userdata['is_online'] = $GLOBALS['EXEC_TIME'];
            $feUser->user = $userdata;

            $GLOBALS['TSFE']->fe_user = &$feUser;
            $GLOBALS['TSFE']->fe_user->createUserSession($userdata);
            $GLOBALS['TSFE']->initUserGroups();
            $GLOBALS['TSFE']->setSysPageWhereClause();
        }
    }
    
    /**
     * logoff
     *
     * @return void
     */
    public function logoff()
    {
        $GLOBALS['TSFE']->fe_user->logoff();
        $GLOBALS['TSFE']->loginUser = 0;
    }
}
