<?php
namespace DCNGmbH\MooxCommunity\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use \TYPO3\CMS\Core\Messaging\FlashMessage;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

/**
 *
 *
 * @package moox_community
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class HelperService implements \TYPO3\CMS\Core\SingletonInterface
{
    
    /**
     * objectManager
     *
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    protected $objectManager;
    
    /**
     * configurationManager
     *
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     */
    protected $configurationManager;
    
    /**
     * frontendUserRepository
     *
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\FrontendUserRepository
     */
    protected $frontendUserRepository;
    
    /**
     * configuration
     *
     * @var \array
     */
    protected $configuration;
    
    /**
     * extConf
     *
     * @var boolean
     */
    protected $extConf;
    
    /**
     * storagePids
     *
     * @var array
     */
    protected $storagePids;
    
    /**
     * Path to the locallang file
     * @var string
     */
    const LLPATH = 'LLL:EXT:moox_community/Resources/Private/Language/locallang.xlf:';
    
    /**
     *
     * @return void
     */
    public function initialize()
    {
        
        // initialize object manager
        $this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
        
        // initialize configuration manager
        $this->configurationManager = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');
        
        // initialize frontend user repository
        $this->frontendUserRepository = $this->objectManager->get('DCNGmbH\MooxCommunity\Domain\Repository\FrontendUserRepository');
        
        // get typoscript configuration
        $this->configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK, 'MooxCommunity');
        
        // get extensions's configuration
        $this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_community']);
    }
    
    /**
     * prepare mail template and render mail body
     *
     * @param \DCNGmbH\MooxCommunity\Domain\Mode\Template $template template
     * @param array $variables variables
     * @return string $emailBody email body
     */
    public function prepareMailBody($template, $variables)
    {
        
        // initialize
        $this->initialize();
        
        if (!empty($this->extConf['mailRenderingPartialRoot'])) {
            $partialRootPath = GeneralUtility::getFileAbsFileName($this->extConf['mailRenderingPartialRoot']);
            if (!is_dir($partialRootPath)) {
                unset($partialRootPath);
            }
        }
            
        if ($partialRootPath=='') {
            $partialRootPath = GeneralUtility::getFileAbsFileName(str_replace('Backend/', '', $this->configuration['view']['partialRootPath']).'Mail');
        }
        
        $mailBody = $this->objectManager->create('TYPO3\\CMS\\Fluid\\View\StandaloneView');
        $mailBody->setFormat('html');
        $mailBody->setTemplateSource($template->getTemplate());
        if ($partialRootPath!='') {
            $mailBody->setPartialRootPath($partialRootPath);
        }
        $mailBody->assignMultiple($variables);
        $mailBody = $mailBody->render();
        
        return $mailBody;
    }
    
    /**
     * prepare mail subject
     *
     * @param string $subject subject
     * @param array $variables $variables
     * @return string $subject
     */
    public function prepareMailSubject($subject, $variables = null)
    {
        $subject = str_replace('#KW#', date('W'), $subject);
        $subject = str_replace('#YEAR#', date('Y'), $subject);
        $subject = str_replace('#MONTH#', date('m'), $subject);
        $subject = str_replace('#DAY#', date('d'), $subject);
        $subject = str_replace('#TITLE#', $variables['title'], $subject);
        $subject = str_replace('#USERNAME#', $variables['username'], $subject);
        $subject = str_replace('#NAME#', $variables['name'], $subject);
        $subject = str_replace('#FIRSTNAME#', $variables['first_name'], $subject);
        $subject = str_replace('#MIDDLENAME#', $variables['middle_name'], $subject);
        $subject = str_replace('#LASTNAME#', $variables['last_name'], $subject);
        $subject = str_replace('#EMAIL#', $variables['email'], $subject);
                
        return $subject;
    }
    
    /**
     * send mail
     *
     * @param array $mail mail
     * @return void
     */
    public function sendMail($mail)
    {
        
        // initialize
        $this->initialize();
        
        if ($this->extConf['useSMTP']) {
            $TYPO3_CONF_VARS['MAIL']['transport'] = 'smtp';
            if ($this->extConf['smtpEncrypt']!='' && $this->extConf['smtpEncrypt']!='none') {
                $TYPO3_CONF_VARS['MAIL']['transport_smtp_server'] = $this->extConf['smtpEncrypt'];
            }
            $TYPO3_CONF_VARS['MAIL']['transport_smtp_encrypt']  = $this->extConf['smtpServer'];
            $TYPO3_CONF_VARS['MAIL']['transport_smtp_username'] = $this->extConf['smtpUsername'];
            $TYPO3_CONF_VARS['MAIL']['transport_smtp_password'] = $this->extConf['smtpPassword'];
        }
        
        if ($mail['sender_name']=='') {
            $mail['sender_name'] = $mail['sender_address'];
        }
        
        if ($mail['receiver_name']=='') {
            $mail['receiver_name'] = $mail['receiver_address'];
        }
        
        $sendMail = GeneralUtility::makeInstance('TYPO3\CMS\Core\Mail\MailMessage');
        $sendMail->setFrom(array($mail['sender_address'] => $mail['sender_name']));
        $sendMail->setTo(array($mail['receiver_address'] => $mail['receiver_name']));
        $sendMail->setSubject($mail['subject']);
        $sendMail->setBody(strip_tags($mail['body']));
        $sendMail->addPart($mail['body'], 'text/html');
        $sendMail->send();
    }
    
    /**
     * generate name from user object
     *
     * @param \DCNGmbH\MooxCommunity\Domain\Model\FrontendUser $item $item
     * @return string $name
     */
    public function generateName($item = null)
    {
        $name = '';
        
        if ($item->getFirstName()!='') {
            $name = $item->getFirstName();
        }
        if ($item->getMiddleName()!='' && $name=='') {
            $name = $item->getMiddleName();
        } elseif ($item->getMiddleName()!='' && $name!='') {
            $name .= ' '.$item->getMiddleName();
        }
        if ($item->getLastName()!='' && $name=='') {
            $name = $item->getLastName();
        } elseif ($item->getLastName()!='' && $name!='') {
            $name .= ' '.$item->getLastName();
        }
                
        return $name;
    }
    
    /**
     * check dynamic form fields
     *
     * @param \array $fields fields
     * @param \array $item item
     * @param \array &$messages messages
     * @param \array &$errors errors
     * @return void
     */
    public function checkFields($fields = array(), $item = array(), &$messages, &$errors)
    {
        
        // initialize
        $this->initialize();

        // check fields
        foreach ($fields as $field) {
            if (in_array($field['config']['type'], array('check'))) {
                if (is_array($item[$field['key']])) {
                    $item[$field['key']] = implode(',', $item[$field['key']]);
                }
            }
            
            if (in_array($field['config']['type'], array('tree'))) {
                if (isset($field['config']['minitems']) && $field['config']['minitems']>0) {
                    $field['config']['required'] = 1;
                }
            }
            
            if (in_array($field['config']['type'], array('tree','file'))) {
                if ($field['config']['required'] && (!isset($field['config']['minitems']) || $field['config']['minitems']<1)) {
                    $field['config']['minitems'] = 1;
                }
            }
            
            if (in_array($field['config']['type'], array('file'))) {
                if ($field['config']['required'] && (!isset($field['config']['maxfilesize']) || $field['config']['maxfilesize']<1)) {
                    $field['config']['maxfilesize'] = 102400;
                }
            }
            
            $msgtitle = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'], $this->extensionName);
            // set fallback title
            if (!$msgtitle) {
                $msgtitle = LocalizationUtility::translate(str_replace('moox_marketplace', $field['extkey'], self::LLPATH).'form.'.$field['key'], $field['extkey']);
            }
                
            // check required fields only
            if ($field['config']['validator']=='tree' && ((isset($field['config']['minitems']) && $field['config']['minitems']>0) || (isset($field['config']['maxitems']) && $field['config']['maxitems']>0))) {
                if ($item[$field['key']]=='') {
                    $itemCnt = 0;
                } else {
                    $items = explode(',', $item[$field['key']]);
                    $itemCnt = count($items);
                }
                
                // if no file choosen
                if ($itemCnt<$field['config']['minitems']) {
                    $message = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.too_few', $this->extensionName, array($field['config']['minitems']));
                    
                    // set fallback message
                    if (!$message) {
                        $message = LocalizationUtility::translate(str_replace('moox_marketplace', $field['extkey'], self::LLPATH).'form.'.$field['key'].'.error.too_few', $field['extkey'], array($field['config']['minitems']));
                    }
                    if (!$message) {
                        $message = LocalizationUtility::translate(self::LLPATH.'form.error.too_few', $this->extensionName, array($field['config']['minitems']));
                    }
                    
                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => $msgtitle,
                        'text' => $message,
                        'type' => FlashMessage::ERROR
                    );
                    
                    // set error
                    $errors[$field['key']] = true;
                }
                
                // if to many files choosen
                if ($item[$field['key']]!='' && isset($field['config']['maxitems']) && $itemCnt>$field['config']['maxitems']) {
                    
                    // prepare message
                    $message = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.too_many', $this->extensionName, array($field['config']['maxitems']));
                    
                    // set fallback message
                    if (!$message) {
                        $message = LocalizationUtility::translate(str_replace('moox_marketplace', $field['extkey'], self::LLPATH).'form.'.$field['key'].'.error.too_many', $field['extkey'], array($field['config']['maxitems']));
                    }
                    if (!$message) {
                        $message = LocalizationUtility::translate(self::LLPATH.'form.error.too_many', $this->extensionName, array($field['config']['maxitems']));
                    }
                    
                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => $msgtitle,
                        'text' => $message,
                        'type' => FlashMessage::ERROR
                    );
                    
                    // set error
                    $errors[$field['key']] = true;
                }
                
            // check required fields only
            } elseif ($field['config']['validator']=='file' && ((isset($field['config']['minitems']) && $field['config']['minitems']>0) || (isset($field['config']['maxitems']) && $field['config']['maxitems']>0) || (isset($field['config']['accepts']) && $field['config']['accepts']!=''))) {
                $fileError = false;
                
                if ($item[$field['key']]['name']!='' && $field['config']['accepts']!='') {
                    $accepts = explode(',', strtolower($field['config']['accepts']));
                    $fileInfo = $this->getFileInfo($item[$field['key']]['name']);
                    if (!in_array($fileInfo['extension'], $accepts)) {
                        
                        // prepare message
                        $message = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.invalid', $this->extensionName, array($fileInfo['extension'],implode(',', $accepts)));
                        
                        // set fallback message
                        if (!$message) {
                            $message = LocalizationUtility::translate(str_replace('moox_marketplace', $field['extkey'], self::LLPATH).'form.'.$field['key'].'.error.invalid', $field['extkey'], array($fileInfo['extension'],implode(',', $accepts)));
                        }
                        if (!$message) {
                            $message = LocalizationUtility::translate(self::LLPATH.'form.error.invalid', $this->extensionName, array($fileInfo['extension'],implode(',', $accepts)));
                        }
                        
                        // add message
                        $messages[] = array(
                            'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                            'title' => $msgtitle,
                            'text' => $message,
                            'type' => FlashMessage::ERROR
                        );
                        
                        // set error
                        $errors[$field['key']] = true;
                        
                        $fileError = true;
                    }
                }
                
                if (!$fileError) {
                    if (!isset($item[$field['key'].'_tmp']) || !is_array($item[$field['key'].'_tmp'])) {
                        $fileCnt = 0;
                    } else {
                        $fileCnt = count($item[$field['key'].'_tmp']);
                    }
                    if ($item[$field['key']]['name']!='') {
                        $fileCnt++;
                    }
                    
                    // if no file choosen
                    if ($fileCnt<$field['config']['minitems']) {
                        
                        // prepare message
                        $message = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.too_few', $this->extensionName, array($field['config']['minitems']));
                        
                        // set fallback message
                        if (!$message) {
                            $message = LocalizationUtility::translate(str_replace('moox_marketplace', $field['extkey'], self::LLPATH).'form.'.$field['key'].'.error.too_few', $field['extkey'], array($field['config']['minitems']));
                        }
                        if (!$message) {
                            $message = LocalizationUtility::translate(self::LLPATH.'form.error.too_few', $this->extensionName, array($field['config']['minitems']));
                        }
                        
                        // add message
                        $messages[] = array(
                            'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                            'title' => $msgtitle,
                            'text' => $message,
                            'type' => FlashMessage::ERROR
                        );
                        
                        // set error
                        $errors[$field['key']] = true;
                    }
                    
                    // if to many files choosen
                    if ($item[$field['key']]['name']!='' && isset($field['config']['maxitems']) && $fileCnt>$field['config']['maxitems']) {
                        
                        // prepare message
                        $message = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.too_many', $this->extensionName, array($field['config']['maxitems']));
                        
                        // set fallback message
                        if (!$message) {
                            $message = LocalizationUtility::translate(str_replace('moox_marketplace', $field['extkey'], self::LLPATH).'form.'.$field['key'].'.error.too_many', $field['extkey'], array($field['config']['maxitems']));
                        }
                        if (!$message) {
                            $message = LocalizationUtility::translate(self::LLPATH.'form.error.too_many', $this->extensionName, array($field['config']['maxitems']));
                        }
                        
                        // add message
                        $messages[] = array(
                            'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                            'title' => $msgtitle,
                            'text' => $message,
                            'type' => FlashMessage::ERROR
                        );
                        
                        // set error
                        $errors[$field['key']] = true;
                    }
                }
                
            // check required fields only
            } elseif (!in_array($field['config']['type'], array('file')) && ($field['config']['required'] || isset($field['config']['maxlength']) || isset($field['config']['minlength']) || isset($field['config']['limit-low']) || isset($field['config']['limit-high']))) {
                
                // check if field has a value
                if ($field['config']['required'] && (trim($item[$field['key']])=='' || ($field['key']=='gender' && trim($item[$field['key']])==0))) {
                    
                    // prepare message
                    $message = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.empty', $this->extensionName);
                    
                    // set fallback message
                    if (!$message) {
                        $message = LocalizationUtility::translate(str_replace('moox_marketplace', $field['extkey'], self::LLPATH).'form.'.$field['key'].'.error.empty', $field['extkey']);
                    }
                    if (!$message) {
                        $message = LocalizationUtility::translate(self::LLPATH.'form.error.empty', $this->extensionName);
                    }
                    
                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => $msgtitle,
                        'text' => $message,
                        'type' => FlashMessage::ERROR
                    );

                    // set error
                    $errors[$field['key']] = true;
                // check if field value smaller than maxlength
                } elseif (trim($item[$field['key']])!='' && isset($field['config']['maxlength']) && strlen(trim($item[$field['key']]))>$field['config']['maxlength']) {
                    
                    // prepare message
                    $message = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.too_long', $this->extensionName, array($field['config']['maxlength']));
                    
                    // set fallback message
                    if (!$message) {
                        $message = LocalizationUtility::translate(str_replace('moox_marketplace', $field['extkey'], self::LLPATH).'form.'.$field['key'].'.error.too_long', $field['extkey'], array($field['config']['maxlength']));
                    }
                    if (!$message) {
                        $message = LocalizationUtility::translate(self::LLPATH.'form.error.too_long', $this->extensionName, array($field['config']['maxlength']));
                    }
                    
                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => $msgtitle,
                        'text' => $message,
                        'type' => FlashMessage::ERROR
                    );

                    // set error
                    $errors[$field['key']] = true;
                    
                // check if field value larger than minlength
                } elseif (trim($item[$field['key']])!='' && isset($field['config']['minlength']) && strlen(trim($item[$field['key']]))<$field['config']['minlength']) {
                    
                    // prepare message
                    $message = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.too_short', $this->extensionName, array($field['config']['minlength']));
                    
                    // set fallback message
                    if (!$message) {
                        $message = LocalizationUtility::translate(str_replace('moox_marketplace', $field['extkey'], self::LLPATH).'form.'.$field['key'].'.error.too_short', $field['extkey'], array($field['config']['minlength']));
                    }
                    if (!$message) {
                        $message = LocalizationUtility::translate(self::LLPATH.'form.error.too_short', $this->extensionName, array($field['config']['minlength']));
                    }
                    
                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => $msgtitle,
                        'text' => $message,
                        'type' => FlashMessage::ERROR
                    );

                    // set error
                    $errors[$field['key']] = true;
                
                // check if field value greater than lowlimit
                } elseif (trim($item[$field['key']])!='' && isset($field['config']['limit-low']) && trim($item[$field['key']])<$field['config']['limit-low']) {
                    
                    // prepare message
                    $message = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.too_small', $this->extensionName, array($field['config']['limit-low']));
                    
                    // set fallback message
                    if (!$message) {
                        $message = LocalizationUtility::translate(str_replace('moox_marketplace', $field['extkey'], self::LLPATH).'form.'.$field['key'].'.error.too_small', $field['extkey'], array($field['config']['limit-low']));
                    }
                    if (!$message) {
                        $message = LocalizationUtility::translate(self::LLPATH.'form.error.too_small', $this->extensionName, array($field['config']['limit-low']));
                    }
                    
                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => $msgtitle,
                        'text' => $message,
                        'type' => FlashMessage::ERROR
                    );

                    // set error
                    $errors[$field['key']] = true;
                
                // check if field value smaller than highlimit
                } elseif (trim($item[$field['key']])!='' && isset($field['config']['limit-high']) && trim($item[$field['key']])>$field['config']['limit-high']) {
                    
                    // prepare message
                    $message = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.too_large', $this->extensionName, array($field['config']['limit-high']));
                    
                    // set fallback message
                    if (!$message) {
                        $message = LocalizationUtility::translate(str_replace('moox_marketplace', $field['extkey'], self::LLPATH).'form.'.$field['key'].'.error.too_large', $field['extkey'], array($field['config']['limit-high']));
                    }
                    if (!$message) {
                        $message = LocalizationUtility::translate(self::LLPATH.'form.error.too_large', $this->extensionName, array($field['config']['limit-high']));
                    }
                    
                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => $msgtitle,
                        'text' => $message,
                        'type' => FlashMessage::ERROR
                    );

                    // set error
                    $errors[$field['key']] = true;
                
                // check if email is valid
                } elseif ($field['config']['validator']=='email' && !GeneralUtility::validEmail(trim($item[$field['key']]))) {
                    
                    // prepare message
                    $message = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.invalid', $this->extensionName);
                    
                    // set fallback message
                    if (!$message) {
                        $message = LocalizationUtility::translate(str_replace('moox_marketplace', $field['extkey'], self::LLPATH).'form.'.$field['key'].'.error.invalid', $field['extkey']);
                    }
                    if (!$message) {
                        $message = LocalizationUtility::translate(self::LLPATH.'form.error.invalid', $this->extensionName);
                    }
                    
                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => $msgtitle,
                        'text' => $message,
                        'type' => FlashMessage::ERROR
                    );

                    // set error
                    $errors[$field['key']] = true;
                
                // check if field is uniqe
                } elseif (in_array($field['key'], array('username','email')) && $field['config']['unique'] && $this->frontendUserRepository->findByUsername($this->storagePids, trim($item[$field['key']]), false)->count()>0) {
                                        
                    // prepare message
                    $message = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.existing', $this->extensionName);
                    
                    // set fallback message
                    if (!$message) {
                        $message = LocalizationUtility::translate(self::LLPATH.'form.error.existing', $this->extensionName);
                    }
                    
                    
                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'], $this->extensionName),
                        'text' => $message,
                        'type' => FlashMessage::ERROR
                    );

                    // set error
                    $errors[$field['key']] = true;
                
                // check if passwords are equal
                } elseif ($field['config']['validator']=='password' && trim($item[$field['key']])!=trim($item[$field['key'].'_repeat'])) {
                    
                    // prepare message
                    $message = LocalizationUtility::translate(self::LLPATH.'form.'.$field['key'].'.error.not_equal', $this->extensionName);
                    
                    // set fallback message
                    if (!$message) {
                        $message = LocalizationUtility::translate(str_replace('moox_marketplace', $field['extkey'], self::LLPATH).'form.'.$field['key'].'.error.not_equal', $field['extkey']);
                    }
                    if (!$message) {
                        $message = LocalizationUtility::translate(self::LLPATH.'form.error.not_equal', $this->extensionName);
                    }
                    
                    
                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                        'title' => $msgtitle,
                        'text' => $message,
                        'type' => FlashMessage::ERROR
                    );
                    
                    // set errors
                    $errors[$field['key']] = true;
                    $errors[$field['key'].'_repeat'] = true;
                }
            }
        }
    }
    
    /**
     * set flash messages
     *
     * @param \mixed &$flashMessageContainer
     * @param \array $messages
     * @return void
     */
    public function setFlashMessages(&$flashMessageContainer = null, $messages = array())
    {
        if ($flashMessageContainer) {
        
            // set flash messages
            foreach ($messages as $message) {
                if ($message['text']=='') {
                    $message['text'] = 'Unbekannter Fehler / Unknown error';
                }
                if ($message['icon']!='' && $message['title']!='') {
                    $message['title'] = $message['icon'].$message['title'];
                }
                $flashMessageContainer->add($message['text'], ($message['title']!='')?$message['title'].': ':'', $message['type'], true);
            }
        }
    }
    
    /**
     * get plugin field groups
     *
     * @param \string $model
     * @param \string $plugin
     * @param \string $action
     * @return \array $groups
     */
    public function getPluginFieldGroups($model = '', $plugin = '', $action = '')
    {
        $groups = array();
        
        if ($model!='' && $plugin != '' && is_array($GLOBALS['TCA'][$model]['moox']['fieldGroups']) && count($GLOBALS['TCA'][$model]['moox']['fieldGroups'])) {
            foreach ($GLOBALS['TCA'][$model]['moox']['fieldGroups'] as $groupname => $group) {
                if ($group['fields']!='') {
                    $groups[] = array($group['label'],'moox_fg_'.$groupname);
                }
            }
        }
        
        return $groups;
    }
    
    /**
     * get plugin fields
     *
     * @param \string $model
     * @param \string $plugin
     * @param \string $action
     * @return \array $fields
     */
    public function getPluginFields($model = '', $plugin = '', $action = '')
    {
        $fields = array();
        
        if ($model!='' && $plugin != '' && is_array($GLOBALS['TCA'][$model]['columns']) && count($GLOBALS['TCA'][$model]['columns'])) {
            foreach ($GLOBALS['TCA'][$model]['columns'] as $fieldname => $field) {
                if (is_array($field['moox']['plugins'])) {
                    if (($action=='all' && (in_array($plugin, $field['moox']['plugins']) || is_array($field['moox']['plugins'][$plugin]))) || in_array($plugin, $field['moox']['plugins']) || ($action!='' && is_array($field['moox']['plugins'][$plugin]) && in_array($action, $field['moox']['plugins'][$plugin]))) {
                        $field['table'] = $model;
                        $fields[$fieldname] = $field;
                    }
                }
            }
        }

        return $fields;
    }
    
    /**
     * get field config
     *
     * @param \array $fields
     * @param \array $settings
     * @return \array $fieldConfig
     */
    public function getFieldConfig($fields = array(), $settings = array())
    {
        
        // initialize
        $this->initialize();
        $fieldConfig = array();
        
        $hours = array();
        for ($i = 0; $i < 24; $i++) {
            $hours[str_pad($i, 2, '0', STR_PAD_LEFT)] = str_pad($i, 2, '0', STR_PAD_LEFT);
        }
        
        $minutes = array();
        for ($i = 0; $i < 60; $i+=5) {
            $minutes[str_pad($i, 2, '0', STR_PAD_LEFT)] = str_pad($i, 2, '0', STR_PAD_LEFT);
        }
        
        foreach ($fields as $fieldname => $field) {
            $config = array();
            
            if (!$field['moox']['header']) {
                $eval = array();
                if ($field['config']['eval']!='') {
                    $eval = explode(',', $field['config']['eval']);
                }
                
                $config['model'] = $field['table'];
                $config['extkey'] = $field['moox']['extkey'];
                $config['extkeyUCC'] = GeneralUtility::underscoredToUpperCamelCase($config['extkey']);
                                
                if (in_array('password', $eval)) {
                    $config['type'] = 'password';
                    $config['validator'] = 'password';
                    $config['data']['data-type'] = 'password';
                    $config['data']['data-validator'] = 'password';
                } elseif ($fieldname=='email') {
                    $config['type'] = 'email';
                    $config['validator'] = 'email';
                    $config['data']['data-type'] = 'email';
                    $config['data']['data-validator'] = 'email';
                } elseif (in_array('email', $eval)) {
                    $config['type'] = 'email';
                    $config['validator'] = 'email';
                    $config['data']['data-type'] = 'email';
                    $config['data']['data-validator'] = 'email';
                } elseif (in_array('date', $eval)) {
                    $config['type'] = 'date';
                    $config['hours'] = $hours;
                    $config['minutes'] = $minutes;
                    $config['data']['data-type'] = 'date';
                } elseif (in_array('datetime', $eval)) {
                    $config['type'] = 'datetime';
                    $config['hours'] = $hours;
                    $config['minutes'] = $minutes;
                    $config['data']['data-type'] = 'datetime';
                } elseif ($field['config']['type']=='inline' && $field['config']['foreign_table']=='sys_file_reference') {
                    $config['type'] = 'file';
                    $config['reference-type'] = ($field['config']['reference'])?$field['config']['reference']:'file';
                    $config['validator'] = 'file';
                    $config['data']['data-type'] = 'file';
                    $config['data']['data-validator'] = 'file';
                } elseif ($field['config']['type']=='check') {
                    $config['type'] = 'check';
                    $config['validator'] = 'check';
                    $config['data']['data-type'] = 'check';
                    $config['data']['data-validator'] = 'check';
                } elseif ($field['config']['type']=='text') {
                    if (isset($field['defaultExtras']) && strpos($field['defaultExtras'], 'richtext')!== false) {
                        $config['type'] = 'editor';
                    } else {
                        $config['type'] = 'textarea';
                    }
                    $config['data']['data-type'] = 'text';
                } elseif ($field['config']['type']=='select') {
                    $config['type'] = 'select';
                    $config['data']['data-type'] = 'select';
                } elseif ($field['config']['type']=='radio') {
                    $config['type'] = 'radio';
                    $config['data']['data-type'] = 'radio';
                } else {
                    $config['type'] = 'text';
                    $config['data']['data-type'] = 'text';
                }
                
                if ($field['config']['unique']) {
                    $config['unique'] = 1;
                    $config['data']['data-unique'] = 1;
                }
                
                if (in_array($field['config']['type'], array('select','radio'))) {
                    if (is_array($field['config']['items'])) {
                        $config['items'] = array();
                        foreach ($field['config']['items'] as $item) {
                            if (substr($item[0], 0, 4)=='LLL:') {
                                $label = LocalizationUtility::translate($item[0], $config['extkeyUCC']);
                            } else {
                                $label = $item[0];
                            }
                            $config['items'][$item[1]] = $label;
                        }
                        if ($field['config']['foreign_table']!='') {
                            $itemsFromDb = $this->frontendUserRepository->findTcaFieldValues($fieldname, $this->storagePids, $field['config']['foreign_table'], $field['config']['foreign_table_where']);
                            foreach ($itemsFromDb as $item) {
                                $config['items'][$item['uid']] = $item['title'];
                            }
                        }
                    }
                }
                
                if ($field['config']['type']=='check') {
                    if (is_array($field['config']['items'])) {
                        $config['items'] = array();
                        foreach ($field['config']['items'] as $item) {
                            if (substr($item[0], 0, 4)=='LLL:') {
                                $label = LocalizationUtility::translate($item[0], $config['extkeyUCC']);
                            } else {
                                $label = $item[0];
                            }
                            $config['items'][$item[1]] = $label;
                        }
                        if ($field['config']['foreign_table']!='') {
                            $itemsFromDb = $this->frontendUserRepository->findTcaFieldValues($fieldname, $this->storagePids, $field['config']['foreign_table'], $field['config']['foreign_table_where']);
                            foreach ($itemsFromDb as $item) {
                                $config['items'][$item['uid']] = $item['title'];
                            }
                        }
                    } else {
                        $label_select = self::LLPATH.'form.'.$fieldname.'.select';
                        $label_select_test = LocalizationUtility::translate($label_select, $config['extkeyUCC']);
                        if ($label_select_test=='') {
                            $label_select = self::LLPATH.'form.'.$fieldname;
                        }
                        $config['label-select'] = $label_select;
                        
                        $label_0 = LocalizationUtility::translate(self::LLPATH.'form.'.$fieldname.'.select.0', $config['extkeyUCC']);
                        if ($label_0=='') {
                            $label_0 = LocalizationUtility::translate(self::LLPATH.'form.deactivated', $config['extkeyUCC']);
                        }
                        $label_1 = LocalizationUtility::translate(self::LLPATH.'form.'.$fieldname.'.select.1', $config['extkeyUCC']);
                        if ($label_1=='') {
                            $label_1 = LocalizationUtility::translate(self::LLPATH.'form.activated', $config['extkeyUCC']);
                        }
                        
                        $config['items'] = array();
                        $config['items'][0] = $label_0;
                        $config['items'][1] = $label_1;
                    }
                }

                if (in_array('date', $eval) || in_array('datetime', $eval) || in_array($fieldname, array('crdate','tstamp'))) {
                    $config['format'] = 'date';
                } elseif (in_array($field['config']['type'], array('select','radio'))) {
                    $config['format'] = 'selected';
                } elseif (in_array($field['config']['type'], array('check'))) {
                    $config['format'] = 'checked';
                } elseif ($config['type']=='file') {
                    $config['format'] = 'file';
                } else {
                    $config['format'] = 'text';
                }
                
                if ($settings[$fieldname.'Minlength']>0) {
                    $config['minlength'] = $settings[$fieldname.'Minlength'];
                    $config['data']['data-minlength'] = $settings[$fieldname.'Minlength'];
                } elseif ($field['config']['min']>0) {
                    $config['minlength'] = $field['config']['min'];
                    $config['data']['data-minlength'] = $field['config']['min'];
                }
                if ($field['config']['max']>0) {
                    $config['maxlength'] = $field['config']['max'];
                    $config['data']['data-maxlength'] = $field['config']['max'];
                }
                if ($field['config']['range']['lower']>0) {
                    $config['limit-low'] = $field['config']['range']['lower'];
                    $config['data']['data-limit-low'] = $field['config']['range']['lower'];
                }
                if ($field['config']['range']['upper']>0) {
                    $config['limit-high'] = $field['config']['range']['upper'];
                    $config['data']['data-limit-high'] = $field['config']['range']['upper'];
                }
                if ($field['config']['maxitems']>0) {
                    $config['maxitems'] = $field['config']['maxitems'];
                    $config['data']['data-maxitems'] = $field['config']['maxitems'];
                }
                if ($field['config']['minitems']>0) {
                    $config['minitems'] = $field['config']['minitems'];
                    $config['data']['data-minitems'] = $field['config']['minitems'];
                }
                if ($field['config']['maxfilesize']>0) {
                    $config['maxfilesize'] = $field['config']['maxfilesize'];
                    $config['data']['data-maxfilesize'] = $field['config']['maxfilesize'];
                }
                if ($field['config']['accepts']!='') {
                    $config['accepts'] = $field['config']['accepts'];
                    $config['data']['accept'] = $field['config']['accepts'];
                }
                if ($field['config']['cols']!='') {
                    $config['cols'] = $field['config']['cols'];
                }
                if ($field['config']['rows']!='') {
                    $config['rows'] = $field['config']['rows'];
                }
                if ($field['moox']['default']!='') {
                    $config['default'] = $field['moox']['default'];
                }
            } else {
                $config['variant'] = $field['moox']['variant'];
                $config['extkey'] = $field['moox']['extkey'];
                $config['extkeyUCC'] = GeneralUtility::underscoredToUpperCamelCase($config['extkey']);
                $config['type'] = 'header';
                $config['passthrough'] = 1;
            }
            
            $fieldConfig[$fieldname] = $config;
        }
        
        return $fieldConfig;
    }
    
    /**
     * configure fields
     *
     * @param \string $fieldlist
     * @param \string $requiredFieldlist
     * @param \array $fieldConfig
     * @return \array $configuredFields
     */
    public function configureFields($fieldlist = '', $requiredFieldlist = '', $settings = array(), $fieldConfig = array())
    {
        $configuredFields = array();
        $requiredFields    = array();
        
        // if fiels in field list
        if ($fieldlist!='') {
            $fields = explode(',', $fieldlist);
        } else {
            $fields = array();
        }
        
        // remove username field if email is used as username
        if ($this->settings['saveEmailAsUsername']) {
            if (!in_array('email', $fields)) {
                $fields[] = 'email';
            }
            unset($fields['username']);
        }
        
        // if fiels in required field list
        if ($requiredFieldlist!='') {
            $requiredFields = explode(',', $requiredFieldlist);
        }
        
        foreach ($fields as $field) {
            if (isset($fieldConfig[$field])) {
                $config = $fieldConfig[$field];
            } else {
                $config = array('type'=>'text','data'=>array('data-type'=>'text'));
            }
            if (in_array($field, $requiredFields)) {
                $config['required'] = 1;
            } else {
                $config['required'] = 0;
            }
            $config['data']['data-id'] = $field;
            $config['data']['data-required'] = $config['required'];
            if ($config['required'] || $config['data']['data-minlength'] || $config['data']['data-maxlength'] || $config['data']['data-limit-low'] || $config['data']['data-limit-hight'] || $config['data']['data-minitems'] || $config['data']['data-maxitems']) {
                $config['validate'] = 1;
            } else {
                $config['validate'] = 0;
            }
            $config['data']['data-label'] = LocalizationUtility::translate(self::LLPATH.'form.'.$field, $config['extkeyUCC']);
            if ($config['type']=='header') {
                $labelPrefix = 'header';
            } else {
                $labelPrefix = 'form';
            }
            $configuredFields[$field] = array('key' => (string)$field, 'extkey' => (string)$config['extkey'], 'label' => $labelPrefix.'.'.$field, 'config' => $config);
        }
        
        // remove username field if email is used as username
        if ($settings['saveEmailAsUsername']) {
            unset($configuredFields['username']);
            $configuredFields['email']['config']['unique'] = 1;
            $configuredFields['email']['config']['data']['data-unique'] = 1;
        }
        // if saveEmailAsUsername or registerDoubleOptIn set email to required
        if (($this->settings['saveEmailAsUsername'] && $configuredFields['username']['required']) || $settings['registerDoubleOptIn']) {
            $configuredFields['email']['config']['required'] = 1;
            $configuredFields['email']['config']['data']['data-required'] = 1;
            $configuredFields['email']['config']['validate'] = 1;
        }
        
        return $configuredFields;
    }
    
    /**
     * get user groups by uid list
     *
     * @param \array $uids
    *  @param \string $noSelectionLabel
     * @return \array $usergroups
     */
    public function getUsergroups($list = '', $noSelectionLabel = '')
    {
        
        // initialize
        $this->initialize();
        
        $usergroups = array();
        
        if ($list!='') {
            $frontendUserGroupRepository = $this->objectManager->get('DCNGmbH\\MooxCommunity\\Domain\\Repository\\FrontendUserGroupRepository');
            
            $groups = explode(',', $list);
            if ($noSelectionLabel!='' && count($groups)) {
                $usergroups[0] = $noSelectionLabel;
            }
            foreach ($groups as $group) {
                $group = $frontendUserGroupRepository->findByUid($group);
                $usergroups[$group->getUid()] = $group->getTitle();
            }
        }
        
        return $usergroups;
    }
    
    /**
     * Get file info
     *
     * @param \string $filename
     * @return	array	file
     */
    public function getFileInfo($filename)
    {
        $seperatorIndex    = strrpos($filename, '.');
        $file['name']        = substr($filename, 0, $seperatorIndex);
        $file['extension']    = strtolower(substr($filename, ($seperatorIndex+1)));
        return $file;
    }
    
    /**
     * copy to temp
     *
     * @param \string $source
     * @param \string $filename
     * @param \string $folder
     * @return	array	file
     */
    public function copyToTemp($source, $filename, $folder)
    {
        $filenameParts = explode('.', $filename);
        
        $tempfile                = array();
        $tempfile['folder']    = '/typo3temp/'.$folder;
        $tempfile['absolute']    = $GLOBALS['_SERVER']['DOCUMENT_ROOT'].$tempfile['folder'];
        $tempfile['name']        = md5($filenameParts[0].filesize($source).date('Ymdhis')).'.'.$filenameParts[1];
        $tempfile['path']        = $tempfile['absolute'].'/'.$tempfile['name'];
        $tempfile['src']        = $tempfile['folder'].'/'.$tempfile['name'];
        
        if (!is_dir($tempfile['absolute'])) {
            mkdir($tempfile['absolute']);
        }
        
        if (file_exists($tempfile['path'])) {
            unlink($tempfile['path']);
        }
        
        if (!file_exists($tempfile['path'])) {
            move_uploaded_file($source, $tempfile['path']);
        }
        
        return $tempfile;
    }
    
    /**
     * hide tca fields off parent type
     *
     * @param \string $hideFields
     * @param \string $table
     * @param \string $variant
     * @param \string $operator
     * @return	\void
     */
    public function tcaHideDefaultFields($hideFields = '', $table = '', $variant = '', $operator = '=')
    {
        
        // hide classified default fields defined in hide fields string
        if ($hideFields!='') {
            $hideFields = explode(',', $hideFields);
            foreach ($hideFields as $hideField) {
                if (isset($GLOBALS['TCA'][$table]['columns'][$hideField])) {
                    if (!isset($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'])) {
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'] = '';
                    }
                    if (is_string($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']) && $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']=='') {
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'] = 'FIELD:variant:'.$operator.':'.$variant;
                    } elseif (is_string($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']) && $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']!='') {
                        $displayCond = $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'];
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond'] = array();
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'] = array();
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'][] = $displayCond;
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'][] = 'FIELD:variant:'.$operator.':'.$variant;
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'] = array_unique($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR']);
                    } elseif (is_array($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']) && is_array($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'])) {
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'][] = $displayCond;
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'][] = 'FIELD:variant:'.$operator.':'.$variant;
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR'] = array_unique($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['OR']);
                    } elseif (is_array($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']) && is_array($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND'])) {
                        if (!is_array($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND']['OR'])) {
                            $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND']['OR'] = array();
                        }
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND']['OR'][] = 'FIELD:variant:'.$operator.':'.$variant;
                        $GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND']['OR'] = array_unique($GLOBALS['TCA'][$table]['columns'][$hideField]['displayCond']['AND']['OR']);
                    }
                }
            }
        }
    }
    
    /**
     * Get available stream types
     *
     * @param \array $exclude
     * @return \array $types
     */
    public function getAvailableStreamTypes($exclude = null)
    {
        $types = array();
        
        if (is_string($exclude) && $exclude!='') {
            $exclude = explode(',', $exclude);
        } elseif (is_string($exclude) && $exclude=='') {
            $exclude = array();
        }
        
        foreach ($GLOBALS['TCA']['tx_mooxnews_domain_model_news']['columns']['type']['config']['items'] as $type) {
            $types[$type[1]] =  LocalizationUtility::translate($type[0], $this->extensionName);
        }
        
        return $types;
    }
    
    /**
     * Returns storage pids
     *
     * @return \array
     */
    public function getStoragePids()
    {
        return $this->storagePids;
    }

    /**
     * Set storage pids
     *
     * @param \array $storagePids storage pids
     * @return void
     */
    public function setStoragePids($storagePids)
    {
        $this->storagePids = $storagePids;
    }
}
