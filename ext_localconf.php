<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

// register frontend plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'DCNGmbH.' . $_EXTKEY,
    'Pi1',
    array(
        'Pi1' => 'register,unregister,confirmRegistration,profile,confirmProfile,passwordRecovery,newPassword,error',
    ),
    // non-cacheable actions
    array(
        'Pi1' => 'register,unregister,confirmRegistration,profile,confirmProfile,passwordRecovery,newPassword,error',
    )
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'DCNGmbH.' . $_EXTKEY,
    'Pi2',
    array(
        'Pi2' => 'login,error',
    ),
    // non-cacheable actions
    array(
        'Pi2' => 'login,error',
    )
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'DCNGmbH.' . $_EXTKEY,
    'Pi3',
    array(
        'Pi3' => 'logoff,error',
    ),
    // non-cacheable actions
    array(
        'Pi3' => 'logoff,error',
    )
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'DCNGmbH.' . $_EXTKEY,
    'Pi4',
    array(
        'Pi4' => 'listGroups,mapGroups,detailGroup,editGroup,detailUser,editUser,addMembership,removeMembership,addFriendship,removeFriendship,error',
    ),
    // non-cacheable actions
    array(
        'Pi4' => 'listGroups,mapGroups,detailGroup,editGroup,detailUser,editUser,addMembership,removeMembership,addFriendship,removeFriendship,error',
    )
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'DCNGmbH.' . $_EXTKEY,
    'Pi5',
    array(
        'Pi5' => 'listAdminGroups,listMemberships,listFriendships,confirmMembership,rejectMembership,confirmFriendship,rejectFriendship,listGroupMemberships,removeMembership,removeFriendship,addModerator,removeModerator,error',
    ),
    // non-cacheable actions
    array(
        'Pi5' => 'listAdminGroups,listMemberships,listFriendships,confirmMembership,rejectMembership,confirmFriendship,rejectFriendship,listGroupMemberships,removeMembership,removeFriendship,addModerator,removeModerator,error',
    )
);

// Hook backend preview of pi1
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem'][$_EXTKEY] = 'DCNGmbH\MooxCommunity\Hooks\PageLayoutViewDrawItem';
