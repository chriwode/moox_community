<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

// Set language source file
$ll = 'LLL:EXT:moox_community/Resources/Private/Language/locallang_be.xlf:';

// Add flexforms and register plugins
$pluginSignaturePrefix = str_replace('_', '', $_EXTKEY);
$plugins = ['pi1', 'pi2', 'pi3', 'pi4', 'pi5'];
foreach ($plugins as $pluginPrefix) {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        $_EXTKEY,
        ucfirst($pluginPrefix),
        $ll . $pluginPrefix . '.title'
    );

    $TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignaturePrefix . '_' . (string)$pluginPrefix] =
        'pi_flexform';

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
        $pluginSignaturePrefix . '_' . (string)$pluginPrefix,
        'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_' . (string)$pluginPrefix . '.xml'
    );
}

if (TYPO3_MODE === 'BE') {
    $mainModuleName = 'moox';
    if (!isset($TBE_MODULES[$mainModuleName])) {
        $temp_TBE_MODULES = array();
        foreach ($TBE_MODULES as $key => $val) {
            if ($key == 'web') {
                $temp_TBE_MODULES[$key] = $val;
                $temp_TBE_MODULES[$mainModuleName] = '';
            } else {
                $temp_TBE_MODULES[$key] = $val;
            }
        }
        $TBE_MODULES = $temp_TBE_MODULES;
        if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('moox_core')) {
            $mainModuleKey = 'DCNGmbH.moox_core';
            $mainModuleIcon = 'EXT:moox_core/ext_icon32.png';
            $mainModuleLabels = 'LLL:EXT:moox_core/Resources/Private/Language/MainModule.xlf';
        } else {
            $mainModuleKey = 'DCNGmbH.' . $_EXTKEY;
            $mainModuleIcon = 'EXT:' . $_EXTKEY . '/Resources/Public/Moox/MainModuleExtIcon.png';
            $mainModuleLabels = 'LLL:EXT:' . $_EXTKEY . '/Resources/Public/Moox/MainModule.xlf';
        }

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
            $mainModuleKey,
            $mainModuleName,
            '',
            '',
            array(),
            array(
                'access' => 'user,group',
                'icon'   => $mainModuleIcon,
                'labels' => $mainModuleLabels,
            )
        );
    }

    /**
     * Registers a Backend Module
     */
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'DCNGmbH.' . $_EXTKEY,
        $mainModuleName,
        'administration',
        '',    // Position
        array(
            'Template' => 'index,add,edit,delete,previewIframe',
            'Administration' => 'index,add,edit,delete,addGroup,editGroup,deleteGroup,toggleState,toggleDisallowMailing,changeGroup,import,moveToFolder,multiple,csvExport,csvFilteredExport',
            'Import' => 'index',
        ),
        array(
            'access' => 'user,group',
            'icon'   => 'EXT:' . $_EXTKEY . '/ext_icon.gif',
            'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_init.xlf',
        )
    );
}

// Add typoscripts
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    $_EXTKEY,
    'Configuration/TypoScript',
    'MOOX community'
);

// Add Wizard Icons
if (TYPO3_MODE == 'BE') {
    $TBE_MODULES_EXT['xMOD_db_new_content_el']['addElClasses']['DCNGmbH\MooxCommunity\Hooks\Wizicon'] =
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Classes/Hooks/Wizicon.php';
}

// Icon in page tree
$TCA['pages']['columns']['module']['config']['items'][] = [
    'MOOX-Frontend-Benutzer',
    'mxfeuser',
    'EXT:moox_community/ext_icon.gif'
];
\TYPO3\CMS\Backend\Sprite\SpriteManager::addTcaTypeIcon(
    'pages',
    'contains-mxfeuser',
    '../typo3conf/ext/moox_community/ext_icon.gif'
);

// include pageTS
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:moox_community/Configuratoin/PageTs/pageTSconfig.txt">'
);
